﻿(function () {
    appModule.controller('tenant.views.user.createEmployee', [
        '$scope', '$modal', 'abp.services.app.employee','abp.services.app.user', '$location',
        function ($scope, $modal, employeeService,userService, $location) {
            var vm = this;
            vm.saving = false;
            vm.employee = null;
            var userId = $scope.personid;
            var employeeid = $scope.employeeid;
            vm.save = function () {
                vm.saving = true;
                vm.employee.birthDate = vm.dates.date1;
                vm.employee.hireDate = vm.dates.date2;
                vm.employee.sex = vm.Sex;
                vm.employee.userId = userId;
                vm.employee.id = 0;
                vm.employee.locationId = $scope.location.selected.locationId;
                employeeService.createOrUpdateEmployee(vm.employee).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $location.path('/users');
                    //$modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.employee = {};

            function init() {
                userService.getUserForEdit({
                    id: userId
                }).success(function (result) {
                    vm.employee = result.user;
                    vm.employee.firstName = result.user.name;
                    vm.employee.lastName = result.user.surname;
                    vm.employee.email = result.user.emailAddress;
                    console.log(vm.employee);

                   
                });
            }
            function getLocation() {
                employeeService.getEmployeeForEdit({
                    id: employeeid
                }).success(function (result) {
                    
                    vm.location = result.loaction;
                   
                });
            }


            init();
            getLocation();
            $scope.loaction = {};

            vm.cancel = function () {
                $location.path('/dashboard');
            };
            var in10Days = new Date();
            in10Days.setDate(in10Days.getDate() + 10);

            this.dates = {
                date1: new Date(),
                date2: new Date(),
            };


            this.open = {
                date1: false,
                date2: false,

            };

            this.dateOptions = {
                showWeeks: false,
                startingDay: 1
            };

            this.timeOptions = {
                readonlyInput: false,
                showMeridian: true
            };

            this.dateModeOptions = {
                minMode: 'year',
                maxMode: 'year'
            };

            this.openCalendar = function (e, date) {

                vm.open[date] = true;


            };
            $scope.location = {};
        }
    ]);
})();