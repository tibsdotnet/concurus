﻿/* 'app' MODULE DEFINITION */
var appModule = angular.module("app", [
    "ui.router",
    "ui.bootstrap",
    'ui.utils',
    "ui.jq",
    'ui.select',
    'ui.grid.autoResize',
    'ui.grid',
    'ui.grid.pagination',
    'ui.grid.edit',
    'ui.grid.cellNav',
    "ui.bootstrap.datetimepicker",
    "oc.lazyLoad",
    "ngSanitize",
    'angularFileUpload',
    'daterangepicker',
    'abp'
]);

/* LAZY LOAD CONFIG */

/* This application does not define any lazy-load yet but you can use $ocLazyLoad to define and lazy-load js/css files.
 * This code configures $ocLazyLoad plug-in for this application.
 * See it's documents for more information: https://github.com/ocombe/ocLazyLoad
 */
appModule.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        cssFilesInsertBefore: 'ng_load_plugins_before', // load the css files before a LINK element with this ID.
        debug: false,
        events: true,
        modules: []
    });
}]);

/* THEME SETTINGS */
App.setAssetsPath(abp.appPath + 'metronic/assets/');
appModule.factory('settings', ['$rootScope', function ($rootScope) {
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: App.getAssetsPath() + 'admin/layout3/img/',
        layoutCssPath: App.getAssetsPath() + 'admin/layout3/css/',
        assetsPath: abp.appPath + 'metronic/assets',
        globalPath: abp.appPath + 'metronic/assets/global',
        layoutPath: abp.appPath + 'metronic/assets/layouts/layout3'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* ROUTE DEFINITIONS */

appModule.config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        // Default route (overrided below if user has permission)
        $urlRouterProvider.otherwise("/welcome");

        //Welcome page
        $stateProvider.state('welcome', {
            url: '/welcome',
            templateUrl: '~/App/common/views/welcome/index.cshtml'
        });

        //COMMON routes

        if (abp.auth.hasPermission('Pages.Administration.Roles')) {
            $stateProvider.state('roles', {
                url: '/roles',
                templateUrl: '~/App/common/views/roles/index.cshtml',
                menu: 'Administration.Roles'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Users')) {
            $stateProvider.state('users', {
                url: '/users',
                templateUrl: '~/App/common/views/users/index.cshtml',
                menu: 'Administration.Users'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Languages')) {
            $stateProvider.state('languages', {
                url: '/languages',
                templateUrl: '~/App/common/views/languages/index.cshtml',
                menu: 'Administration.Languages'
            });

            if (abp.auth.hasPermission('Pages.Administration.Languages.ChangeTexts')) {
                $stateProvider.state('languageTexts', {
                    url: '/languages/texts/:languageName?sourceName&baseLanguageName&targetValueFilter&filterText',
                    templateUrl: '~/App/common/views/languages/texts.cshtml',
                    menu: 'Administration.Languages'
                });
            }
        }

        if (abp.auth.hasPermission('Pages.Administration.AuditLogs')) {
            $stateProvider.state('auditLogs', {
                url: '/auditLogs',
                templateUrl: '~/App/common/views/auditLogs/index.cshtml',
                menu: 'Administration.AuditLogs'
            });
        }


        //PROJECT ROLE
        if (abp.auth.hasPermission('Pages.Tenant.ProjectRole')) {
            $stateProvider.state('tenant.projectrole', {
                url: '/projectrole',
                templateUrl: '~/App/tenant/views/ProjectRole/index.cshtml',
                menu: 'ProjectRole'
            });
        }


        //HOST routes

        $stateProvider.state('host', {
            'abstract': true,
            url: '/host',
            template: '<div ui-view class="fade-in-up"></div>'
        });

        if (abp.auth.hasPermission('Pages.Tenants')) {
            $urlRouterProvider.otherwise("/host/tenants"); //Entrance page for the host
            $stateProvider.state('host.tenants', {
                url: '/tenants',
                templateUrl: '~/App/host/views/tenants/index.cshtml',
                menu: 'Tenants'
            });
        }

        if (abp.auth.hasPermission('Pages.Editions')) {
            $stateProvider.state('host.editions', {
                url: '/editions',
                templateUrl: '~/App/host/views/editions/index.cshtml',
                menu: 'Editions'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Host.Settings')) {
            $stateProvider.state('host.settings', {
                url: '/settings',
                templateUrl: '~/App/host/views/settings/index.cshtml',
                menu: 'Administration.Settings.Host'
            });
        }

        //TENANT routes

        $stateProvider.state('tenant', {
            'abstract': true,
            url: '/tenant',
            template: '<div ui-view class="fade-in-up"></div>'
        });

        if (abp.auth.hasPermission('Pages.Tenant.Dashboard')) {
            $urlRouterProvider.otherwise("/tenant/dashboard"); //Entrace page for a tenant
            $stateProvider.state('tenant.dashboard', {
                url: '/dashboard',
                templateUrl: '~/App/tenant/views/dashboard/index.cshtml',
                menu: 'Dashboard.Tenant'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Tenant.Settings')) {
            $stateProvider.state('tenant.settings', {
                url: '/settings',
                templateUrl: '~/App/tenant/views/settings/index.cshtml',
                menu: 'Administration.Settings.Tenant'
            });
        }
        //Geography
        if (abp.auth.hasPermission('Pages.Tenant.Geography.Country')) {
            $stateProvider.state('country', {
                url: '/country',
                templateUrl: '~/App/tenant/views/Geography/Country/index.cshtml',
                menu: 'Geography.Country'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Geography.City')) {
            $stateProvider.state('city', {
                url: '/city',
                templateUrl: '~/App/tenant/views/Geography/City/index.cshtml',
                menu: 'Geography.City'
            });
        }
       
        if (abp.auth.hasPermission('Pages.Tenant.Geography.Location')) {
            $stateProvider.state('location', {
                url: '/location',
                templateUrl: '~/App/tenant/views/Geography/Location/index.cshtml',
                menu: 'Geography.Location'
            });
        }
        
        if (abp.auth.hasPermission('Pages.Tenant.Team')) {
            $stateProvider.state('team', {
                url: '/team',
                templateUrl: '~/App/tenant/views/Team/index.cshtml',
                menu: 'Team'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Employee')) {
            $stateProvider.state('employee', {
                url: '/employee',
                templateUrl: '~/App/tenant/views/Employee/index.cshtml',
                menu: 'Employee'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Employee.CreateNewEmployee')) {
            $stateProvider.state('createuseremployee', {
                url: '/createuseremployee/:personid',
                templateUrl: '~/App/common/views/users/createEmployee.cshtml',
                controller: function ($scope, $stateParams) {
                    $scope.personid = $stateParams.personid;

                }
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Employee.CreateNewEmployee')) {
            $stateProvider.state('createemployee', {
                url: '/createemployee',
                templateUrl: '~/App/tenant/views/Employee/createEmployee.cshtml'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Employee.EditEmployee')) {
            $stateProvider.state("Employeedetails", {
                url: "/Employeedetails/:personid",
                templateUrl: "~/App/tenant/views/Employee/EmployeeDetails.cshtml",
                controller: function ($scope, $stateParams) {
                    $scope.personid = $stateParams.personid;

                }

            })
        }

        $stateProvider.state("Employeedetails.information", {
            url: "/information",
            templateUrl: "~/App/tenant/views/Employee/EmployeeInformation.cshtml",

        })

        $stateProvider.state("Employeedetails.account", {
            url: "/account",
            templateUrl: "~/App/tenant/views/Employee/account.cshtml",

        })
        if (abp.auth.hasPermission('Pages.Tenant.Addressbook.Company')) {
            $stateProvider.state('company', {
                url: '/company',
                templateUrl: '~/App/tenant/views/Addressbook/Company/index.cshtml',
                menu: 'Addressbook.Company'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Technology')) {
            $stateProvider.state('Technology', {
                url: '/Technology',
                templateUrl: '~/App/tenant/views/Technology/index.cshtml',
                menu: 'Technology'

            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.ProjectType')) {
            $stateProvider.state('ProjectType', {
                url: '/ProjectType',
                templateUrl: '~/App/tenant/views/ProjectType/index.cshtml',
                menu: 'ProjectType'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Project.ProjectTeam')) {
            $stateProvider.state('projectteam', {
                url: '/projectteam',
                templateUrl: '~/App/tenant/views/ProjectTeam/index.cshtml',
                menu: "projectTeam"

            });
        }
        //PROJECT
        if (abp.auth.hasPermission('Pages.Tenant.Project')) {
            $stateProvider.state('project', {
                url: '/project',
                templateUrl: '~/App/tenant/views/Project/index.cshtml',
                menu: "project"
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Project.EditProject')) {
            $stateProvider.state("ProjectdetailsforEdit", {
                url: "/ProjectdetailsforEdit/:projectId",
                templateUrl: "~/App/tenant/views/Project/ProjectDetails.cshtml",
                controller: function ($scope, $stateParams) {
                    $scope.projectId = $stateParams.projectId;
                    //alert($scope.projectid);

                }

            })
        }
        if (abp.auth.hasPermission('Pages.Tenant.Project.EditProject')) {
            $stateProvider.state("ProjectdetailsforEdit.information", {
                url: "/information",
                templateUrl: "~/App/tenant/views/Project/ProjectInformation.cshtml",

            })
        }
        $stateProvider.state("ProjectdetailsforEdit.projectbacklog", {
            url: "/projectbacklog",
            templateUrl: "~/App/tenant/views/Project/ProjectBacklog.cshtml",

        })
        $stateProvider.state("ProjectdetailsforEdit.projectTask", {
            url: "/projectTask",
            templateUrl: "~/App/tenant/views/Project/ProjectTask.cshtml",

        })
        $stateProvider.state("ProjectdetailsforEdit.checklist", {
            url: "/checklist",
            templateUrl: "~/App/tenant/views/Project/ProjectChecklist.cshtml",

        })

    }
]);

appModule.run(["$rootScope", "settings", "$state", function ($rootScope, settings, $state) {
    $rootScope.$state = $state;
    $rootScope.$settings = settings; 

    $rootScope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
}]);