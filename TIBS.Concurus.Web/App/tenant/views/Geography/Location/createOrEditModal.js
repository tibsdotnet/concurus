﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

(function () {
        appModule.controller('tenant.views.Geography.Location.createOrEditModal', [
        '$scope', '$modalInstance', 'abp.services.app.location', 'locationid',
        function ($scope, $modalInstance, locationService, locationid) {
            var vm = this;
            
            vm.saving = false;
            vm.location = null;
            vm.city = [];

            vm.save = function () {
                vm.saving = true;
                vm.location.cityId = $scope.city.selected.cityId;
                locationService.createOrUpdateLocation(vm.location).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };


            function init() {
                locationService.getLocationForEdit({
                    id: locationid
                }).success(function (result) {
                    vm.location = result.location;
                    vm.city = result.cityName;
                    for (i = 0; i < vm.city.length; i++) {
                        var da = vm.city[i].cityId;
                        if (da == vm.location.cityId) {
                            $scope.city.selected = { cityId: vm.city[i].cityId, cityName: vm.city[i].cityName, countryName: vm.city[i].countryName };
                        }
                    }
                });
            }

            $scope.city = {};
            init();
        }
    ]);
})();