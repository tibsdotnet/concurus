﻿(function () {
    appModule.controller('tenant.views.Geography.Location.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.location',
        function ($scope, $modal, uiGridConstants, locationService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.exportToExcel = function () {
                locationService.getLocationToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.permissions = {
                createLocation: abp.auth.hasPermission('Pages.Tenant.Geography.Location.CreateNewLocation'),
                'edit': abp.auth.hasPermission('Pages.Tenant.Geography.Location.EditLocation'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Geography.Location.DeleteLocation')
            };
            vm.LocationGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            //'<div class=\"ui-grid-cell-contents\">' +
                            //'  <div class="btn-group dropdown" dropdown="">' +
                            //'    <button class="btn btn-xs btn-primary blue" dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            //'    <ul class="dropdown-menu">' +
                            //'      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editLocation(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            //'      <li><a ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteLocation(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            //'    </ul>' +
                            //'  </div>' +
                            //'</div>'

                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editLocation(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteLocation(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('LocationCode'),
                        field: 'locationCode',
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('LocationName'),
                        field: 'locationName',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('City'),
                        field: 'cityName',
                        enableSorting: false,
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    }
                    //{
                    //    name: app.localize('CreationTime'),
                    //    field: 'creationTime',
                    //    cellFilter: 'momentFormat: \'L\'',
                    //    minWidth: 100
                    //}
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getLocation();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getLocation();
                    });
                },
                data: []
            };

            vm.getLocation = function () {
                vm.loading = true;
                locationService.getLocations({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.LocationGridOptions.totalItems = result.totalCount;
                    vm.LocationGridOptions.data = result.items;
                    
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.openCreateLocationModal = function () {
                openLocationModal(null);
            };

            vm.editLocation = function (location) {
                openLocationModal(location.id);
            };

            function openLocationModal(locationid) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Geography/Location/createOrEditModal.cshtml',
                    controller: 'tenant.views.Geography.Location.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        locationid: function () {
                            return locationid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getLocation();
                });
            }


            vm.deleteLocation = function (location) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteCity', location.locationName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            locationService.deleteLocation({
                                id: location.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getLocation();
                            });
                        }
                    }
                );
            };

            vm.getLocation();
        }
    ]);
})();