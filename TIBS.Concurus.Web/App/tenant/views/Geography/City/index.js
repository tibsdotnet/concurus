﻿(function () {
    appModule.controller('tenant.views.Geography.City.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.city',
        function ($scope, $modal, uiGridConstants, cityService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.permissions = {
                createCity: abp.auth.hasPermission('Pages.Tenant.Geography.City.CreateNewCity'),
                'edit': abp.auth.hasPermission('Pages.Tenant.Geography.City.EditCity'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Geography.City.DeleteCity')
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.exportToExcel = function () {
                cityService.getCityToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.CityGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            //'<div class=\"ui-grid-cell-contents\">' +
                            //'  <div class="btn-group dropdown" dropdown="">' +
                            //'    <button class="btn btn-xs btn-primary blue" dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            //'    <ul class="dropdown-menu">' +
                            //'      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editCity(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            //'      <li><a  ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteCity(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            //'    </ul>' +
                            //'  </div>' +
                            //'</div>'

                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editCity(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteCity(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('CityCode'),
                        field: 'cityCode',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('CityName'),
                        field: 'cityName',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                   
                    {
                        name: app.localize('Country'),
                        field: 'countryName',
                        enableSorting: false,
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getCity();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getCity();
                    });
                },
                data: []
            };

            vm.getCity = function () {
                vm.loading = true;
                cityService.getCities({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.CityGridOptions.totalItems = result.totalCount;
                    vm.CityGridOptions.data = result.items;
                    
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.openCreateCityModal = function () {
                openCityModal(null);
            };

            vm.editCity = function (city) {
                openCityModal(city.id);
            };

            function openCityModal(cityid) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Geography/City/createOrEditModal.cshtml',
                    controller: 'tenant.views.Geography.City.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        cityid: function () {
                            return cityid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getCity();
                });
            }


            vm.deleteCity = function (city) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteCity', city.cityName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            cityService.deleteCity({
                                id: city.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getCity();
                            });
                        }
                    }
                );
            };

            vm.getCity();
        }
    ]);
})();