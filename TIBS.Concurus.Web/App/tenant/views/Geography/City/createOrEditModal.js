﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

(function () {
        appModule.controller('tenant.views.Geography.City.createOrEditModal', [
        '$scope', '$modalInstance', 'abp.services.app.city', 'cityid',
        function ($scope, $modalInstance, cityService, cityid) {
            var vm = this;
            
            vm.saving = false;
            vm.city = null;
            vm.country = [];

            vm.save = function () {
                //var assignedCountryNames = _.map(
                //    _.where(vm.country, { isAssigned: true }), //Filter assigned roles
                //    function (country) {
                //        vm.city.countryId = country.countryId;
                //        return country.countryName; //Get names
                //    })
                vm.saving = true;
                vm.city.countryId = $scope.country.selected.countryId;
                cityService.createOrUpdateCity(vm.city).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            //vm.getAssignedCountryCount = function () {
            //    return _.where(vm.country, { isAssigned: true }).length;
            //};
            $scope.country = {};
            //$scope.refreshprojectcountrys = function (country) {
            //    var params = { country: country, sensor: false };
            //    select2service.getMainProjectCountry({ name: params.country }).success(function (result) {
            //        $scope.countrys = result.select2data;
            //    });
            //};

            function init() {
                cityService.getCityForEdit({
                    id: cityid
                }).success(function (result) {
                    vm.city = result.city;
                    vm.country = result.countryName;

                    for (i = 0; i < vm.country.length; i++) {
                        var da = vm.country[i].countryId;
                        if (da == vm.city.countryId) {
                            $scope.country.selected = { countryId: vm.country[i].countryId, countryName: vm.country[i].countryName };
                        }
                    }
                });
            }

            init();
        }
    ]);
})();