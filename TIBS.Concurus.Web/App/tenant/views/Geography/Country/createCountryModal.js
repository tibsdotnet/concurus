﻿(function () {
    appModule.controller('tenant.views.Geography.Country.createCountryModal', [
        '$scope', '$modalInstance', 'abp.services.app.country','countryid',
        function ($scope, $modalInstance, countryService, countryid) {
            var vm = this;
            vm.saving = false;
            vm.country = null;

            vm.save = function () {
                vm.saving = true;
                countryService.createOrUpdateUser(vm.country).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                countryService.getCountryForEdit({
                    id: countryid
                }).success(function (result) {
                    //console.log(result.country);
                    vm.country = result.country;
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();