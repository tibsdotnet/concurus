﻿(function () {
    appModule.controller('tenant.views.Geography.Country.index', [
        '$scope', '$modal', 'abp.services.app.country',
        function ($scope, $modal, countryService) {
            var vm = this;

            vm.countries = [];
            vm.filterText = '';

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.getCountry = function () {
                countryService.getCountry({ filter: vm.filterText }).success(function (result) {
                    vm.countries = result.items;
                });
            }

            vm.openEditCountryModal = function (country) {
                openCountryModal(country.id);
            };

            vm.openCreateCountryModal = function () {
                openCountryModal(null);
            };
            vm.exportToExcel = function () {
                countryService.getCountryToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createCountry: abp.auth.hasPermission('Pages.Tenant.Geography.Country.CreateNewCountry'),
                editCountry: abp.auth.hasPermission('Pages.Tenant.Geography.Country.EditCountry'),
                deleteCountry: abp.auth.hasPermission('Pages.Tenant.Geography.Country.DeleteCountry')
            };

            function openCountryModal(countryid) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Geography/Country/createCountryModal.cshtml',
                    controller: 'tenant.views.Geography.Country.createCountryModal as vm',
                    backdrop: 'static',
                    resolve: {
                        countryid: function () {
                            return countryid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getCountry();
                });
            }

            vm.deleteCountry = function (country) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteCountry', country.countryName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            countryService.deleteCountry({
                                id: country.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getCountry();
                            });
                        }
                    }
                );
            };

            vm.getCountry();
        }
    ]);
})();