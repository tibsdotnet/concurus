﻿(function () {
    appModule.controller('tenant.views.Employee.createEmployee', [
        '$scope', '$modal', 'abp.services.app.employee', '$location', 'FileUploader',
        function ($scope, $modal, employeeService, $location, fileUploader) {
            var vm = this;
            vm.saving = false;
            vm.employee = null;
            var employeeid = $scope.employeeid;
            vm.uploader = new fileUploader({

                url: abp.appPath + 'FileUpload/UploadEmployeeProfileImage/?id=' + 0,
                queueLimit: 1


            });
            vm.save = function () {
                vm.saving = true;
                vm.employee.birthDate = vm.dates.date1;
                vm.employee.hireDate = vm.dates.date2;
                vm.employee.sex = vm.Sex;
                vm.employee.locationId = $scope.location.selected.locationId;
                employeeService.createOrUpdateEmployee(vm.employee).success(function () {
                   
              
                    //location.href = abp.appPath + 'FileUpload/UploadEmployeeProfileImage?id=' + 0,
                       
                    vm.uploader.uploadAll();
                    
                            //alert(result);
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            $location.path('/employee');

                       
                    
                    
                        //$modalInstance.close();
                    }).finally(function () {
                        vm.saving = false;
                    });
                }
                
            

            function init() {
                employeeService.getEmployeeForEdit({
                    id: employeeid
                }).success(function (result) {
                    vm.employee = result.employee;
                    vm.location = result.loaction;
                    for (i = 0; i < vm.location.length; i++) {
                        var da = vm.location[i].locationId;
                        if (da == vm.employee.locationId) {
                            $scope.location.selected = { locationId: vm.location[i].locationId, locationName: vm.location[i].locationName };
                        }
                    }
                });
            }

          init();

            vm.cancel = function () {
                $location.path('/dashboard');
            };
            var in10Days = new Date();
            in10Days.setDate(in10Days.getDate() + 10);

            this.dates = {
                date1: new Date(),
                date2: new Date(),
            };


            this.open = {
                date1: false,
                date2: false,

            };

            this.dateOptions = {
                showWeeks: false,
                startingDay: 1
            };

            this.timeOptions = {
                readonlyInput: false,
                showMeridian: true
            };

            this.dateModeOptions = {
                minMode: 'year',
                maxMode: 'year'
            };

            this.openCalendar = function (e, date) {

                vm.open[date] = true;


            };
            $scope.location = {};
        }
    ]);
})();