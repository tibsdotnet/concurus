﻿(function () {
    appModule.controller('tenant.views.empolyee.account', [
        '$scope', '$modal', 'abp.services.app.user','abp.services.app.employee','$location',
        function ($scope, $modal, userService, employeeService, $location) {
            var vm = this;
            var userId = null;
            vm.saving = false;
            vm.user = null;
            
            var personid = $scope.personid;
            vm.profilePictureId = null;
            vm.roles = [];
            vm.setRandomPassword = (userId == null);
            vm.sendActivationEmail = (userId == null);
            vm.canChangeUserName = true;
            vm.getProfilePictue = function () {
                employeeService.getProfile({
                    id: personid
                }).success(function (result) {
                    vm.profilePictureId = result;
                });
            }

            vm.save = function () {
                var assignedRoleNames = _.map(
                    _.where(vm.roles, { isAssigned: true }), //Filter assigned roles
                    function (role) {
                        return role.roleName; //Get names
                    });

                if (vm.setRandomPassword) {
                    vm.user.password = null;
                }

                vm.saving = true;
                userService.createOrUpdateUser({
                    user: vm.user,
                    assignedRoleNames: assignedRoleNames,
                    sendActivationEmail: vm.sendActivationEmail
                }).success(function () {
                    
                    if (vm.user.id != 0)
                    {
                        vm.UpdateAccount();
                    }
                    
                    //$modal.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.UpdateAccount = function () {
               // alert(vm.user.id);
                employeeService.updateAccount({ id: personid, accountId:vm.user.id }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.saving = false;
                });;
            }


            vm.cancel = function () {
                $location.path('/employee');
            };

            vm.getAssignedRoleCount = function () {
                return _.where(vm.roles, { isAssigned: true }).length;
            };
            

            function init() {
               
                employeeService.getEmployeeForEdit({
                    id: personid
                }).success(function (result) {
                    
                    
                    console.log(result);
                  
                    userId = result.employee.userId;
                   
                        vm.userlist(userId);
                   
                    
                 
                    
                });
                
            }
            vm.userlist = function (userId) {
               
                userService.getUserForEdit({
                    id: userId
                }).success(function (result) {
                    vm.user = result.user;
                  
                    vm.user.passwordRepeat = vm.user.password;
                    vm.roles = result.roles;
                    vm.canChangeUserName = vm.user.userName != app.consts.userManagement.defaultAdminUserName;
                    employeeService.getEmployeeForEdit({
                        id: personid
                    }).success(function (result) {


                        console.log(result);
                        vm.user.name = result.employee.firstName;
                        vm.user.surname = result.employee.lastName;
                        
                        userId = result.employee.userId;
                        

                    });
                });
                employeeService.getEmployeeForEdit({
                    id: personid
                }).success(function (result) {

                    
                    console.log(result);
                    vm.user.name = result.employee.firstName;
                    vm.user.surname = result.employee.lastName;
                   
                    userId = result.employee.userId;
                    

                });
            };

            init();
            vm.getProfilePictue();
            //inittouser();
        }
    ]);
})();