﻿(function () {
    appModule.controller('tenant.views.Employee.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.employee', '$location',
    function ($scope, $modal, uiGridConstants, employeeService,$location) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null

            };
            vm.exportToExcel = function () {
                employeeService.getEmployeeToExcel()
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            
            vm.permissions = {
                createEmployee: abp.auth.hasPermission('Pages.Tenant.Employee.CreateNewEmployee'),
                'edit': abp.auth.hasPermission('Pages.Tenant.Employee.EditEmployee'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Employee.DeleteEmployee')
            };
          

            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.getEmloyee = function () {
                employeeService.getemployee({ filter: vm.filterText }).success(function (result) {
                    vm.countries = result.items;
                });
            }

            vm.EmployeeGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 90,
                        cellTemplate:
                          '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editEmployee(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteemployee(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('FirstName'),
                        field: 'firstName',
                        minWidth: 140,
                        cellTemplate: '<div class=\"ui-grid-cell-contents\">' +
                            '  <img ng-if="row.entity.photoPath" src="{{row.entity.photoPath}}" width="22" height="22" class="img-rounded img-profile-picture-in-grid" />' +
                            '  <img ng-if="!row.entity.photoPath" src="' + abp.appPath + 'Common/Images/default-profile-picture.png" width="22" height="22" class="img-rounded" />' +
                            '  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;' +
                            '</div>',
                    },
                    {
                        name: app.localize('LastName'),
                        field: 'lastName',
                        minWidth: 180,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Address'),
                        field: 'address',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('PhoneNumber'),
                        field: 'phoneNo',
                        minWidth: 120,  
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('UserStatus'),
                        field: 'userStatus',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getEmloyee();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getEmloyee();
                    });
                },
                data: []
            };

            vm.getEmloyee = function () {
                vm.loading = true;
                employeeService.getEmployees({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.EmployeeGridOptions.totalItems = result.totalCount;
                    vm.EmployeeGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editEmployee = function (emolpoyee) {

                var personid = emolpoyee.id;
                //  alert(hash);
                $location.path('/Employeedetails/' + personid);
                // alert("hhij");
            };

            vm.openCreateemployeeModal = function () {
                $location.path('/createemployee')
            };


            vm.deleteemployee = function (employee) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteEmployee', employee.firstName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            employeeService.deleteEmployee({
                                id: employee.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getEmloyee();
                            });
                        }
                    }
                );
            };

            vm.getEmloyee();
        }
    ]);
})();