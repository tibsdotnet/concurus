﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});


appModule.controller('tenant.views.Employee.EmployeeDetails',
     [
        '$scope', '$modal', 'abp.services.app.employee','FileUploader','$location','$state',
       function ($scope, $modal, employeeService, fileUploader, $location, $state) {
            var personid = $scope.personid;
          //  alert(personid);
        var vm = this;
        vm.saving = false;
        vm.profilePictureId = null;
           
        vm.permissions = {
            employeeedit: abp.auth.hasPermission('Pages.Administration.Users.Edit')
            };
       
        init = function () {
            employeeService.getEmployeeForEdit({
                id: personid
            }).success(function (result) {
                console.log(result);
                vm.employee = result.employee;
                vm.location = result.loaction;
                vm.sex = vm.employee.sex;
                vm.role = result.role;
                
                for (i = 0; i < vm.location.length; i++) {
                    
                    var da = vm.location[i].locationId;
                    if (da == vm.employee.locationId) {


                        $scope.location.selected = { locationId: vm.location[i].locationId, locationName: vm.location[i].locationName }

                    }
                }
               


            });
        }
        vm.sex = {};
        var employeeid = $scope.employeeid;
        vm.save = function () {
            vm.saving = true;
            vm.employee.birthDate = vm.dates.date1;
            vm.employee.hireDate = vm.dates.date2;
            console.log(vm.sex);
            vm.employee.sex = vm.sex;
            vm.employee.locationId = $scope.location.selected.locationId;
            employeeService.createOrUpdateEmployee(vm.employee).success(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                $location.path('/employee');
                   }).finally(function () {
                vm.saving = false;
            });
        };
            vm.uploader = new fileUploader({
              
                url: abp.appPath + 'FileUpload/UploadEmployeeProfileImage/?id=' + personid,
                queueLimit: 1
                
                
            });
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                var profilepicuresrc;
                abp.notify.info(app.localize('SavedSuccessfully'));
                employeeService.getProfile({
                    id: personid
                }).success(function (result) {
                    var profileFilePath = result;
                    //alert(result);
                    $('#EmployeeProfile').attr('src', profileFilePath);

                });
                
            };

            vm.saveProfilePicture = function () {
                alert("ok");
               
                //$.ajax({


                //    type: 'POST',
                //    url: '/FileUpload/GetInput',
                //    data: { id:personid },
                //    success: function () {

                        vm.uploader.uploadAll();

                    //},
                    //failure: function (response) {
                    //    $('#result').html(response);

                    //}
                };
                
               
              
          
            var in10Days = new Date();
            in10Days.setDate(in10Days.getDate() + 10);

            this.dates = {
                date1: new Date(),
                date2: new Date(),
            };


            this.open = {
                date1: false,
                date2: false,

            };

           

            this.dateOptions = {
                showWeeks: false,
                startingDay: 1
            };

            this.timeOptions = {
                readonlyInput: false,
                showMeridian: true
            };

            this.dateModeOptions = {
                minMode: 'year',
                maxMode: 'year'
            };

            this.openCalendar = function (e, date) {

                vm.open[date] = true;


            };
           
            vm.cancel = function () {
                $location.path('/employee');
            };

            $scope.location = {};
           
            init();
            $state.go('Employeedetails.information');
           
            
 }]);
