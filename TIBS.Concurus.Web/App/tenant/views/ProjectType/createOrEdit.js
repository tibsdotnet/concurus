﻿(function () {
    appModule.controller('tenant.views.ProjectType.createOrEdit', [
        '$scope', '$modalInstance', 'abp.services.app.projectType', 'ProjectTypeid',
        function ($scope, $modalInstance, ProjectType, ProjectTypeid) {
            var vm = this;
            vm.saving = false;
            vm.ProjectType = null;

            vm.save = function () {
                
                vm.saving = true;
                vm.ProjectType.id = ProjectTypeid;
                ProjectType.createOrUpdateProjectType(vm.ProjectType).success(function () {
                    
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                ProjectType.getProjectForEdit({
                    id: ProjectTypeid
                }).success(function (result) {
                    vm.ProjectType = result.projectType;
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();