﻿(function () {
    appModule.controller('tenant.views.ProjectType.index', [
        '$scope', '$modal', 'abp.services.app.projectType',
        function ($scope, $modal, ProjectType) {
            var vm = this;

            vm.ProjectType = [];
            vm.filterText = '';

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.getProjectType = function () {
                ProjectType.getProjectTypes({ filter: vm.filterText }).success(function (result) {
                    vm.ProjectType = result.items;
                });
            }
            vm.openCreateProjectType = function () {
                openProjectType(null);
            };
            vm.openEditProjectType = function (ProjectType) {
                openProjectType(ProjectType.id);
                
            };  

            vm.exportToExcel = function () {
                ProjectType.getrojectTypeToExcel({ filter: vm.filterText })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createProjectType: abp.auth.hasPermission('Pages.Tenant.Project.ProjectType.CreateProjectTypye'),
                editProjectType: abp.auth.hasPermission('Pages.Tenant.Project.ProjectType.EditProjectType'),
                deleteProjectType: abp.auth.hasPermission('Pages.Tenant.Project.ProjectType.DeleteProjectType')
            };
            function openProjectType(ProjectTypeid) {
                //(proleid);
                
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/ProjectType/createOrEdit.cshtml',
                    controller: 'tenant.views.ProjectType.createOrEdit as vm',
                    backdrop: 'static',
                    resolve: {
                        ProjectTypeid: function () {
                            return ProjectTypeid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getProjectType();
                });
            }
            vm.deleteProjectType = function (projectType) {
                abp.message.confirm(
                    app.localize('AreYouSureToProjectType', projectType.ProjectTypeName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ProjectType.deleteProjectType({
                                id: projectType.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getProjectType();
                            });
                        }
                    }
                );
            };
           


          


            vm.getProjectType();
        }
    ]);
})();