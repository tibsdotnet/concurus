﻿(function () {
    appModule.controller('tenant.views.Addressbook.Company.createCompanyModal', [
        '$scope', '$modalInstance', '$modal', 'abp.services.app.company', 'uiGridConstants', 'companyId',
        function ($scope, $modalInstance,$modal, CompanyService,uiGridConstants, companyId) {
            var vm = this;
            vm.saving = false;
            vm.Company = null;
            vm.contactType = [];
           
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                companyId:companyId,
                sorting: null
            };
            vm.exportToExcel = function () {
                CompanyService.getContactToExcel(requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.save = function () {
                vm.saving = true;
                vm.Company.contactTypeId = $scope.contactType.selected.contactTypeId;
               // alert(vm.Company.contactTypeId)
                CompanyService.createOrUpdateCompany(vm.Company).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                CompanyService.getCompanyForEdit({
                    id: companyId
                }).success(function (result) {
                    //console.log(result.Company);
                    vm.Company = result.company;
                    vm.contactType = result.typeName;
                    for (i = 0; i < vm.contactType.length; i++) {
                        var da = vm.contactType[i].contactTypeId;
                        if (da == vm.Company.contactTypeId) {
                            $scope.contactType.selected = { contactTypeId: vm.contactType[i].contactTypeId, typeName: vm.contactType[i].typeName };
                        }
                    }
                });
            }

            init();
            vm.ContactGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            //'<div class=\"ui-grid-cell-contents\">' +
                            //'  <div class="btn-group dropdown" dropdown="">' +
                            //'    <button class="btn btn-xs btn-primary blue" dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            //'    <ul class="dropdown-menu">' +
                            //'      <li><a  ng-click="grid.appScope.editMainProjectSehedule(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            //'      <li><a  ng-click="grid.appScope.deleteMainprojectschedule(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            //'    </ul>' +
                            //'  </div>' +
                            //'</div>'

                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editCompanyContact(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.deletecompanyContact(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                   {
                       name: app.localize('ComtactName'),
                       field: 'name',
                       minWidth: 100
                       // cellTemplate: cellToolTipTemplate
                   },
                   {
                       name: app.localize('PhoneNumber'),
                       field: 'phoneNumber',

                       minWidth: 150
                   },
                   {
                       name: app.localize('Address'),
                       field: 'address',
                       minWidth: 200
                       // cellTemplate: cellToolTipTemplate
                   },
                   {
                       name: app.localize('MailId'),
                       field: 'mailId',
                       minWidth: 200
                       //   cellTemplate: cellToolTipTemplate
                   }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getContacts();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getContacts();
                    });
                },
                data: []
            };
            vm.getContacts = function () {
                CompanyService.getCompanyContacts({ filter: vm.filterText,companyId:companyId }).success(function (result) {
                    vm.ContactGridOptions.totalItems = result.totalCount;
                    vm.ContactGridOptions.data = result.items;
                });
            };
            vm.getContacts();
            vm.editCompanyContact = function (contact) {
                openCompanyCompanyModal(contact.id);
            };
            vm.openCreateCompanyContactModal = function () {
                openCompanyCompanyModal(null);
            };
            function openCompanyCompanyModal(contactId) {

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Addressbook/Company/createContactModal.cshtml',
                    controller: 'tenant.views.Addressbook.Company.createContactModal as vm',
                    backdrop: 'static',
                    resolve: {
                        contactId: function () {
                            return contactId;

                        },
                        companyId: function () {
                            return companyId;
                        }
                    }
                });
               

               $modalInstance.result.then(function () {

                   vm.getContacts();
                });
            }
            vm.deletecompanyContact = function (contact) {
                abp.message.confirm(
                app.localize('AreYouSureToDeleteContact', contact.contactName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        CompanyService.deleteConact({
                            id: contact.id
                        }).success(function () {
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                            vm.getContacts();
                        });
                    }
                }
            );

            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
            $scope.contactType = {};
        }
    ]);
})();