﻿(function () {
    appModule.controller('tenant.views.Addressbook.Company.createContactModal', [
        '$scope', '$modalInstance', 'abp.services.app.company', 'uiGridConstants', 'contactId','companyId',
        function ($scope, $modalInstance, CompanyService, uiGridConstants, contactId, companyId) {
            
            var vm = this;
            vm.contact = {};
            vm.save = function () {
                vm.saving = true;
                vm.contact.companyId = companyId;
                CompanyService.createOrUpdateContact(vm.contact).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                CompanyService.getContactForEdit({
                    id: contactId
                }).success(function (result) {
                    //console.log(result.Company);
                    vm.contact = result.contact;
                });
            }

            init();
            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();