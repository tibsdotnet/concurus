﻿(function () {
    appModule.controller('tenant.views.AddressBook.Company.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.company',
        function ($scope, $modal, uiGridConstants, CompanyService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.permissions = {
                createCompany: abp.auth.hasPermission('Pages.Tenant.Addressbook.Company.CreateCompany'),
                'edit': abp.auth.hasPermission('Pages.Tenant.Addressbook.Company.EditCompany'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Addressbook.Company.DeleteCompany')
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.getCompany = function () {
                CompanyService.getCompany({ filter: vm.filterText }).success(function (result) {
                    vm.countries = result.items;
                });
            }
            vm.exportToExcel = function () {
                CompanyService.getCompanyToExcel(requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.CompanyGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 90,
                        cellTemplate:
                           
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button ng-if="grid.appScope.permissions.edit" class="btn btn-default btn-xs" ng-click="grid.appScope.editCompany(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button ng-if="grid.appScope.permissions.delete" class="btn btn-default btn-xs" ng-click="grid.appScope.deleteCompany(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('CompanyName'),
                        field: 'name',
                        minWidth: 140,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ContactType'),
                        field: 'typeName',
                        minWidth: 180,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('PhoneNumber'),
                        field: 'phoneNumber',
                        minWidth: 180,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Email'),
                        field: 'mailId',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Website'),
                        field: 'website',
                        minWidth: 120,  
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Address'),
                        field: 'address',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getCompany();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getCompany();
                    });
                },
                data: []
            };

            vm.getCompany = function () {
                vm.loading = true;
                CompanyService.getCompanies({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.CompanyGridOptions.totalItems = result.totalCount;
                    vm.CompanyGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editCompany = function (Company) {
                openCompanyModal(Company.id);
            };

            vm.openCreateCompanyModal = function () {
                openCompanyModal(null);
            };

            function openCompanyModal(companyId) {
               
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Addressbook/Company/createCompanyModal.cshtml',
                    controller: 'tenant.views.Addressbook.Company.createCompanyModal as vm',
                    backdrop: 'static',
                    resolve: {
                        companyId: function () {
                            return companyId;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    
                    vm.getCompany();
                });
            }


            vm.deleteCompany = function (Company) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteCompany', Company.CompanyName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            CompanyService.deleteCompany({
                                id: Company.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getCompany();
                            });
                        }
                    }
                );
            };

            vm.getCompany();
        }
    ]);
})();