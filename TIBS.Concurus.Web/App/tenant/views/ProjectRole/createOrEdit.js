﻿(function () {
    appModule.controller('tenant.views.ProjectRole.createOrEdit', [
        '$scope', '$modalInstance', 'abp.services.app.projectRole', 'proleid',
        function ($scope, $modalInstance, projectRole, proleid) {
            var vm = this;
            vm.saving = false;
            vm.projectrole = null;

            vm.save = function () {
                
                vm.saving = true;
                vm.projectrole.id = proleid;
                projectRole.createOrUpdateProjectRole(vm.projectrole).success(function () {
                    
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                projectRole.getProjectRoleForEdit({
                    id: proleid
                }).success(function (result) {
                    vm.projectrole = result.projectRole;
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();