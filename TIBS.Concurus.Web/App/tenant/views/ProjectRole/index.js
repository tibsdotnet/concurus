﻿(function () {
    appModule.controller('tenant.views.ProjectRole.index', [
        '$scope', '$modal', 'abp.services.app.projectRole',
        function ($scope, $modal, projectRole) {
            var vm = this;

            vm.projectrole = [];
            vm.filterText = '';

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.getProjectRole = function () {
                projectRole.getProjectRole({ filter: vm.filterText }).success(function (result) {
                    vm.projectrole = result.items;
                });
            }
            vm.openCreateProjectRole = function () {
                openProjectRole(null);
            };
            vm.openEditProjectRole = function (prole) {
                openProjectRole(prole.id);
                
            };  

            vm.exportToExcel = function () {
                projectRole.getProjectRoleToExcel(vm.requestParams)
                    .success(function (result) {    
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createprole: abp.auth.hasPermission('Pages.Tenant.ProjectRole.CreateProjectRole'),
                editprole: abp.auth.hasPermission('Pages.Tenant.ProjectRole.EditProjectRole'),
                deleteprole: abp.auth.hasPermission('Pages.Tenant.ProjectRole.DeleteProjectRole')
            };
            function openProjectRole(proleid) {
                
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/ProjectRole/createOrEdit.cshtml',
                    controller: 'tenant.views.ProjectRole.createOrEdit as vm',
                    backdrop: 'static',
                    resolve: {
                        proleid: function () {
                            return proleid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getProjectRole();
                });
            }
            vm.deleteProjectRole = function (prole) {
                abp.message.confirm(
                    app.localize('AreYouSureToProjectRole', prole.pRoleName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            projectRole.deleteProjectRole({
                                id: prole.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getProjectRole();
                            });
                        }
                    }
                );
            };
           


          


            vm.getProjectRole();
        }
    ]);
})();