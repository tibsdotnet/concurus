﻿(function () {
    appModule.controller('tenant.views.projectTeam.createCompany', [
        '$scope', '$modalInstance', 'abp.services.app.projectTeam',
        function ($scope, $modalInstance, projectTeamService) {
            var vm = this;
            vm.saving = false;
            vm.projectTeam = {};
            
            vm.save = function () {
                vm.saving = true;
                vm.projectTeam.projectId = $scope.company.selected.companyId;
                projectTeamService.createProjectTeam(vm.projectTeam).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.location = [];
            function init() {
                projectTeamService.getCompanyForEdit({
                    id: null
                }).success(function (result) {
                  
                    vm.company = result.company;
                    
                   
                });
            }

            $scope.company = {};
          init();

            vm.cancel = function () {
                $modalInstance.close();
            };
           
           
        }
    ]);
})();