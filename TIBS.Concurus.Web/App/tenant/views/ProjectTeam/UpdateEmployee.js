﻿(function () {
    appModule.controller('tenant.views.projectTeam.UpdateEmployee', [
        '$scope', '$modalInstance', 'abp.services.app.projectTeam','projectId',
        function ($scope, $modalInstance, projectTeamService, projectId) {
            var vm = this;
            vm.saving = false;
            vm.projectTeam = {};

            vm.save = function () {
                vm.saving = true;
                vm.projectTeam.projectId = projectId;
                vm.projectTeam.employeeId = $scope.employee.selected.employeeId;
                vm.projectTeam.projectRoleId = $scope.projectRole.selected.projectRoleId;
                projectTeamService.updateProjectEmployee(vm.projectTeam).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.location = [];
            function init() {
                projectTeamService.getProjectEmployeeForEdit({
                    id: null
                }).success(function (result) {

                    vm.employee = result.employee;
                    vm.projectRole = result.projectRole;


                });
            }

            $scope.employee = {};
            $scope.projectRole = {};
            init();

            vm.cancel = function () {
                $modalInstance.close();
            };


        }
    ]);
})();