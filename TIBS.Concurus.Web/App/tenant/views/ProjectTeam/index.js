﻿(function () {
    appModule.controller('tenant.views.ProjectTeam.index', [
        '$scope', '$modal', 'abp.services.app.projectTeam',
        function ($scope, $modal, ProjectTeam) {
            var vm = this;

            vm.ProjectTeam = [];
            vm.filterText = '';

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.getProjectTeam = function () {
                ProjectTeam.getProjectTeam({ filter: vm.filterText }).success(function (result) {
                    vm.ProjectTeam = result.items;
                });
            }
            vm.exportToExcel = function () {
                ProjectTeam.getProjectTeamToExcel({ filter: vm.filterText })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.openCreateProjectTeam = function () {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/ProjectTeam/createCompany.cshtml',
                    controller: 'tenant.views.projectTeam.createCompany as vm',
                    backdrop: 'static'
                   

                    
                });
            };
            vm.openEditProjectTeam = function (projectTeam) {
                openProjectTeamForEdit(projectTeam.projectId);
                
            };  

            vm.exportToExcel = function () {
                ProjectTeam.getProjectTeamToExcel(vm.requestParams)
                    .success(function (result) {    
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createprojectTeam: abp.auth.hasPermission('Pages.Tenant.Project.ProjectTeam.CreateProjectTeam'),
                editprojectTeam: abp.auth.hasPermission('Pages.Tenant.Project.ProjectTeam.EditProjectTeam'),
                deleteprojectTeam: abp.auth.hasPermission('Pages.Tenant.Project.ProjectType.DeleteProjectType')
            };
            function openProjectTeamForEdit(projectId) {
                
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/ProjectTeam/createOrEdit.cshtml',
                    controller: 'tenant.views.ProjectTeam.createOrEdit as vm',
                    backdrop: 'static',
                    resolve: {
                        projectId: function () {
                            return projectId;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getProjectTeam();
                });
            }
            vm.deleteProjectTeam = function (projectTeam) {
                abp.message.confirm(
                    app.localize('AreYouSureToProjectTeam', projectTeam.projectTeamName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ProjectTeam.deleteProjectTeam({
                                id: projectTeam.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getProjectTeam();
                            });
                        }
                    }
                );
            };
           


          


            vm.getProjectTeam();
        }
    ]);
})();