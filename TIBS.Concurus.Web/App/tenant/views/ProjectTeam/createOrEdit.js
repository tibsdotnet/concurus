﻿(function () {
    appModule.controller('tenant.views.ProjectTeam.createOrEdit', [
        '$scope', '$modalInstance','$modal', 'abp.services.app.projectTeam', 'projectId',
        function ($scope, $modalInstance,$modal, projectTeamService, projectId) {
            var vm = this;
            vm.saving = false;
            vm.employees = [];
            //alert(projectId);
            vm.team = {};

            vm.save = function () {
                var assignedEmployee = _.map(
                    _.where(vm.employees, { isAssigned: true }), //Filter assigned employee
                    function (employee) {
                        return employee.employeeId; //Get names
                    });
                vm.saving = true;
                vm.team.id = projectId;
                vm.team.employeeId = assignedEmployee;
                console.log(vm.team.employeeId);
                projectTeamService.createOrUpdateTeam(vm.team).success(function () {
                    
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            vm.openCreateProjectTeamMember = function () {
                alert(projectId);
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/ProjectTeam/UpdateEmployee.cshtml',
                    controller: 'tenant.views.projectTeam.UpdateEmployee as vm',
                    backdrop: 'static',
                    resolve: {
                        projectId: function () {
                            return projectId;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    init();
                });
            };


            function init() {
                projectTeamService.getProjectTeamForEdit({
                    id: projectId
                }).success(function (result) {
                    vm.team = result.projectTeam;
                    vm.employees = result.employee;
                    vm.projectName = result.projectName;
                    console.log(vm.employees);
                });
            }

            vm.deleteProjectTeamMember = function (projectTeamEmployeeId) {
                abp.message.confirm(
                    app.localize('AreYouSureToProjectTeamEmployee'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            projectTeamService.deleteTeamEmplyee({
                                id: projectTeamEmployeeId
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                init();
                            });
                        }
                    }
                );
            };


            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();