﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});


(function () {
    appModule.controller('tenant.views.Project.CreateProjectTask', [
        '$scope', '$modalInstance', '$modal', 'abp.services.app.project', 'ProjectTaskId', 'projectId',
        function ($scope, $modalInstance, $modal, ProjectTaskService, ProjectTaskId, projectId) {
            var vm = this;
            vm.saving = false;
            vm.ProjectTask = {};
            vm.allotedhours = {};
            vm.estimatehours = {};

            // alert(projectId);
            vm.save = function () {
                vm.saving = true;
                //  vm.ProjectTask.projectId = projectId;
                vm.ProjectTask.assignedUserId = $scope.assigned.selected.userId;
                vm.ProjectTask.productBacklogId = $scope.projectBacklog.selected.productBacklogId;
                vm.ProjectTask.allotedHours = $scope.alloatedhours.selected.id;
                vm.ProjectTask.estimatehours=$scope.estimatehours.selected.id;

                ProjectTaskService.createOrUpdateProjectTask(vm.ProjectTask).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                ProjectTaskService.getProjectTaskForEdit({
                    id: ProjectTaskId
                }).success(function (result) {
                    //console.log(result.ProjectTask);
                    vm.ProjectTask = result.projectTask;
                    vm.projectBacklog = result.projectBacklog;
                    vm.assigned = result.user;
                    console.log(vm.projectBacklog);
                   
                    console.log(result.projectTask);
                    for (i = 0; i < vm.projectBacklog.length; i++) {
                        var da = vm.projectBacklog[i].productBacklogId;

                        if (da == vm.ProjectTask.productBacklogId) {
                            $scope.projectBacklog.selected = { productBacklogId: vm.projectBacklog[i].productBacklogId, projectBacklogName: vm.projectBacklog[i].projectBacklogName };
                        }
                    }
                    for (i = 0; i < vm.assigned.length; i++) {
                        var da = vm.assigned[i].userId;
                        if (da == vm.ProjectTask.assignedUserId) {
                            $scope.assigned.selected = { userId: vm.assigned[i].userId, userName: vm.assigned[i].userName };
                        }
                    }
                    for (i = 0; i < vm.EstimateHours.length; i++) {
                        var da = vm.EstimateHours[i].id;
                        
                        //alert("OK");
                        if (da == result.projectTask.estimateHours) {
                            
                            $scope.estimatehours.selected = { id: vm.EstimateHours[i].id, name: vm.EstimateHours[i].name };
                        }
                    }
                    for (i = 0; i < vm.AlloatedHours.length; i++) {
                        //alert("OK");
                        var da = vm.AlloatedHours[i].id;
                        if (da == result.projectTask.allotedHours) {
                           
                            $scope.alloatedhours.selected = { id: vm.AlloatedHours[i].i, name: vm.AlloatedHours[i].name };
                        }
                    }
                });
            }

            init();
            $scope.projectBacklog = {};
            $scope.assigned = {};
            $scope.alloatedhours = {};
            $scope.estimatehours = {};
            var in10Days = new Date();
            in10Days.setDate(in10Days.getDate() + 10);

            this.dates = {
                date1: new Date(),
                date2: new Date(),
            };


            this.open = {
                date1: false,
                date2: false,
            };

            // Disable weekend selection
            this.disabled = function (date, mode) {
                return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
            };

            this.dateOptions = {
                showWeeks: false,
                startingDay: 1
            };


            this.timeOptions = {
                readonlyInput: false,
                showMeridian: true
            };

            this.dateModeOptions = {
                minMode: 'year',
                maxMode: 'year'
            };

            this.openCalendar = function (e, date) {
                vm.open[date] = true;
            };
            vm.EstimateHours = [{ name: '1 Hour', id: 1 },
             { name: '2 Hours', id: 2},
             { name: '4 Hours', id: 4 },
             { name: '8 Hours', id: 8 },
            ]
            vm.AlloatedHours = [{ name: '1 Hour', value: 1 },
             { name: '2 Hours', id: 2 },
             { name: '4 Hours', id: 4 },
             { name: '8 Hours', id: 8 },
            ]
            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();