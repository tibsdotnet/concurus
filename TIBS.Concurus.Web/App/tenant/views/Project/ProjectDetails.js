﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});


appModule.controller('tenant.views.Project.ProjectDetails',
     [
        '$scope', '$modal', 'abp.services.app.project', '$location', '$state', 'appSession', '$rootScope',
       function ($scope, $modal, ProjectService, $location, $state, appSession, $rootScope) {
           var projectId = $scope.projectId;
           var pageid = 0;
          //  alert(projectId);
        var vm = this;
        vm.saving = false;
        vm.profilePictureId = null;
           
        vm.permissions = {
            Projectedit: abp.auth.hasPermission('Pages.Tenant.Project.EditProject'),
            projectbacklog: abp.auth.hasPermission('Pages.Tenant.Project.ProjectBacklog'),
            projectTask: abp.auth.hasPermission('Pages.Tenant.Project.ProjectTask')
            };
       
        init = function () {
            ProjectService.getProjectForEdit({
                id: projectId
            }).success(function (result) {
                console.log(result);
                vm.Project = result.project;
                vm.technology = result.technology;
                vm.projectType = result.projectType;

                vm.company = result.company;
                vm.team = result.team;
                for (i = 0; i < vm.technology.length; i++) {
                    var da = vm.technology[i].technologyId;
                    if (da == vm.Project.technologyId) {
                        $scope.technology.selected = { technologyId: vm.technology[i].technologyId, technologyName: vm.technology[i].technologyName };
                    }
                }
                for (i = 0; i < vm.projectType.length; i++) {
                    var da = vm.projectType[i].projectTypeId;
                    if (da == vm.Project.projectTypeId) {
                        $scope.projectType.selected = { projectTypeId: vm.projectType[i].projectTypeId, projectTypeName: vm.projectType[i].projectTypeName };
                    }
                }
                for (i = 0; i < vm.company.length; i++) {
                    var da = vm.company[i].companyId;
                    if (da == vm.Project.companyId) {
                        $scope.company.selected = { companyId: vm.company[i].companyId, companyName: vm.company[i].companyName };
                    }
                }
                for (i = 0; i < vm.team.length; i++) {
                    var da = vm.team[i].teamId;
                    if (da == vm.Project.teamId) {
                        $scope.team.selected = { teamId: vm.team[i].teamId, teamName: vm.team[i].teamName };
                    }
                }
                
                
               
               


            });
        }
        vm.sex = {};
        var projectId = $scope.projectId;
        vm.save = function () {
            vm.saving = true;
            vm.Project.technologyId = $scope.technology.selected.technologyId;
            vm.Project.companyId = $scope.company.selected.companyId;
            vm.Project.projectTypeId = $scope.projectType.selected.projectTypeId;
            vm.Project.teamId = $scope.team.selected.teamId;

            ProjectService.createOrUpdateProject(vm.Project).success(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                $location.path('/project');
                   }).finally(function () {
                vm.saving = false;
            });
        };
            //vm.uploader = new fileUploader({
              
            //    url: abp.appPath + 'FileUpload/UploadProjectProfileImage/?id=' + projectId,
            //    queueLimit: 1
                
                
            //});
            //vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
            //    var profilepicuresrc;
            //    abp.notify.info(app.localize('SavedSuccessfully'));
            //    ProjectService.getProfile({
            //        id: projectId
            //    }).success(function (result) {
            //        var profileFilePath = result;
            //        //alert(result);
            //        $('#ProjectProfile').attr('src', profileFilePath);

            //    });
                
            //};

            //vm.saveProfilePicture = function () {
            //    alert("ok");
               
            //    //$.ajax({


            //    //    type: 'POST',
            //    //    url: '/FileUpload/GetInput',
            //    //    data: { id:projectId },
            //    //    success: function () {

            //            vm.uploader.uploadAll();

            //        //},
            //        //failure: function (response) {
            //        //    $('#result').html(response);

            //        //}
            //    };
                
               
              
          
            var in10Days = new Date();
            in10Days.setDate(in10Days.getDate() + 10);

            this.dates = {
                date1: new Date(),
                date2: new Date(),
            };


            this.open = {
                date1: false,
                date2: false,

            };

           

            this.dateOptions = {
                showWeeks: false,
                startingDay: 1
            };

            this.timeOptions = {
                readonlyInput: false,
                showMeridian: true
            };

            this.dateModeOptions = {
                minMode: 'year',
                maxMode: 'year'
            };

            this.openCalendar = function (e, date) {

                vm.open[date] = true;


            };
           
            vm.cancel = function () {
                $location.path('/Project');
            };

            $scope.company = {};
            $scope.projectType = {};
            $scope.team = {};
            $scope.technology = {};
           
            init();
            vm.childmethod = function () {
                $rootScope.$emit("CallParentMethod", {});
            }
            $state.go('ProjectdetailsforEdit.information');



           ///PROJECTCHECKLIST CONTROLLER
           
            
 }]);
