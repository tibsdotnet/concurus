﻿(function () {
    appModule.controller('tenant.views.Project.ProjectChecklist',
         [
            '$scope','$rootScope', '$modal', 'abp.services.app.project', '$location', '$state','appSession',
           function ($scope, $rootScope, $modal, ProjectService, $location, $state, appSession) {
               var projectId = $scope.projectId;
               $scope.$on('$viewContentLoaded', function () {
                   Metronic.initAjax();
               });
              // alert(projectId);
              var vm = this;
              vm.projectchecklist = {};
               vm.getShownUserName = function () {
                   if (!abp.multiTenancy.isEnabled) {
                       return appSession.user.userName;
                   } else {
                       if (appSession.tenant) {
                           return appSession.tenant.tenancyName + '\\' + appSession.user.userName.toUpperCase();
                       } else {
                           return '.\\' + appSession.user.userName.toUpperCase();
                       }
                   }
               };
               vm.save = function () {
                   vm.projectchecklist.projectId = projectId;
                   ProjectService.createOrUpdateProjectCheckList(vm.projectchecklist).success(function () {
                       vm.getProjectCheckList();
                       vm.projectchecklist = null;
                   })
               };
               vm.getProjectCheckList = function () {
                   vm.loading = true;
                   ProjectService.getProjectChecklists({
                       id:projectId
                   }).success(function (result) {

                       vm.ProjectCheckList = result.items;
                       console.log(vm.ProjectCheckList);
                   }).finally(function () {
                       vm.loading = false;
                   });
               };
               vm.init = function (projectchecklist) {
                   
                   ProjectService.getProjectCheckListForEdit({ id: projectchecklist.id }).success(function (result) {
                       vm.projectchecklist = result.projectCheckList;
                   })
               };
               init = function () {
                   ProjectService.getProjectForEdit({
                       id: projectId
                   }).success(function (result) {
                       console.log(result);
                       vm.ProjectName = result.project.projectName;
                   })
               };
               vm.makeImportant = function (projectChecklist) {
                   vm.projectchecklist = projectChecklist;
                   vm.projectchecklist.projectId = projectId
                   vm.projectchecklist.projectImportant = true;
                   ProjectService.updateProjectImportant(vm.projectchecklist).success(function () {
                       vm.getProjectCheckList();
                   })
               };
              
               $rootScope.$on("CallParentMethod", function () {
                   vm.parentmethod();
               });
               vm.parentmethod = function () {
                   //alert("jj");
                   vm.ProjectCheckList = null;
                   vm.loading = true;
                   ProjectService.getImportantProjectCheckList({
                       id: projectId
                   }).success(function (result) {

                       vm.ProjectCheckList = result.items;
                       console.log(vm.ProjectCheckList);
                   }).finally(function () {
                       vm.loading = false;
                   });
               };
               init();
               vm.getProjectCheckList();
           }

         ])
})();