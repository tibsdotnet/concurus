﻿(function () {
    appModule.controller('tenant.views.Project.Projectbacklog', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.project',
        function ($scope, $modal, uiGridConstants, ProjectbacklogService) {
            var vm = this;
            var projectId = $scope.projectId;
            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.showButton = false;
            vm.filterText = null;

            $scope.backlogProirity1 = [];
            vm.exportToExcel = function () {
                ProjectbacklogService.getProjectBacklogToExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    projectId: projectId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
          
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.permissions = {
                createProjectBacklog: abp.auth.hasPermission('Pages.Tenant.Project.ProjectBacklog.CreateProjectBacklog'),
                'edit': abp.auth.hasPermission('Pages.Tenant.Project.ProjectBacklog.EditProjectBacklog'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Project.ProjectBacklog.DeleteProjectBacklog')
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            //vm.getProjectbacklog = function () {
            //    ProjectbacklogService.getProjectbacklog({ filter: vm.filterText }).success(function (result) {
            //        vm.countries = result.items;
            //    });
            //}

            vm.ProjectbacklogGridOptions = {};

            //vm.ProjectbacklogGridOptions = {
            vm.ProjectbacklogGridOptions.enableHorizontalScrollbar = uiGridConstants.scrollbars.WHEN_NEEDED;
            vm.ProjectbacklogGridOptions.enableVerticalScrollbar = uiGridConstants.scrollbars.WHEN_NEEDED;
            vm.ProjectbacklogGridOptions.paginationPageSizes = app.consts.grid.defaultPageSizes;
            vm.ProjectbacklogGridOptions.paginationPageSize = app.consts.grid.defaultPageSize;
            vm.ProjectbacklogGridOptions.useExternalPagination = true;
            vm.ProjectbacklogGridOptions.useExternalSorting = true;
            vm.ProjectbacklogGridOptions.appScopeProvider = vm;
               
            
            
            
         vm.ProjectbacklogGridOptions.onRegisterApi= function (gridApi) {
             $scope.gridApi = gridApi;
             $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                 if (!sortColumns.length || !sortColumns[0].field) {
                     requestParams.sorting = null;
                 } else {
                     requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                 }
                 vm.getProjectbacklog();
             });
             gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                 requestParams.skipCount = (pageNumber - 1) * pageSize;
                 requestParams.maxResultCount = pageSize;

                 vm.getProjectbacklog();
             });
             
             gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                 colDef.cellClass = function (uigrid, row, col, rowRenderIndex, colRenderIndex) {
                     if (rowEntity.id === row.entity.id && newValue !== oldValue) {
                         //alert(rowEntity.id);
                         //alert(row.entity.id);
                         rowEntity.enableEdit = true;
                         return "test";
                     }
                     return "";
                 };

                 $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                 if (colDef.name === app.localize('BacklogPriority')) {
                     for (var i = 0; i < $scope.backlogProirity1.length; i++) {
                         var prod = $scope.backlogProirity1[i];
                         if (prod.backlogPriorityId === newValue) {
                             
                             rowEntity.backlogPriority = prod.backlogPriorityName;
                             vm.backlogProirityId = prod.backlogPriorityId;
                             break;
                         }
                     }
                     
                     //calculateBOption(rowEntity);
                 }
                 

                
                 if(colDef.name===app.localize('BacklogType'))
                 {
                     for (var i = 0; i < $scope.backlogType.length; i++) {
                         var prod = $scope.backlogType[i];
                         if (prod.backlogTypeId === newValue) {

                             rowEntity.backlogType = prod.backlogTypeName;
                             vm.backlogTypeId = prod.backlogTypeId;
                             //alert(prod.backlogTypeName);
                             return "test";
                             break;
                         }
                     }

                 }
                 if (colDef.name === app.localize('BacklogStatus')) {
                     for (var i = 0; i < $scope.backlogStatus.length; i++) {
                         var prod = $scope.backlogStatus[i];
                         if (prod.backlogStatusId === newValue) {

                             rowEntity.backlogStatus = prod.backlogStatusName;
                             vm.backlogStatusId = prod.backlogStatusId;
                             //alert(prod.backlogTypeName);
                             break;
                         }
                     }

                 }
                 if (colDef.name === app.localize('ApprovedBy')) {
                     for (var i = 0; i < $scope.approveduser.length; i++) {
                         var prod = $scope.approveduser[i];
                         if (prod.userId === newValue) {

                             rowEntity.approvedBy = prod.userName;
                             vm.approveduserId = prod.userId;
                             //alert(prod.backlogTypeName);
                             break;
                         }
                     }

                 }
                 if (colDef.name === app.localize('Who')) {
                     for (var i = 0; i < $scope.whoList.length; i++) {
                         var prod = $scope.whoList[i];
                         if (prod.userId === newValue) {

                             rowEntity.who = prod.userName;
                             vm.whouserId = prod.userId;
                             //alert(prod.backlogTypeName);
                             break;
                         }
                     }

                 }
                 if (colDef.name === app.localize('RequestFrom')) {
                     for (var i = 0; i < $scope.requesteduser.length; i++) {
                         var prod = $scope.requesteduser[i];
                         if (prod.userId === newValue) {

                             rowEntity.requestFrom = prod.userName;
                             vm.requesteduserId = prod.userId;
                             //alert(prod.backlogTypeName);
                             break;
                         }
                     }

                 }
             });
         };
                 
            vm.ProjectbacklogGridOptions.data= []
            //};

            vm.getProjectbacklog = function () {
               
                vm.loading = true;
                ProjectbacklogService.getProjectBacklog({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    projectId:projectId
                }).success(function (result) {
                    vm.ProjectbacklogGridOptions. rowTemplate= '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>';
                    vm.ProjectbacklogGridOptions . columnDefs= [
                {
                    name: app.localize('Actions'),
                    enableSorting: false,
                    width: 90,
                    cellTemplate:

                        '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button ng-if="grid.appScope.permissions.edit" ng-show="row.entity.enableEdit" class="btn btn-default btn-xs" ng-click="grid.appScope.save(row.entity)"><i class="fa fa-floppy-o font-blue"></i></button>' +
                             '  <button ng-if="grid.appScope.permissions.delete" class="btn btn-default btn-xs" ng-click="grid.appScope.deleteProjectbacklog(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                            '</div>'
                },
                {
                    name: app.localize('BacklogName'),
                    field: 'projectBacklogName',
                    minWidth: 140,
                    cellTemplate: cellToolTipTemplate
                },
                {
                    name: app.localize('ProjectName'),
                    field: 'projectName',
                    minWidth: 140,
                    enableCellEdit: false,
                    cellTemplate: cellToolTipTemplate
                },
                {
                    name: app.localize('BacklogType'),
                    field: 'backlogType',
                    minWidth: 180,
                    cellTemplate: cellToolTipTemplate,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownOptionsArray: $scope.backlogType,
                    cellTemplate: cellToolTipTemplate,
                    editDropdownIdLabel: 'backlogTypeId',
                    editDropdownValueLabel: 'backlogTypeName'
                },
                {
                    name: app.localize('BacklogPriority'),
                    field: 'backlogPriority',
                    minWidth: 180,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownOptionsArray: $scope.backlogProirity1,
                    cellTemplate: cellToolTipTemplate,
                    editDropdownIdLabel: 'backlogPriorityId',
                    editDropdownValueLabel: 'backlogPriorityName'
                },
                {
                    name: app.localize('Sprint'),
                    field: 'sprint',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate
                },
                {
                    name: app.localize('BacklogStatus'),
                    field: 'backlogStatus',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownOptionsArray: $scope.backlogStatus,
                    cellTemplate: cellToolTipTemplate,
                    editDropdownIdLabel: 'backlogStatusId',
                    editDropdownValueLabel: 'backlogStatusName'
                },
                {
                    name: app.localize('Where'),
                    field: 'where',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate
                },
                {
                    name: app.localize('Who'),
                    field: 'who',
                    minWidth: 180,
                    cellTemplate: cellToolTipTemplate,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownOptionsArray: $scope.whoList,
                    cellTemplate: cellToolTipTemplate,
                    editDropdownIdLabel: 'userId',
                    editDropdownValueLabel: 'userName'
                },
                {
                    name: app.localize('What'),
                    field: 'what',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate
                },
                {
                    name: app.localize('Why'),
                    field: 'why',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate
                },
                {
                    name: app.localize('When'),
                    field: 'when',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate
                },
                {
                    name: app.localize('Notes'),
                    field: 'notes',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate
                },
                {
                    name: app.localize('TotalHousrs'),
                    field: 'totalHousrs',
                    minWidth: 180,
                    cellTemplate: cellToolTipTemplate,
                    type: 'number'
                },
                {
                    name: app.localize('RequestFrom'),
                    field: 'requestFrom',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownOptionsArray: $scope.requesteduser,
                    cellTemplate: cellToolTipTemplate,
                    editDropdownIdLabel: 'userId',
                    editDropdownValueLabel: 'userName'
                },
                {
                    name: app.localize('RequestDate'),
                    field: 'requestDate',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate,
                    type: 'date',
                    cellFilter: 'date:"yyyy-MM-dd"',
                },
                {
                    name: app.localize('ApprovedBy'),
                    field: 'approvedBy',
                    minWidth: 120,
                    cellTemplate: cellToolTipTemplate,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownOptionsArray: $scope.approveduser,
                    cellTemplate: cellToolTipTemplate,
                    editDropdownIdLabel: 'userId',
                    editDropdownValueLabel: 'userName'
                },
                {
                    name: app.localize('ApprovedDate'),
                    field: 'approvedDate',
                    minWidth: 150,
                    cellTemplate: cellToolTipTemplate,
                    type: 'date',
                    cellFilter: 'date:"yyyy-MM-dd"',
                }
                    ];
                
                    vm.ProjectbacklogGridOptions.totalItems = result.totalCount;
                    vm.ProjectbacklogGridOptions.data = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editProjectbacklog = function (Projectbacklog) {
                openProjectbacklogModal(Projectbacklog.id);
            };

            vm.openCreateProjectbacklogModal = function () {
                openProjectbacklogModal(null);
            };

            function openProjectbacklogModal(ProjectbacklogId) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Project/CreateProjectBacklog.cshtml',
                    controller: 'tenant.views.Project.CreateProjectBacklog as vm',
                    backdrop: 'static',
                    resolve: {
                        ProjectbacklogId: function () {
                            return ProjectbacklogId;
                        },
                        projectId: function () {
                            return projectId;
                        }
                    }
                });

                modalInstance.result.then(function () {

                    vm.getProjectbacklog();
                });
            }


            vm.deleteProjectbacklog = function (Projectbacklog) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteProjectbacklog', Projectbacklog.ProjectbacklogName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ProjectbacklogService.deleteProjectBacklog({
                                id: Projectbacklog.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getProjectbacklog();
                            });
                        }
                    }
                );
            };

            function init() {
                ProjectbacklogService.getProjectBacklogForEdit({
                    id: 0
                }).success(function (result) {
                    $scope.backlogProirity1 = result.backlogProirity;
                    vm.project = result.project;
                    $scope.backlogStatus = result.backlogStatus;
                    $scope.backlogType = result.backlogType;
                    $scope.whoList = result.users;
                    $scope.requesteduser = result.users;
                    $scope.approveduser = result.users;
                    vm.getProjectbacklog();
                })
            }
            vm.backlogTypeId = null;
            vm.backlogStatusId=null;
            vm.backlogProirityId = null;
            vm.whouserId = null;
            vm.approveduserId = null;
            vm.resquesteduserId = null;
            vm.projectbacklogupdate = {};
            vm.save = function (projectbacklog) {
                
                vm.projectbacklogupdate = projectbacklog;
                if (vm.backlogTypeId != null) {
                    vm.projectbacklogupdate.backlogTypeId = vm.backlogTypeId;
                }
                if (vm.backlogStatusId != null)
                {
                    vm.projectbacklogupdate.backlogStatusId = vm.backlogStatusId;

                }
                if (vm.backlogProirityId != null) {
                    vm.projectbacklogupdate.backlogPriorityId = vm.backlogProirityId;
                }
                if (vm.whouserId != null) {
                      vm.projectbacklogupdate.whoId = vm.whouserId;
                }
                if (vm.approveduserId != null)
                {
                    vm.projectbacklogupdate.approvedBy = vm.approveduserId;
                }
                if (vm.requesteduserId != null)
                {
                    vm.projectbacklogupdate.requestFrom = vm.requesteduserId;
                }
               
                ProjectbacklogService.createOrUpdateProjectBacklog(vm.projectbacklogupdate).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.getProjectbacklog();

                });
            };

            init();
            
        }
    ]);
})();