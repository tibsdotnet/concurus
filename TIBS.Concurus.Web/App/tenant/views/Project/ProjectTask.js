﻿(function () {
    appModule.controller('tenant.views.Project.ProjectTask', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.project',
        function ($scope, $modal, uiGridConstants, ProjectTaskService) {
            var vm = this;
            var projectId = $scope.projectId;
            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.exportToExcel = function () {
                ProjectTaskService.getProjectTaskForExcel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    projectId: projectId
                })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createprojecttask: abp.auth.hasPermission('Pages.Tenant.Project.ProjectTask.CreateProjectTask'),
                'edit': abp.auth.hasPermission('Pages.Tenant.Project.ProjectTask.EditProjectTask'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Project.ProjectTask.DeleteProjectTask')
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.getProjectTask = function () {
                ProjectTaskService.getProjectTask({ filter: vm.filterText }).success(function (result) {
                    vm.countries = result.items;
                });
            }

            vm.ProjectTaskGridOptions = {};
            vm.ProjectTaskGridOptions.enableHorizontalScrollbar= uiGridConstants.scrollbars.WHEN_NEEDED;
            vm.ProjectTaskGridOptions.enableVerticalScrollbar= uiGridConstants.scrollbars.WHEN_NEEDED;
            vm.ProjectTaskGridOptions.paginationPageSizes= app.consts.grid.defaultPageSizes;
            vm.ProjectTaskGridOptions.paginationPageSize= app.consts.grid.defaultPageSize;
            vm.ProjectTaskGridOptions.useExternalPagination= true;
            vm.ProjectTaskGridOptions.useExternalSorting= true;
            vm.ProjectTaskGridOptions.appScopeProvider = vm;

                 
            vm.ProjectTaskGridOptions.onRegisterApi = function (gridApi) {

                $scope.gridApi = gridApi;
                $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                    if (!sortColumns.length || !sortColumns[0].field) {
                        requestParams.sorting = null;
                    } else {
                        requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                    }
                    vm.getProjectTask();
                });
                gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                    requestParams.skipCount = (pageNumber - 1) * pageSize;
                    requestParams.maxResultCount = pageSize;

                    vm.getProjectTask();
                });
                gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                    colDef.cellClass = function (uigrid, row, col, rowRenderIndex, colRenderIndex) {
                        if (rowEntity.id === row.entity.id && newValue !== oldValue) {
                            //alert(rowEntity.id);
                            //alert(row.entity.id);
                            rowEntity.enableEdit = true;
                            return "test";
                        }
                        return "";
                    };
                    $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
                    if (colDef.name === app.localize('AssignedUser')) {
                        for (var i = 0; i < $scope.assignedUser.length; i++) {
                            var prod = $scope.assignedUser[i];
                            if (prod.userId === newValue) {

                                rowEntity.assignedUser = prod.userName;
                                vm.assignedUserId = prod.userId;
                                //alert(prod.backlogTypeName);
                                break;
                            }
                        }

                        
                    }
                    if (colDef.name === app.localize('ProductBacklog')) {
                        for (var i = 0; i < $scope.projectBacklog.length; i++) {
                            var prod = $scope.projectBacklog[i];
                            if (prod.productBacklogId === newValue) {
                                //alert(prod.projectBacklogName);

                                rowEntity.productBacklog = prod.projectBacklogName;
                                vm.productBacklogId = prod.productBacklogId;
                                //alert(prod.backlogTypeName);
                                break;
                            }
                        }

                    }
                    if (colDef.name === app.localize('Status')) {
                        for (var i = 0; i < $scope.backlogStatus.length; i++) {
                            var prod = $scope.backlogStatus[i];
                            if (prod.backlogStatusId === newValue) {

                                rowEntity.status = prod.backlogStatusName;
                                vm.backlogStatusId = prod.backlogStatusId;
                                //alert(prod.backlogTypeName);
                                break;
                            }
                        }

                    }
                });
            }



            vm.getProjectTask = function () {
                vm.loading = true;
                ProjectTaskService.getProjectTask({

                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    projectId: projectId
                }).success(function (result) {

                    vm.ProjectTaskGridOptions.rowTemplate = '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                    vm.ProjectTaskGridOptions.columnDefs = [
                        {
                            name: app.localize('Actions'),
                            enableSorting: false,
                            width: 90,
                            cellTemplate:

                                '<div class=\"ui-grid-cell-contents text-center\">' +
                                    '  <button ng-if="grid.appScope.permissions.edit" ng-show="row.entity.enableEdit" class="btn btn-default btn-xs" ng-click="grid.appScope.save(row.entity)"><i class="fa fa-floppy-o font-blue"></i></button>' +
                                     '  <button ng-if="grid.appScope.permissions.delete" class="btn btn-default btn-xs" ng-click="grid.appScope.deleteProjectTask(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                    '</div>'
                        },
                        {
                            name: app.localize('TaskName'),
                            field: 'taskName',
                            minWidth: 140,
                            cellTemplate: cellToolTipTemplate
                        },
                        {
                            name: app.localize('ProductBacklog'),
                            field: 'productBacklog',
                            minWidth: 180,
                            cellTemplate: cellToolTipTemplate,
                            editableCellTemplate: 'ui-grid/dropdownEditor',
                            editDropdownOptionsArray: $scope.projectBacklog,
                            cellTemplate: cellToolTipTemplate,
                            editDropdownIdLabel: 'productBacklogId',
                            editDropdownValueLabel: 'projectBacklogName'
                        },
                        {
                            name: app.localize('AssignedUser'),
                            field: 'assignedUser',
                            minWidth: 180,
                            cellTemplate: cellToolTipTemplate,
                            editableCellTemplate: 'ui-grid/dropdownEditor',
                            editDropdownOptionsArray: $scope.assignedUser,
                            cellTemplate: cellToolTipTemplate,
                            editDropdownIdLabel: 'userId',
                            editDropdownValueLabel: 'userName'
                        },
                        {
                            name: app.localize('EstimateHours'),
                            field: 'estimateHours',
                            minWidth: 120,
                            cellTemplate: cellToolTipTemplate
                        },
                        {
                            name: app.localize('AllotedHours'),
                            field: 'allotedHours',
                            minWidth: 120,
                            cellTemplate: cellToolTipTemplate
                        },
                        {
                            name: app.localize('ActualTime'),
                            field: 'actualTime',
                            minWidth: 120,
                            cellTemplate: cellToolTipTemplate
                        },
                        {
                            name: app.localize('StartDate'),
                            field: 'startDate',
                            minWidth: 180,
                            cellTemplate: cellToolTipTemplate,
                            type: 'date',
                            cellFilter: 'date:"yyyy-MM-dd"'
                        },
                        {
                            name: app.localize('EndDate'),
                            field: 'endDate',
                            minWidth: 120,
                            cellTemplate: cellToolTipTemplate,
                            type: 'date',
                            cellFilter: 'date:"yyyy-MM-dd"'
                        },
                        {
                            name: app.localize('remainingTime'),
                            field: 'remainingTime',
                            minWidth: 120,
                            cellTemplate: cellToolTipTemplate
                        },
                        {
                            name: app.localize('Status'),
                            field: 'status',
                            minWidth: 120,
                            cellTemplate: cellToolTipTemplate,
                            editableCellTemplate: 'ui-grid/dropdownEditor',
                            editDropdownOptionsArray: $scope.backlogStatus,
                            cellTemplate: cellToolTipTemplate,
                            editDropdownIdLabel: 'backlogStatusId',
                            editDropdownValueLabel: 'backlogStatusName'
                        },
                        {
                            name: app.localize('Remarks'),
                            field: 'remarks',
                            minWidth: 120,
                            cellTemplate: cellToolTipTemplate
                        }
                    ],

                     vm.ProjectTaskGridOptions.totalItems = result.totalCount;
                    vm.ProjectTaskGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editProjectTask = function (ProjectTask) {
                openProjectTaskModal(ProjectTask.id);
            };

            vm.openCreateProjectTaskModal = function () {
                openProjectTaskModal(null);
            };

            function openProjectTaskModal(ProjectTaskId) {


                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Project/CreateProjectTask.cshtml',
                    controller: 'tenant.views.Project.CreateProjectTask as vm',
                    backdrop: 'static',
                    resolve: {
                        ProjectTaskId: function () {
                            return ProjectTaskId;
                        },
                        projectId: function () {
                            return projectId;
                        }
                    }
                });

                modalInstance.result.then(function () {

                    vm.getProjectTask();
                });
            }


            vm.deleteProjectTask = function (ProjectTask) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteProjectTask', ProjectTask.ProjectTaskName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ProjectTaskService.deleteProjectTask({
                                id: ProjectTask.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getProjectTask();
                            });
                        }
                    }
                );
            };

            function init() {
                ProjectTaskService.getProjectTaskForEdit({
                    id: 0
                }).success(function (result) {
                    //$scope.backlogProirity1 = result.backlogProirity;
                    $scope.projectBacklog = result.projectBacklog;
                    $scope.assignedUser = result.user;
                    $scope.backlogStatus = result.backlogStatus;
                    //$scope.approveduser = result.users;
                    vm.getProjectTask();
                })
            }
            vm.save = function (projectTask) {
                vm.updateprojectTask = projectTask;
                if (vm.assignedUserId != null)
                {
                    vm.updateprojectTask.assignedUserId = vm.assignedUserId;
                }
                if (vm.productBacklogId != null)
                {
                    vm.updateprojectTask.productBacklogId = vm.productBacklogId;
                }
                if (vm.backlogStatusId != null) {
                    vm.updateprojectTask.statusId = vm.backlogStatusId;
                }
                ProjectTaskService.createOrUpdateProjectTask(vm.updateprojectTask).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    vm.getProjectTask();
                })
            };

            init();
            //vm.getProjectbacklog
            //vm.getProjectTask();
        }
    ]);
})();