﻿(function () {
    appModule.controller('tenant.views.Project.CreateProjectBacklog', [
        '$scope', '$modalInstance', '$modal', 'abp.services.app.project', 'ProjectbacklogId','projectId',
        function ($scope, $modalInstance, $modal, projectBacklogService, projectbacklogId, projectId) {
            var vm = this;
            vm.saving = false;
            vm.ProjectBacklog = {};
           
           // alert(projectId);
            vm.save = function () {
                vm.saving = true;
                vm.ProjectBacklog.projectId = projectId;
                vm.ProjectBacklog.backlogPriorityId = $scope.backlogProirity.selected.backlogPriorityId;
                vm.ProjectBacklog.backlogStatusId = $scope.backlogStatus.selected.backlogStatusId;
                vm.ProjectBacklog.backlogTypeId = $scope.backlogType.selected.backlogTypeId;

                projectBacklogService.createOrUpdateProjectBacklog(vm.ProjectBacklog).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                projectBacklogService.getProjectBacklogForEdit({
                    id: projectbacklogId
                }).success(function (result) {
                    //console.log(result.projectBacklog);
                    vm.ProjectBacklog = result.projectBacklog;
                    vm.project = result.project;
                    vm.backlogProirity = result.backlogProirity;
                    vm.backlogStatus = result.backlogStatus;
                    vm.backlogType = result.backlogType;
                    console.log(vm.project);
                    console.log(vm.backlogProirity);

                    
                    for (i = 0; i < vm.project.length; i++) {
                        var da = vm.project[i].projectId;
                       
                        if (da == vm.ProjectBacklog.projectId) {
                            $scope.project.selected = { projectId: vm.project[i].projectId, projectName: vm.project[i].projectName };
                        }
                    }
                    for (i = 0; i < vm.backlogProirity.length; i++) {
                        var da = vm.backlogProirity[i].backlogProirityId;
                        if (da == vm.ProjectBacklog.backlogProirityId) {
                            $scope.backlogProirity.selected = { backlogPriorityId: vm.backlogProirity[i].backlogPriorityId, backlogPriorityName: vm.backlogProirity[i].backlogPriorityName };
                        }
                    }
                    for (i = 0; i < vm.backlogStatus.length; i++) {
                        var da = vm.backlogStatus[i].backlogStatusId;
                        if (da == vm.ProjectBacklog.backlogStatusId) {
                            $scope.backlogStatus.selected = { backlogStatusId: vm.backlogStatus[i].backlogStatusId, backlogStatusName: vm.backlogStatus[i].backlogStatusName };
                        }
                    }
                    for (i = 0; i < vm.backlogType.length; i++) {
                        var da = vm.backlogType[i].backlogTypeId;
                        if (da == vm.ProjectBacklog.backlogTypeId) {
                            $scope.backlogType.selected = { backlogTypeId: vm.backlogType[i].backlogTypeId, backlogTypeName: vm.backlogType[i].backlogTypeName };
                        }
                    }
                });
            }

            init();
            $scope.project = {};
            $scope.backlogProirity = {};
            $scope.backlogStatus = {};
            $scope.backlogType = {};

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();