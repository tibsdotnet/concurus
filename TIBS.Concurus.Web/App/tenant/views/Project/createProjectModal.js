﻿(function () {
    appModule.controller('tenant.views.Project.createProjectModal', [
        '$scope', '$modalInstance', '$modal', 'abp.services.app.project', 'uiGridConstants', 'ProjectId',
        function ($scope, $modalInstance,$modal, ProjectService,uiGridConstants, ProjectId) {
            var vm = this;
            vm.saving = false;
            vm.Project = null;
           
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                ProjectId:ProjectId,
                sorting: null
            };
            vm.save = function () {
                vm.saving = true;
                vm.Project.technologyId = $scope.technology.selected.technologyId;
                vm.Project.companyId = $scope.company.selected.companyId;
                vm.Project.projectTypeId = $scope.projectType.selected.projectTypeId;
                vm.Project.teamId = $scope.team.selected.teamId;

                ProjectService.createOrUpdateProject(vm.Project).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                ProjectService.getProjectForEdit({
                    id: ProjectId
                }).success(function (result) {
                    //console.log(result.Project);
                    vm.Project = result.project;
                    vm.technology = result.technology;
                    vm.projectType = result.projectType;
                   
                    vm.company = result.company;
                    vm.team = result.team;
                    for (i = 0; i < vm.technology.length; i++) {
                        var da = vm.technology[i].technologyId;
                        if (da == vm.Project.technologyId) {
                            $scope.technology.selected = { technologyId: vm.technology[i].technologyId, technologyName: vm.technology[i].technologyName };
                        }
                    }
                    for (i = 0; i < vm.projectType.length; i++) {
                        var da = vm.projectType[i].projectTypeId;
                        if (da == vm.Project.projectTypeId) {
                            $scope.projectType.selected = { projectTypeId: vm.projectType[i].projectTypeId, projectTypeName: vm.projectType[i].projectTypeName };
                        }
                    }
                    for (i = 0; i < vm.company.length; i++) {
                        var da = vm.company[i].companyId;
                        if (da == vm.Project.companyId) {
                            $scope.company.selected = { companyId: vm.company[i].companyId, companyName: vm.company[i].companyName };
                        }
                    }
                    for (i = 0; i < vm.team.length; i++) {
                        var da = vm.team[i].teamId;
                        if (da == vm.Project.teamId) {
                            $scope.team.selected = { teamId: vm.team[i].teamId, teamName: vm.team[i].teamName };
                        }
                    }
                });
            }

            init();
            $scope.company = {};
            $scope.projectType = {};
            $scope.team = {};
            $scope.technology = {};
           
            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();