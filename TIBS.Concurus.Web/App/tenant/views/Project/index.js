﻿(function () {
    appModule.controller('tenant.views.Project.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.project', '$location', '$state',
        function ($scope, $modal, uiGridConstants, ProjectService, $location, $state) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.exportToExcel = function () {
                ProjectService.getProjectToExcel({ filter: vm.filterText })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createProject: abp.auth.hasPermission('Pages.Tenant.Project.CreateProject'),
                editProject: abp.auth.hasPermission('Pages.Tenant.Project.EditProject'),
                deleteProject: abp.auth.hasPermission('Pages.Tenant.Project.DeleteProject')
            };

            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.getProject = function () {
                ProjectService.getProject({ filter: vm.filterText }).success(function (result) {
                    vm.countries = result.items;
                });
            }

            vm.ProjectGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 90,
                        cellTemplate:

                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editProject(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.deleteProject(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('ProjectName'),
                        field: 'projectName',
                        minWidth: 140,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Description'),
                        field: 'description',
                        minWidth: 180,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Team'),
                        field: 'team',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Technology'),
                        field: 'technology',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Company'),
                        field: 'company',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ProjectType'),
                        field: 'projectType',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getProject();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getProject();
                    });
                },
                data: []
            };
            vm.project = null;
            vm.getProject = function () {
                vm.loading = true;
                ProjectService.getProjects({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {

                    vm.project = result.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };


            vm.openCreateProjectModal = function () {
                openProjectModal(null);
            };

            function openProjectModal(ProjectId) {

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Project/createProjectModal.cshtml',
                    controller: 'tenant.views.Project.createProjectModal as vm',
                    backdrop: 'static',
                    resolve: {
                        ProjectId: function () {
                            return ProjectId;
                        }
                    }
                });

                modalInstance.result.then(function () {

                    vm.getProject();
                });
            }


            vm.deleteProject = function (project) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteProject'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ProjectService.deleteProject({
                                id: project.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getProject();
                            });
                        }
                    }
                );
            };

            vm.editProject = function (project) {
                $location.path('/ProjectdetailsforEdit/' + project.id);
                // $state.go('ProjectdetailsforEdit');

            };
            //$(document.body).on('click', '.projectbtn', function () {
            //    var id = $(this).data('id');

            //    vm.editProject(id);
            //});

            vm.getProject();
        }
    ]);
})();