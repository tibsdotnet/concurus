﻿(function () {
    appModule.controller('tenant.views.Team.createEmployee', [
        '$scope', '$modalInstance', 'abp.services.app.employee', 'EmployeeId',
        function ($scope, $modalInstance, employeeService, EmployeeId) {
            var vm = this;
            vm.saving = false;
            vm.employee = null;
            
           
            vm.location = [];
            function init() {
                employeeService.getEmployeeForEdit({
                    id: EmployeeId
                }).success(function (result) {
                    vm.employee = result.employee;
                    vm.location = result.loaction;
                    
                    for (i = 0; i < vm.location.length; i++) {
                        var da = vm.location[i].locationId;
                        if (da == vm.employee.locationId) {
                            vm.employee.locationName = vm.location[i].locationName;
                        }

                    }
                    console.log(vm.employee);
                });
            }

          init();

            vm.cancel = function () {
                $modalInstance.close();
            };
           
           
        }
    ]);
})();