﻿(function () {
    appModule.controller('tenant.views.team.index', [
        '$scope', '$modal', 'abp.services.app.team',
        function ($scope, $modal, teamService) {
            var vm = this;

            vm.Team = [];
            vm.filterText = '';

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.getteam = function () {
                teamService.getTeam({ filter: vm.filterText }).success(function (result) {
                    vm.Team = result.items;
                });
            }
            vm.openCreateteam = function () {
                openteam(null);
            };
            vm.openEditTeam = function (team) {
                openteam(team.id);
                
            };  

            vm.exportToExcel = function () {
                teamService.getTeamToExcel({ filter: vm.filterText })
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createteam: abp.auth.hasPermission('Pages.Tenant.team.Createteam'),
                editteam: abp.auth.hasPermission('Pages.Tenant.team.Editteam'),
                deleteteam: abp.auth.hasPermission('Pages.Tenant.team.Deleteteam')
            };
            function openteam(teamId) {

                
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Team/createOrEdit.cshtml',
                    controller: 'tenant.views.team.createOrEdit as vm',
                    backdrop: 'static',
                    resolve: {
                        teamId: function () {
                            return teamId;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getteam();
                });
            }
            vm.deleteteam = function (prole) {
                abp.message.confirm(
                    app.localize('AreYouSureToteam', prole.pRoleName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            teamService.deleteteam({
                                id: prole.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getteam();
                            });
                        }
                    }
                );
            };
            vm.teamemployees = [];
            vm.getTeamMember = function (team) {
                

                if (team == vm.teamMembers) {
                    vm.teamMembers = null;
                } else {
                    vm.teamMembers = team;
                }
                teamService.getTeamMembers({
                    id: team.id
                }).success(function (result) {
                    //console.log(result);
                    vm.teamemployees = result.items;
                    console.log(vm.teamemployees);
                });
            };
            vm.deleteTeamEmployee = function (team) {
                abp.message.confirm(
                   app.localize('AreYouSureToteamMember', team.firstName),
                   function (isConfirmed) {
                       if (isConfirmed) {
                           teamService.deleteTeamEmployee({
                               id: team.id
                           }).success(function () {
                               abp.notify.success(app.localize('SuccessfullyDeleted'));
                               vm.getTeamMember();
                           });
                       }
                   }
               );

            };
           vm.viewemp=function(team) {
                var EmployeeId = team.userId;


                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Team/CreateEmployee.cshtml',
                    controller: 'tenant.views.Team.createEmployee as vm',
                   
                    backdrop: 'static',
                    resolve: {
                        EmployeeId: function () {
                            return EmployeeId;
                        }
                    }
                });

                
            }
           


          


            vm.getteam();
        }
    ]);
})();