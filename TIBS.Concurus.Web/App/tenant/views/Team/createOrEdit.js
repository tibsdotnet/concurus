﻿(function () {
    appModule.controller('tenant.views.team.createOrEdit', [
        '$scope', '$modalInstance', 'abp.services.app.team', 'teamId',
        function ($scope, $modalInstance, teamService, teamId) {
            var vm = this;
            vm.saving = false;
            vm.employees = [];
            vm.team = {};

            vm.save = function () {
                var assignedEmployee = _.map(
                    _.where(vm.employees, { isAssigned: true }), //Filter assigned employee
                    function (employee) {
                        return employee.employeeId; //Get names
                    });
                vm.saving = true;
                vm.team.id = teamId;
                vm.team.employeeId = assignedEmployee;
                console.log(vm.team.employeeId);
                teamService.createOrUpdateTeam(vm.team).success(function () {
                    
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                teamService.getTeamForEdit({
                    id: teamId
                }).success(function (result) {
                    vm.team = result.team;
                    vm.employees = result.employee;
                    console.log(vm.employees);
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();