﻿(function () {
    appModule.controller('tenant.views.Technology.index', [
        '$scope', '$modal', 'abp.services.app.technology',
        function ($scope, $modal, Technology) {
            var vm = this;

            vm.Technology = [];
            vm.filterText = '';

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.getTechnology = function () {
                Technology.getTechnogies({ filter: vm.filterText }).success(function (result) {
                    vm.Technology = result.items;
                });
            }
            vm.openCreateTechnology = function () {
                openTechnology(null);
            };
            vm.openEditTechnology = function (technology) {
                openTechnology(technology.id);
                
            };  

            vm.exportToExcel = function () {
                Technology.getTechnogyToExcel({ filter: vm.filterText })
                    .success(function (result) {    
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createtechnology: abp.auth.hasPermission('Pages.Tenant.Technology.CreateTechnology'),
                edittechnology: abp.auth.hasPermission('Pages.Tenant.Technology.EditTechnology'),
                deletetechnology: abp.auth.hasPermission('Pages.Tenant.Technology.DeleteTechnology')
            };
            function openTechnology(technologyid) {
                //(proleid);
                
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Technology/createOrEdit.cshtml',
                    controller: 'tenant.views.Technology.createOrEdit as vm',
                    backdrop: 'static',
                    resolve: {
                        technologyid: function () {
                            return technologyid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getTechnology();
                });
            }
            vm.deleteTechnology = function (technology) {
                abp.message.confirm(
                    app.localize('AreYouSureToTechnology', technology.technologyName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            Technology.deleteTechnology({
                                id: prole.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getTechnology();
                            });
                        }
                    }
                );
            };
           


          


            vm.getTechnology();
        }
    ]);
})();