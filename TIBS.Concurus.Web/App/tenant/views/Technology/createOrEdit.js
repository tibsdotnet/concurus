﻿(function () {
    appModule.controller('tenant.views.Technology.createOrEdit', [
        '$scope', '$modalInstance', 'abp.services.app.technology', 'technologyid',
        function ($scope, $modalInstance, Technology, technologyid) {
            var vm = this;
            vm.saving = false;
            vm.Technology = null;

            vm.save = function () {
                
                vm.saving = true;
                vm.Technology.id = technologyid;
                Technology.createOrUpdateTechnology(vm.Technology).success(function () {
                    
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                Technology.getTechnologyForEdit({
                    id: technologyid
                }).success(function (result) {
                    vm.Technology = result.technology;
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();