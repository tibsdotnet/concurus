using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using TIBS.Concurus.Web.Controllers;

namespace TIBS.Concurus.Web.Areas.Mpa.Controllers
{
    [AbpMvcAuthorize]
    public class WelcomeController : ConcurusControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}