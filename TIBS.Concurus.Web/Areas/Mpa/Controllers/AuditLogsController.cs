﻿using System.Web.Mvc;
using Abp.Auditing;
using Abp.Web.Mvc.Authorization;
using TIBS.Concurus.Authorization;
using TIBS.Concurus.Web.Controllers;

namespace TIBS.Concurus.Web.Areas.Mpa.Controllers
{
    [DisableAuditing]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_AuditLogs)]
    public class AuditLogsController : ConcurusControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}