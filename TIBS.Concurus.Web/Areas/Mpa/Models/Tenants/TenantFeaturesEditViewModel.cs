using Abp.AutoMapper;
using TIBS.Concurus.MultiTenancy;
using TIBS.Concurus.MultiTenancy.Dto;
using TIBS.Concurus.Web.Areas.Mpa.Models.Common;

namespace TIBS.Concurus.Web.Areas.Mpa.Models.Tenants
{
    [AutoMapFrom(typeof (GetTenantFeaturesForEditOutput))]
    public class TenantFeaturesEditViewModel : GetTenantFeaturesForEditOutput, IFeatureEditViewModel
    {
        public Tenant Tenant { get; set; }

        public TenantFeaturesEditViewModel(Tenant tenant, GetTenantFeaturesForEditOutput output)
        {
            Tenant = tenant;
            output.MapTo(this);
        }
    }
}