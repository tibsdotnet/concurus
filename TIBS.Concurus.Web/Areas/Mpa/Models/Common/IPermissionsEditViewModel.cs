using System.Collections.Generic;
using TIBS.Concurus.Authorization.Dto;

namespace TIBS.Concurus.Web.Areas.Mpa.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }

        List<string> GrantedPermissionNames { get; set; }
    }
}