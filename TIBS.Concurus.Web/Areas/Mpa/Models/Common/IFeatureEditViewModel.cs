using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TIBS.Concurus.Editions.Dto;

namespace TIBS.Concurus.Web.Areas.Mpa.Models.Common
{
    public interface IFeatureEditViewModel
    {
        List<NameValueDto> FeatureValues { get; set; }

        List<FlatFeatureDto> Features { get; set; }
    }
}