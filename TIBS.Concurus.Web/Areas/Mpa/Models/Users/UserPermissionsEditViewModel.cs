using Abp.AutoMapper;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.Authorization.Users.Dto;
using TIBS.Concurus.Web.Areas.Mpa.Models.Common;

namespace TIBS.Concurus.Web.Areas.Mpa.Models.Users
{
    [AutoMapFrom(typeof(GetUserPermissionsForEditOutput))]
    public class UserPermissionsEditViewModel : GetUserPermissionsForEditOutput, IPermissionsEditViewModel
    {
        public User User { get; private set; }

        public UserPermissionsEditViewModel(GetUserPermissionsForEditOutput output, User user)
        {
            User = user;
            output.MapTo(this);
        }
    }
}