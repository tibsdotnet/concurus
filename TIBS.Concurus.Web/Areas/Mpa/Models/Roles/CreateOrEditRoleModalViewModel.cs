﻿using Abp.AutoMapper;
using TIBS.Concurus.Authorization.Roles.Dto;
using TIBS.Concurus.Web.Areas.Mpa.Models.Common;

namespace TIBS.Concurus.Web.Areas.Mpa.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class CreateOrEditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool IsEditMode
        {
            get { return Role.Id.HasValue; }
        }

        public CreateOrEditRoleModalViewModel(GetRoleForEditOutput output)
        {
            output.MapTo(this);
        }
    }
}