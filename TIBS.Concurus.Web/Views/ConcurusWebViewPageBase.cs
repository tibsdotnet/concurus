﻿using Abp.Configuration;
using Abp.Dependency;
using Abp.Web.Mvc.Views;

namespace TIBS.Concurus.Web.Views
{
    public abstract class ConcurusWebViewPageBase : ConcurusWebViewPageBase<dynamic>
    {

    }

    public abstract class ConcurusWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        public ISettingManager SettingManager { get; set; }

        protected ConcurusWebViewPageBase()
        {
            LocalizationSourceName = ConcurusConsts.LocalizationSourceName;
            SettingManager = IocManager.Instance.Resolve<ISettingManager>();
        }
    }
}