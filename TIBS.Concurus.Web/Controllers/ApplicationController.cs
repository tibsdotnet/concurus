﻿using System.Web.Mvc;
using Abp.Auditing;
using Abp.Web.Mvc.Authorization;

namespace TIBS.Concurus.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ApplicationController : ConcurusControllerBase
    {
        [DisableAuditing]
        public ActionResult Index()
        {
            return View("~/App/common/views/layout/layout.cshtml"); //Layout of the angular application.
        }
    }
}