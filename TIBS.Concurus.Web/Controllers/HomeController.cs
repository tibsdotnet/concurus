﻿using System.Web.Mvc;

namespace TIBS.Concurus.Web.Controllers
{
    public class HomeController : ConcurusControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}