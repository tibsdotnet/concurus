﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abp.AutoMapper;
using TIBS.Concurus.EntityFramework;
using System.Threading.Tasks;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.Storage;
using Abp.Authorization.Users;
using Abp.IO.Extensions;
using Abp.Domain.Uow;
using TIBS.Concurus.Employees.Dto;
namespace TIBS.Concurus.Web.Controllers
{
    public class FileUploadController : ConcurusControllerBase
    {
       public static int employeeId;
       ConcurusDbContext db = new ConcurusDbContext();
       private readonly UserManager _userManager;
       private readonly IBinaryObjectManager _binaryObjectManager;
        //
        // GET: /FileUpload/
       public FileUploadController( UserManager userManager, IBinaryObjectManager binaryObjectManager)
       {
           
           _userManager = userManager;
           _binaryObjectManager = binaryObjectManager;
       }
        public ActionResult Index()
        {
            return View();
        }

        public async virtual Task UploadEmployeeProfileImage(int Id)
        {
            employeeId = Id;
            try
            {
                if (employeeId == 0)
                {
                    employeeId = (db.Employees.Select(x => (int?)x.Id).Max() ?? 0);

                }
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
                }
                var file = Request.Files[0];
                string filename = file.FileName;
                string path = System.Web.HttpContext.Current.Server.MapPath("~/EmployeeProfilePicture/" + employeeId + "");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                path = Path.Combine(Server.MapPath("~/EmployeeProfilePicture/" + employeeId + ""), filename);
                //path = Path.Combine(Server.MapPath("~/Photos"), empId + ".jpg");
                file.SaveAs(path);
                path = Url.Content(Path.Combine("/EmployeeProfilePicture/" + employeeId + "/" + filename));
                CreateEmployeeInput input = new CreateEmployeeInput();


                var employeeInput = db.Employees.Where(p => p.Id == employeeId).FirstOrDefault();
                employeeInput.PhotoPath = path;
                // var contactcompany = employeeInput.MapTo<CompanyContact>();
                db.SaveChanges();

                long UserId = (long)employeeInput.UserId;

                //await ChangeUserProfilePicture(UserId);

                var user = await _userManager.GetUserByIdAsync(UserId);
                //Delete old picture
                if (user.ProfilePictureId.HasValue)
                {
                    await _binaryObjectManager.DeleteAsync(user.ProfilePictureId.Value);
                }

                //Save new picture
                var storedFiles = new BinaryObject(file.InputStream.GetAllBytes());
                await _binaryObjectManager.SaveAsync(storedFiles);
                user.ProfilePictureId = storedFiles.Id;
            }
            catch (Exception ex)
            {

            }
           
        }
        //[UnitOfWork]
        //public async Task ChangeUserProfilePicture(long Id)
        //{
        //    var file = Request.Files[0];
        //    var user = await _userManager.GetUserByIdAsync(Id);
        //    //Delete old picture
        //    if (user.ProfilePictureId.HasValue)
        //    {
        //        await _binaryObjectManager.DeleteAsync(user.ProfilePictureId.Value);
        //    }

        //    //Save new picture
        //    var storedFile = new BinaryObject(file.InputStream.GetAllBytes());
        //    await _binaryObjectManager.SaveAsync(storedFile);
        //    user.ProfilePictureId = storedFile.Id;
        //}
        
	}
}