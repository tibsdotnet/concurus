﻿using System.Web.Mvc;
using Abp.Auditing;

namespace TIBS.Concurus.Web.Controllers
{
    public class ErrorController : ConcurusControllerBase
    {
        [DisableAuditing]
        public ActionResult E404()
        {
            return View();
        }
    }
}