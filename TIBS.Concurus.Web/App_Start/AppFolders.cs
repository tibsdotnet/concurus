﻿using Abp.Dependency;

namespace TIBS.Concurus.Web
{
    public class AppFolders : IAppFolders, ISingletonDependency
    {
        public string TempFileDownloadFolder { get; set; }
    }
}