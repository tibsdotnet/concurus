﻿using Abp.Application.Navigation;
using Abp.Localization;
using TIBS.Concurus.Authorization;

namespace TIBS.Concurus.Web.Navigation
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class AppNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Tenants,
                    L("Tenants"),
                    url: "host.tenants",
                    icon: "icon-globe",
                    requiredPermissionName: AppPermissions.Pages_Tenants
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Editions,
                    L("Editions"),
                    url: "host.editions",
                    icon: "icon-grid",
                    requiredPermissionName: AppPermissions.Pages_Editions
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Dashboard,
                    L("Dashboard"),
                    url: "tenant.dashboard",
                    icon: "icon-home",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.ProjectRole,
                    L("ProjectRole"),
                    url: "tenant.projectrole",
                    icon: "fa fa-street-view",
                    requiredPermissionName: AppPermissions.Pages_Tenant_ProjectRole
                    ))

                    .AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Geography,
                    L("Geography"),
                    icon: "icon-globe",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Geography
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Country,
                    L("Country"),
                    url: "country",
                    icon: "fa fa-futbol-o",
                     requiredPermissionName: AppPermissions.Pages_Tenant_Geography_Country

                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.City,
                        L("City"),
                        url: "city",
                        icon: "fa fa-check",
                requiredPermissionName: AppPermissions.Pages_Tenant_Geography_City
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Location,
                        L("Location"),
                        url: "location",
                        icon: "fa fa-location-arrow",
                requiredPermissionName: AppPermissions.Pages_Tenant_Geography_Location
                        )
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Employee,
                    L("Employee"),
                    url: "employee",
                   icon: "fa fa-user",
                   requiredPermissionName:AppPermissions.Pages_Tenant_Employee
                   ))
                   .AddItem(new MenuItemDefinition(
                       PageNames.App.Tenant.Addressbook,
                       L("Addressbook"),
                       icon: "fa fa-map-marker",
                       requiredPermissionName:AppPermissions.Pages_Tenant_Addressbook)
                       .AddItem(new MenuItemDefinition(
                           PageNames.App.Tenant.Company,
                           L("Company"),
                           icon: "fa fa-building",
                           url: "company",
                           requiredPermissionName:AppPermissions.Pages_Tenant_Addressbook_Compnay)))
                           .AddItem(new MenuItemDefinition(
                               PageNames.App.Tenant.Project,
                               L("Project"),
                               icon: "fa fa-caret-square-o-down",
                               requiredPermissionName: AppPermissions.Pages_Tenant_Projects)
                           .AddItem(new MenuItemDefinition(
                               PageNames.App.Tenant.Technology,
                               L("Technology"),
                               icon: "fa fa-caret-square-o-up",
                               url: "Technology",
                               requiredPermissionName:AppPermissions.Pages_Tenant_Technology))
                   .AddItem(new MenuItemDefinition(
                       PageNames.App.Tenant.Team,
                       L("Team"),
                       url: "team",
                       icon: "fa fa-users",
                       requiredPermissionName:AppPermissions.Pages_Tenant_Team))
                       .AddItem(new MenuItemDefinition(
                           PageNames.App.Tenant.ProjectType,
                           L("ProjectType"),
                           url: "ProjectType",
                           icon: "fa fa-anchor",
                           requiredPermissionName: AppPermissions.Pages_Tenant_Project_ProjectType))
                           .AddItem(new MenuItemDefinition(
                               PageNames.App.Tenant.Project,
                               L("Project"),
                               url: "project",
                               icon: "fa fa-shopping-cart",
                               requiredPermissionName: AppPermissions.Pages_Tenant_Project))
                               .AddItem(new MenuItemDefinition(
            PageNames.App.Tenant.ProjectTeam,
            L("ProjectTeam"),
             url: "projectteam",
            icon: "fa fa-futbol-o",
            requiredPermissionName: AppPermissions.Pages_Tenant_Project_ProjectTeam)))
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Common.Administration,
                    L("Administration"),
                    icon: "icon-wrench"
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Roles,
                        L("Roles"),
                        url: "roles",
                        icon: "icon-briefcase",
                        requiredPermissionName: AppPermissions.Pages_Administration_Roles
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Users,
                        L("Users"),
                        url: "users",
                        icon: "icon-users",
                        requiredPermissionName: AppPermissions.Pages_Administration_Users
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Languages,
                        L("Languages"),
                        url: "languages",
                        icon: "icon-flag",
                        requiredPermissionName: AppPermissions.Pages_Administration_Languages
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.AuditLogs,
                        L("AuditLogs"),
                        url: "auditLogs",
                        icon: "icon-lock",
                        requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Settings,
                        L("Settings"),
                        url: "host.settings",
                        icon: "icon-settings",
                        requiredPermissionName: AppPermissions.Pages_Administration_Host_Settings
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Settings,
                        L("Settings"),
                        url: "tenant.settings",
                        icon: "icon-settings",
                        requiredPermissionName: AppPermissions.Pages_Administration_Tenant_Settings
                        )
                    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ConcurusConsts.LocalizationSourceName);
        }
    }
}
