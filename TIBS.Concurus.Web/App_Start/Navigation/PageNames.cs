namespace TIBS.Concurus.Web.Navigation
{
    public static class PageNames
    {
        public static class App
        {
            public static class Common
            {
                public const string Administration = "Administration";
                public const string Roles = "Administration.Roles";
                public const string Users = "Administration.Users";
                public const string AuditLogs = "Administration.AuditLogs";
                public const string Languages = "Administration.Languages";
            }

            public static class Host
            {
                public const string Tenants = "Tenants";
                public const string Editions = "Editions";
                public const string Settings = "Administration.Settings.Host";
            }

            public static class Tenant
            {
                public const string Dashboard = "Dashboard.Tenant";
                public const string Settings = "Administration.Settings.Tenant";
                public const string ProjectRole = "ProjectRole";
                public const string Geography = "Geography";
                public const string Country = "Geography.Country";
                public const string City = "Geography.City";
                public const string Location = "Geography.Location";
                public const string Employee = "Employee";
                public const string Team = "Team";
                public const string Addressbook = "AddressBook";
                public const string Company = "AddressBook.Company";
                public const string Technology = "Technology";
                public const string ProjectType = "ProjectType";
                public const string Project = "Project";
                public const string ProjectTeam = "ProjectTeam";

            }
        }

        public static class Frontend
        {
            public const string Home = "Frontend.Home";
            public const string About = "Frontend.About";
        }
    }
}