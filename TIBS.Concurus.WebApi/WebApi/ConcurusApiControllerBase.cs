using Abp.WebApi.Controllers;

namespace TIBS.Concurus.WebApi
{
    public abstract class ConcurusApiControllerBase : AbpApiController
    {
        protected ConcurusApiControllerBase()
        {
            LocalizationSourceName = ConcurusConsts.LocalizationSourceName;
        }
    }
}