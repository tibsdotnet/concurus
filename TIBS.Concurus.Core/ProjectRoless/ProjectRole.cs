﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.ProjectRoless
{
    [Table("ProjectRole")]
    public class ProjectRole: FullAuditedEntity
    {
        public virtual string PRoleCode { get; set; }
        public virtual string PRoleName { get; set; }
    }
}
