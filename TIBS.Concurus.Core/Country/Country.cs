﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace TIBS.Concurus.Countryy
{
    [Table("Country")]
    public class Country : FullAuditedEntity
    {
        public const int NameLength = 100;
        public const int CodeLength = 15;
        public const int ISDLength = 15;
        [Required]
        [MaxLength(NameLength)]
        public virtual string CountryName { get; set; }

        [Required]
        [MaxLength(CodeLength)]
        public virtual string CountryCode { get; set; }

        [MaxLength(ISDLength)]
        public virtual string ISDCode { get; set; }
    }
}
