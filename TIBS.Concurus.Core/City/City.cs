﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using TIBS.Concurus.Countryy;

namespace TIBS.Concurus.Cityy
{
    [Table("City")]
    public class City:FullAuditedEntity
    {
        public const int NameLength = 100;
        public const int CodeLength = 15;
        public const int ISDLength = 15;
        [Required]
        [MaxLength(NameLength)]
        public virtual string CityName { get; set; }

        [Required]
        [MaxLength(CodeLength)]
        public virtual string CityCode { get; set; }

        [MaxLength(ISDLength)]
        public virtual string ISDCode { get; set; }

        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }
        [Required]
        public virtual int CountryId { get; set; }

    }
}
