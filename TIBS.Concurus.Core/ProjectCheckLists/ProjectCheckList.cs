﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.ProjectImportants;
using TIBS.Concurus.Projects;

namespace TIBS.Concurus.ProjectCheckLists
{
    [Table("ProjectCheckLists")]
    public class ProjectCheckList:FullAuditedEntity
    {
        public virtual string Factor { get; set; }
        public virtual string Description { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Projects { get; set; }
        public virtual int ProjectId { get; set; }
        [ForeignKey("PostedId")]
        public virtual User PostedUser { get; set; }
        public virtual long PostedId { get; set; }
        
        public virtual bool? ProjectImportant { get; set; }
    }
}
