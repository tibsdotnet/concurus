﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.TeamMasterTable
{
    [Table("Teams")]
    public class Team:FullAuditedEntity
    {
        public const int CodeLength = 15;
        public const int NameLength = 150;
        [MaxLength(CodeLength)]
        public string TeamCode { get; set; }
        [MaxLength(NameLength)]
        public string TeamName { get; set; }
    }
}
