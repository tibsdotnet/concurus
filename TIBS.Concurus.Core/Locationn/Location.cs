﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using TIBS.Concurus.Cityy;

namespace TIBS.Concurus.Locationn
{
    [Table("Location")]
    public class Location:FullAuditedEntity
    {
        public const int NameLength = 100;
        public const int CodeLength = 15;
        [Required]
        [MaxLength(NameLength)]
        public virtual string LocationName { get; set; }

        [Required]
        [MaxLength(CodeLength)]
        public virtual string LocationCode { get; set; }

        [ForeignKey("CityId")]
        public virtual City City { get; set; }
        [Required]
        public virtual int CityId { get; set; }
    }
}
