﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using TIBS.Concurus.Projects;
using System.ComponentModel.DataAnnotations;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.BackLogTypes;
using TIBS.Concurus.BackLogPrioritys;
using TIBS.Concurus.BackLogStatuss;

namespace TIBS.Concurus.ProductBacklogs
{
    [Table("ProductBacklog")]
    public class ProductBacklog:FullAuditedEntity
    {
        [ForeignKey("ProjectId")]
        public virtual Project Projects { get; set; }
        public virtual int ProjectId { get; set; }
        [ForeignKey("BacklogTypeId")]
        public virtual BackLogType BacklogTypes{get;set;}
        public virtual int? BacklogTypeId{get;set;}
        [ForeignKey("BacklogPriorityId")]
        public virtual BackLogPriority BackLogPrioritys { get; set; }
        public virtual int? BacklogPriorityId { get; set; }
        public virtual string Sprint { get; set; }
        [ForeignKey("BacklogStatusId")]
        public virtual BackLogStatus BackLogStatuss { get; set; }
        public virtual int? BacklogStatusId { get; set; }
        public virtual string Where { get; set; }
       
        [ForeignKey("WhoId")]
        public virtual User WhoUser { get; set; }
        public virtual long? WhoId { get; set; }
        public virtual string What { get; set; }
        public virtual string Why { get; set; }
        public virtual string When { get; set; }
        public virtual string Notes { get; set; }
        public virtual string TotalHousrs{get;set;}
        [ForeignKey("RequestFrom")]
        public virtual User RequestUser { get; set; }
        public virtual long? RequestFrom { get; set; }
        public virtual DateTime? RequestDate { get; set; }
        [ForeignKey("ApprovedBy")]
        public virtual User ApproverUser { get; set; }
        public virtual long? ApprovedBy { get; set; }
        public virtual DateTime? ApprovedDate { get; set; }
        public virtual string ProjectBacklogName { get; set; }
    }
}
