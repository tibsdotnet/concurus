﻿namespace TIBS.Concurus
{
    /// <summary>
    /// Some general constants for the application.
    /// </summary>
    public class ConcurusConsts
    {
        public const string LocalizationSourceName = "Concurus";
    }
}