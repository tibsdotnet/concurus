﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using TIBS.Concurus.Projects;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.ProjectRoless;

namespace TIBS.Concurus.ProjectTeams
{
    [Table("ProjectTeam")]
   public class ProjectTeam:FullAuditedEntity
    {
        [ForeignKey("ProjectId")]
        public virtual Project Projects { get; set; }
        public virtual int ProjectId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual User User { get; set; }
        public virtual long? EmployeeId { get; set; }
        public virtual DateTime? ActualStart { get; set; }
        public virtual DateTime? ActualEnd { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual int? Hours { get; set; }
        [ForeignKey("ProjectRoleId")]
        public virtual ProjectRole ProjectRoles { get; set; }
        public virtual int? ProjectRoleId { get; set; }
    }
}
