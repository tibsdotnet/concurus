﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Companies;

namespace TIBS.Concurus.Contacts
{
    [Table("CompanyContact")]
    public class Contact:FullAuditedEntity
    {
        public virtual string ContactName { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string MailId { get; set; }
        public virtual string Address { get; set;  }
        [ForeignKey("CompanyId")]
        public virtual Company companies { get; set; }
        public virtual int CompanyId { get; set; }
    }
}
