﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Employees;
using TIBS.Concurus.TeamMasterTable;

namespace TIBS.Concurus.TeamEmploees
{
    [Table("TeamEmplyees")]
    public class TeamEmployee:FullAuditedEntity
    {
        [ForeignKey("TeamId")]
        public virtual Team Teams { get; set; }
        public virtual int TeamId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employees { get; set; }
        public virtual int EmployeeId { get; set; }
    }
}
