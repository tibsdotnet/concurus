﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.BackLogStatuss
{
    [Table("BackLogStatus")]
    public class BackLogStatus : FullAuditedEntity
    {
        public virtual string BackLogStatusCode { get; set; }
        public virtual string BackLogStatusName { get; set; }
    }
}
