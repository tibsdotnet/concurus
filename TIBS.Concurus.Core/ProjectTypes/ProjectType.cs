﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace TIBS.Concurus.ProjectTypes
{
    [Table("ProjectTypes")]
    public class ProjectType:FullAuditedEntity
    {
        public virtual string ProjectTypeCode { get; set; }
        public virtual string ProjectTypeName { get; set; }
    }
}
