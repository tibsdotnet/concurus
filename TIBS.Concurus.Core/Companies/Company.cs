﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.ContactTypes;

namespace TIBS.Concurus.Companies
{
    [Table("companies")]
    public class Company:FullAuditedEntity
    {
        [Required]
        public virtual string Name { get; set; }
        public virtual string Address { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string MailId { get; set; }
        public virtual string Website { get; set; }
        [ForeignKey("ContactTypeId")]
        public virtual ContactType ContactTypes { get; set; }
        public virtual int? ContactTypeId { get; set; }
    }
}
