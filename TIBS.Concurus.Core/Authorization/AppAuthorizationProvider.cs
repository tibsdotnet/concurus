﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace TIBS.Concurus.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));
            
            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));
            
            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            var gpy = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Geography, L("Geography"), multiTenancySides: MultiTenancySides.Tenant);
            var country = gpy.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_Country, L("Country"), multiTenancySides: MultiTenancySides.Tenant);
            country.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_Country_CreateNewCountry, L("CreateNewCountry"), multiTenancySides: MultiTenancySides.Tenant);
            country.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_Country_EditCountry, L("EditCountry"), multiTenancySides: MultiTenancySides.Tenant);
            country.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_Country_DeleteCountry, L("DeleteCountry"), multiTenancySides: MultiTenancySides.Tenant);


            var cty = gpy.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_City, L("City"), multiTenancySides: MultiTenancySides.Tenant);
            cty.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_City_CreateNewCity, L("CreateNewCity"), multiTenancySides: MultiTenancySides.Tenant);
            cty.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_City_EditCity, L("EditCity"), multiTenancySides: MultiTenancySides.Tenant);
            cty.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_City_DeleteCity, L("DeleteCity"), multiTenancySides: MultiTenancySides.Tenant);


            var lcn = gpy.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_Location, L("Location"), multiTenancySides: MultiTenancySides.Tenant);
            lcn.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_Location_CreateNewLocation, L("CreateNewLocation"), multiTenancySides: MultiTenancySides.Tenant);
            lcn.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_Location_EditLocation, L("EditLocation"), multiTenancySides: MultiTenancySides.Tenant);
            lcn.CreateChildPermission(AppPermissions.Pages_Tenant_Geography_Location_DeleteLocation, L("DeleteLocation"), multiTenancySides: MultiTenancySides.Tenant);
            //PROJECTROLE
            var projectrole = pages.CreateChildPermission(AppPermissions.Pages_Tenant_ProjectRole, L("ProjectRole"), multiTenancySides: MultiTenancySides.Tenant);
            projectrole.CreateChildPermission(AppPermissions.Pages_Tenant_ProjectRole_CreateProjectRole, L("CreateProjectRole"), multiTenancySides: MultiTenancySides.Tenant);
            projectrole.CreateChildPermission(AppPermissions.Pages_Tenant_ProjectRole_EditProjectRole, L("EditProjectRole"), multiTenancySides: MultiTenancySides.Tenant);
            projectrole.CreateChildPermission(AppPermissions.Pages_Tenant_ProjectRole_DeleteProjectRole, L("DeleteProjectRole"), multiTenancySides: MultiTenancySides.Tenant);

            // EMPLOYEE 
            var employee = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Employee, L("Employee"), multiTenancySides: MultiTenancySides.Tenant);
            employee.CreateChildPermission(AppPermissions.Pages_Tenant_Employee_CreateNewEmployee, L("CreateNewEmplyee"), multiTenancySides: MultiTenancySides.Tenant);
            employee.CreateChildPermission(AppPermissions.Pages_Tenant_Employee_EditEmployee, L("EditEmplyee"), multiTenancySides: MultiTenancySides.Tenant);
            employee.CreateChildPermission(AppPermissions.Pages_Tenant_Employee_DeleteEmployee, L("DeleteEmployee"), multiTenancySides: MultiTenancySides.Tenant);
           
            //PROJECTTYPE
            var ProjectType = pages.CreateChildPermission(AppPermissions.Pages_Tenant_ProjectType, L("ProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            ProjectType.CreateChildPermission(AppPermissions.Pages_Tenant_ProjectType_CreateProjectType, L("CreateNewProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            ProjectType.CreateChildPermission(AppPermissions.Pages_Tenant_ProjectType_EditTechnogy, L("EditProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            ProjectType.CreateChildPermission(AppPermissions.Pages_Tenant_ProjectType_DeleteProjectType, L("DeleteProjectType"), multiTenancySides: MultiTenancySides.Tenant);

            //PROJECTS
            var projects = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Projects, L("Project"), multiTenancySides: MultiTenancySides.Tenant);
            var project = projects.CreateChildPermission(AppPermissions.Pages_Tenant_Project, L("Project"), multiTenancySides: MultiTenancySides.Tenant);
            project.CreateChildPermission(AppPermissions.Pages_Tenant_Project_CreateProject, L("CreateNewProject"), multiTenancySides: MultiTenancySides.Tenant);
            project.CreateChildPermission(AppPermissions.Pages_Tenant_Project_EditProject, L("EditProject"), multiTenancySides: MultiTenancySides.Tenant);
            project.CreateChildPermission(AppPermissions.Pages_Tenant_Project_DeleteProject, L("DeleteProject"), multiTenancySides: MultiTenancySides.Tenant);


            //TEAM
            var team = projects.CreateChildPermission(AppPermissions.Pages_Tenant_Team, L("Team"), multiTenancySides: MultiTenancySides.Tenant);
            team.CreateChildPermission(AppPermissions.Pages_Tenant_Team_CreateTeam, L("CreateNewTeam"), multiTenancySides: MultiTenancySides.Tenant);
            team.CreateChildPermission(AppPermissions.Pages_Tenant_Team_EditTeam, L("EditTeam"), multiTenancySides: MultiTenancySides.Tenant);
            team.CreateChildPermission(AppPermissions.Pages_Tenant_Team_DeleteTeam, L("DeleteTeam"), multiTenancySides: MultiTenancySides.Tenant);

            //TECHNOLOGY
            var technology = projects.CreateChildPermission(AppPermissions.Pages_Tenant_Technology, L("Technology"), multiTenancySides: MultiTenancySides.Tenant);
            technology.CreateChildPermission(AppPermissions.Pages_Tenant_Technology_CreateTechnology, L("CreateNewTechnology"), multiTenancySides: MultiTenancySides.Tenant);
            technology.CreateChildPermission(AppPermissions.Pages_Tenant_Technology_EditTechnogy, L("EditTechnology"), multiTenancySides: MultiTenancySides.Tenant);
            technology.CreateChildPermission(AppPermissions.Pages_Tenant_Technology_DeleteTechnology, L("DeleteTechnology"), multiTenancySides: MultiTenancySides.Tenant);


            //PROJECTBACKLOG
            var projectbacklog = project.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectBacklog, L("Projectbacklog"), multiTenancySides: MultiTenancySides.Tenant);
            projectbacklog.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectBacklog_CreateProjectBacklog, L("CreateNewProjectBacklog"), multiTenancySides: MultiTenancySides.Tenant);
            projectbacklog.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectBacklog_EditProjectBacklog, L("EditProjectBacklog"), multiTenancySides: MultiTenancySides.Tenant);
            projectbacklog.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectBacklog_DeleteProjectBacklog, L("DeleteProjectBacklog"), multiTenancySides: MultiTenancySides.Tenant);


            //PROJECTTASK
            var projectTask = project.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectTask, L("ProjectTask"), multiTenancySides: MultiTenancySides.Tenant);
            projectTask.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectTask_CreateProjectTask, L("CreateNewProjectTask"), multiTenancySides: MultiTenancySides.Tenant);
            projectTask.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectTask_EditProjectTask, L("EditProjectTask"), multiTenancySides: MultiTenancySides.Tenant);
            projectTask.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectTask_DeleteProjectTask, L("DeleteProjectTask"), multiTenancySides: MultiTenancySides.Tenant);

            //TEAM
         //   var team = projects.CreateChildPermission(AppPermissions.Pages_Tenant_Project_Team, L("Team"),multiTenancySides:MultiTenancySides.Tenant);

            //PROJECT TYPE
            var projectType = projects.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectType, L("ProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            projectType.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectType_CreateProjectType, L("CreateNewProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            projectType.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectType_EditProjectType, L("EditProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            projectType.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectType_DeleteProjectType, L("DeleteProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            

            //PROJECT TEAM
            var projectTeam = projects.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectTeam, L("ProjectTeam"), multiTenancySides: MultiTenancySides.Tenant);
            projectTeam.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectTeam_CreateProjectTeam, L("CreateNewProjectTeam"), multiTenancySides: MultiTenancySides.Tenant);
            projectTeam.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectTeam_EditProjectTeam, L("EditProjectTeam"), multiTenancySides: MultiTenancySides.Tenant);
            projectTeam.CreateChildPermission(AppPermissions.Pages_Tenant_Project_ProjectTeam_DeleteProjectTeam, L("DeleteProjectTeam"), multiTenancySides: MultiTenancySides.Tenant);

            //COMPANY

            var address = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Addressbook, L("Addressbook"), multiTenancySides: MultiTenancySides.Tenant);
            var company = address.CreateChildPermission(AppPermissions.Pages_Tenant_Addressbook_Compnay, L("Company"), multiTenancySides: MultiTenancySides.Tenant);
            company.CreateChildPermission(AppPermissions.Pages_Tenant_Addressbook_Company_CreateCompany, L("CreateNewCompany"), multiTenancySides: MultiTenancySides.Tenant);
            company.CreateChildPermission(AppPermissions.Pages_Tenant_Addressbook_Company_EditCompany, L("EditCompany"), multiTenancySides: MultiTenancySides.Tenant);
            company.CreateChildPermission(AppPermissions.Pages_Tenant_Addressbook_Company_DeleteCompany, L("DeleteCompany"), multiTenancySides: MultiTenancySides.Tenant);


            
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ConcurusConsts.LocalizationSourceName);
        }
    }
}
