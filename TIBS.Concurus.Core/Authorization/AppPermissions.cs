﻿namespace TIBS.Concurus.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";
        
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";
        
        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";

        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";


        //GEOGRAPHY PERMISSIONS
        public const string Pages_Tenant_Geography = "Pages.Tenant.Geography";
        public const string Pages_Tenant_Geography_Country = "Pages.Tenant.Geography.Country";
        public const string Pages_Tenant_Geography_Country_CreateNewCountry = "Pages.Tenant.Geography.Country.CreateNewCountry";
        public const string Pages_Tenant_Geography_Country_EditCountry = "Pages.Tenant.Geography.Country.EditCountry";
        public const string Pages_Tenant_Geography_Country_DeleteCountry = "Pages.Tenant.Geography.Country.DeleteCountry";

        public const string Pages_Tenant_Geography_City = "Pages.Tenant.Geography.City";
        public const string Pages_Tenant_Geography_City_CreateNewCity = "Pages.Tenant.Geography.City.CreateNewCity";
        public const string Pages_Tenant_Geography_City_EditCity = "Pages.Tenant.Geography.City.EditCity";
        public const string Pages_Tenant_Geography_City_DeleteCity = "Pages.Tenant.Geography.City.DeleteCity";


        public const string Pages_Tenant_Geography_Location = "Pages.Tenant.Geography.Location";
        public const string Pages_Tenant_Geography_Location_CreateNewLocation = "Pages.Tenant.Geography.Location.CreateNewLocation";
        public const string Pages_Tenant_Geography_Location_EditLocation = "Pages.Tenant.Geography.Location.EditLocation";
        public const string Pages_Tenant_Geography_Location_DeleteLocation = "Pages.Tenant.Geography.Location.DeleteLocation";
        //PROJECTROLE
        public const string Pages_Tenant_ProjectRole = "Pages.Tenant.ProjectRole";
        public const string Pages_Tenant_ProjectRole_CreateProjectRole = "Pages.Tenant.ProjectRole.CreateProjectRole";
        public const string Pages_Tenant_ProjectRole_EditProjectRole = "Pages.Tenant.ProjectRole.EditProjectRole";
        public const string Pages_Tenant_ProjectRole_DeleteProjectRole = "Pages.Tenant.ProjectRole.DeleteProjectRole";

        // Employee PERMISSIONS
        public const string Pages_Tenant_Employee = "Pages.Tenant.Employee";
        public const string Pages_Tenant_Employee_CreateNewEmployee = "Pages.Tenant.Employee.CreateNewEmployee";
        public const string Pages_Tenant_Employee_EditEmployee = "Pages.Tenant.Employee.EditEmployee";
        public const string Pages_Tenant_Employee_DeleteEmployee = "Pages.Tenant.Employee.DeleteEmployee";

        //TEAM PERMISSIONS
        public const string Pages_Tenant_Team = "Pages.Tenant.Team";
        public const string Pages_Tenant_Team_CreateTeam = "Pages.Tenant.team.Createteam";
        public const string Pages_Tenant_Team_EditTeam = "Pages.Tenant.team.Editteam";
        public const string Pages_Tenant_Team_DeleteTeam = "Pages.Tenant.team.Deleteteam";

        //TECHNOLOGY PERMISSIONS
        public const string Pages_Tenant_Technology = "Pages.Tenant.Technology";
        public const string Pages_Tenant_Technology_CreateTechnology = "Pages.Tenant.Technology.CreateTechnology";
        public const string Pages_Tenant_Technology_EditTechnogy = "Pages.Tenant.Technology.EditTechnology";
        public const string Pages_Tenant_Technology_DeleteTechnology = "Pages.Tenant.Technology.DeleteTechnology";
        //PROJECTTYPE PERMISSIONS
        public const string Pages_Tenant_ProjectType = "Pages.Tenant.ProjectType";
        public const string Pages_Tenant_ProjectType_CreateProjectType = "Pages.Tenant.ProjectType.CreateProjectType";
        public const string Pages_Tenant_ProjectType_EditTechnogy = "Pages.Tenant.ProjectType.EditProjectType";
        public const string Pages_Tenant_ProjectType_DeleteProjectType = "Pages.Tenant.ProjectType.DeleteProjectType";
        

        //PROJECT
        public const string Pages_Tenant_Projects = "Pages.Tenant.Projects";
        public const string Pages_Tenant_Project = "Pages.Tenant.Project";
        public const string Pages_Tenant_Project_CreateProject = "Pages.Tenant.Project.CreateProject";
        public const string Pages_Tenant_Project_EditProject = "Pages.Tenant.Project.EditProject";
        public const string Pages_Tenant_Project_DeleteProject = "Pages.Tenant.Project.DeleteProject";

        //PROJECTBACKLOG
        public const string Pages_Tenant_Project_ProjectBacklog = "Pages.Tenant.Project.ProjectBacklog";
        public const string Pages_Tenant_Project_ProjectBacklog_CreateProjectBacklog = "Pages.Tenant.Project.ProjectBacklog.CreateProjectBacklog";
        public const string Pages_Tenant_Project_ProjectBacklog_EditProjectBacklog = "Pages.Tenant.Project.ProjectBacklog.EditProjectBacklog";
        public const string Pages_Tenant_Project_ProjectBacklog_DeleteProjectBacklog = "Pages.Tenant.Project.ProjectBacklog.DeleteProjectBacklog";

        //PROJECTTASK
        public const string Pages_Tenant_Project_ProjectTask = "Pages.Tenant.Project.ProjectTask";
        public const string Pages_Tenant_Project_ProjectTask_CreateProjectTask = "Pages.Tenant.Project.ProjectTask.CreateProjectTask";
        public const string Pages_Tenant_Project_ProjectTask_EditProjectTask = "Pages.Tenant.Project.ProjectTask.EditProjectTask";
        public const string Pages_Tenant_Project_ProjectTask_DeleteProjectTask = "Pages.Tenant.Project.ProjectTask.DeleteProjectTask";

        ////TEAM
        //public const string Pages_Tenant_Project_Team = "Pages.Tenant.Project.Team";
        //public const string Pages_Tenant_Project_Team_CreateTeam = "Pages.Tenant.Project.Team.CreateTeam";
        //public const string Pages_Tenant_Project_Team_EditTeam = "Pages.Tenant.Project.Team.EditTeam";
        //public const string Pages_Tenant_Project_Teame_DeleteTeam="Pages.Tenant.Project.Team.DeleteTeam";

        //PROJECT TYPE
        public const string Pages_Tenant_Project_ProjectType = "Pages.Tenant.Project.ProjectType";
        public const string Pages_Tenant_Project_ProjectType_CreateProjectType = "Pages.Tenant.Project.ProjectType.CreateProjectTypye";
        public const string Pages_Tenant_Project_ProjectType_EditProjectType = "Pages.Tenant.Project.ProjectType.EditProjectType";
        public const string Pages_Tenant_Project_ProjectType_DeleteProjectType = "Pages.Tenant.Project.ProjectType.DeleteProjectType";


        //PROJECT TEAM
        public const string Pages_Tenant_Project_ProjectTeam = "Pages.Tenant.Project.ProjectTeam";
        public const string Pages_Tenant_Project_ProjectTeam_CreateProjectTeam = "Pages.Tenant.Project.ProjectTeam.CreateProjectTeam";
        public const string Pages_Tenant_Project_ProjectTeam_EditProjectTeam = "Pages.Tenant.Project.ProjectTeam.EditProjectTeam";
        public const string Pages_Tenant_Project_ProjectTeam_DeleteProjectTeam = "Pages.Tenant.Project.ProjectTeam.DeleteProjectTeam";

        //COMPANY
        public const string Pages_Tenant_Addressbook = "Pages.Tenant.Addressbook";
        public const string Pages_Tenant_Addressbook_Compnay = "Pages.Tenant.Addressbook.Company";
        public const string Pages_Tenant_Addressbook_Company_CreateCompany = "Pages.Tenant.Addressbook.Company.CreateCompany";
        public const string Pages_Tenant_Addressbook_Company_EditCompany = "Pages.Tenant.Addressbook.Company.EditCompany";
        public const string Pages_Tenant_Addressbook_Company_DeleteCompany = "Pages.Tenant.Addressbook.Company.DeleteCompany";


       





    }
}