﻿using Abp.Authorization;
using TIBS.Concurus.Authorization.Roles;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.MultiTenancy;

namespace TIBS.Concurus.Authorization
{
    /// <summary>
    /// Implements <see cref="PermissionChecker"/>.
    /// </summary>
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
