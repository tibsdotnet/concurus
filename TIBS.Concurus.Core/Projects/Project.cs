﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using TIBS.Concurus.Technologies;
using TIBS.Concurus.ProjectTypes;
using TIBS.Concurus.Companies;
using TIBS.Concurus.TeamMasterTable;

namespace TIBS.Concurus.Projects
{
    [Table("Projects")]
    public class Project:FullAuditedEntity
    {
        public virtual string ProjectName { get; set; }
        public virtual string Description { get; set; }
        [ForeignKey("TechnologyId")]
        public virtual Technology Technologies { get; set; }
        public virtual int? TechnologyId { get; set; }
        [ForeignKey("TeamId")]
        public virtual Team Teams { get; set; }
        public virtual int? TeamId { get; set; }
        [ForeignKey("ProjectTypeId")]
        public virtual ProjectType ProjectTypes { get; set; }
        public virtual int? ProjectTypeId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Companies { get; set; }
        public virtual int? CompanyId { get; set; }
    }
}
