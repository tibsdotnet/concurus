﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.BackLogTypes
{
    [Table("BackLogType")]
    public class BackLogType : FullAuditedEntity
    {
        public virtual string BackLogTypeCode { get; set; }
        public virtual string BackLogTypeName { get; set; }
    }
}
