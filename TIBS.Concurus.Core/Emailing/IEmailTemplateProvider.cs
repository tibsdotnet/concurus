﻿namespace TIBS.Concurus.Emailing
{
    public interface IEmailTemplateProvider
    {
        string GetDefaultTemplate();
    }
}
