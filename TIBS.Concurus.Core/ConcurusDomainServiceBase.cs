﻿using Abp.Domain.Services;

namespace TIBS.Concurus
{
    public abstract class ConcurusDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected ConcurusDomainServiceBase()
        {
            LocalizationSourceName = ConcurusConsts.LocalizationSourceName;
        }
    }
}
