﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.BackLogPrioritys
{
    [Table("BackLogPriority")]
    public class BackLogPriority : FullAuditedEntity
    {
        public virtual string BackLogPriorityCode { get; set; }
        public virtual string BackLogPriorityName { get; set; }
    }
}
