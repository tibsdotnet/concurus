﻿using Abp.Application.Features;
using TIBS.Concurus.Authorization.Roles;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.MultiTenancy;

namespace TIBS.Concurus.Editions
{
    public class FeatureValueStore : AbpFeatureValueStore<Tenant, Role, User>
    {
        public FeatureValueStore(TenantManager tenantManager) 
            : base(tenantManager)
        {
        }
    }
}
