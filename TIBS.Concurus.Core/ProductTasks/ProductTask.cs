﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using TIBS.Concurus.ProductBacklogs;
using System.ComponentModel.DataAnnotations;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.BackLogStatuss;

namespace TIBS.Concurus.ProductTasks
{
    [Table("ProductTask")]
    public class ProductTask:FullAuditedEntity
    {
        [ForeignKey("ProductBacklogId")]
        public virtual ProductBacklog ProductBacklogs { get; set; }
        public virtual int? ProductBacklogId { get; set; }
        [Required]
        public virtual string TaskName { get; set; }
        [ForeignKey("AssignedUserId")]
        public virtual User AssignedUser { get; set; }
        public virtual long? AssignedUserId { get; set; }
        public virtual decimal EstimateHours { get; set; }
        public virtual decimal AllotedHours { get; set; }
        public virtual decimal ActualTime { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual decimal RemainingTime { get; set; }
        [ForeignKey("StatusId")]
        public virtual BackLogStatus BackLogStatuss { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual string Remarks { get; set; }
        
    }
}
