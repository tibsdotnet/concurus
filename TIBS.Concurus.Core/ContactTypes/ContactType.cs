﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TIBS.Concurus.ContactTypes
{
    [Table("ContactType")]
    public class ContactType:FullAuditedEntity
    {
        public virtual string TypeCode { get; set; }
        public virtual string TypeName { get; set; }
    }
}
