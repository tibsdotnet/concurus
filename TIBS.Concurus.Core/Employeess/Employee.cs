﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Authorization.Roles;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.Cityy;
using TIBS.Concurus.Countryy;
using TIBS.Concurus.Locationn;

namespace TIBS.Concurus.Employees
{
    [Table("Employee")]
    public class Employee : FullAuditedEntity
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime? BirthDate { get; set; }
        public virtual DateTime? HireDate { get; set; }
        public virtual string Address { get; set; }
        public virtual string PhoneNo { get; set; }
        public virtual string Email { get; set; }
        public string PhotoPath { get; set; }
        [ForeignKey("UserId")]
        public virtual User AbpUser { get; set; }
        public virtual long? UserId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Countries { get; set; }
        public virtual int? CountryId { get; set; }
        [ForeignKey("CityId")]
        public virtual City cities { get; set; }
        public virtual int? CityId { get; set; }
        [ForeignKey("LocationId")]
        public virtual Location Locations { get; set; }
        public virtual int? LocationId { get; set; }
        public virtual string Sex { get; set; }
        public virtual decimal? RateForHour { get; set; }
        public virtual int? Age { get; set; }
        
    }
}
