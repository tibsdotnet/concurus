﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace TIBS.Concurus.Technologies
{
    [Table("Technologies")]
     public class Technology:FullAuditedEntity
    {
        public virtual string TechnologyCode { get; set; }
        public virtual string TechnologyName { get; set; }
    }
}
