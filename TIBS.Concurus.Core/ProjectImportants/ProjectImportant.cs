﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.ProjectCheckLists;
using TIBS.Concurus.Projects;

namespace TIBS.Concurus.ProjectImportants
{
    [Table("ProjectImportants")]
    public class ProjectImportant:FullAuditedEntity
    {
        [ForeignKey("ProjectChecklistId")]
        public virtual ProjectCheckList ProjectChecklists { get; set; }
        public virtual int? ProjectChecklistId { get; set; }
        [ForeignKey("ImportantMakeUserId")]
        public virtual User MakeUser { get; set; }
        public virtual long ImportantMakeUserId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Projects { get; set; }
        public virtual int? ProjectId { get; set; }
    }
}
