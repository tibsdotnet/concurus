﻿using Abp.Domain.Repositories;
using Abp.MultiTenancy;
using TIBS.Concurus.Authorization.Roles;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.Editions;

namespace TIBS.Concurus.MultiTenancy
{
    /// <summary>
    /// Tenant manager.
    /// </summary>
    public class TenantManager : AbpTenantManager<Tenant, Role, User>
    {
        public TenantManager(
            IRepository<Tenant> tenantRepository, 
            IRepository<TenantFeatureSetting, long> tenantFeatureRepository, 
            EditionManager editionManager) : 
            base(
                tenantRepository, 
                tenantFeatureRepository, 
                editionManager
            )
        {
        }
    }
}
