namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class added_Projects_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectName = c.String(),
                        Description = c.String(),
                        TechnologyId = c.Int(),
                        TeamId = c.Int(),
                        ProjectTypeId = c.Int(),
                        CompanyId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Project_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.companies", t => t.CompanyId)
                .ForeignKey("dbo.ProjectTypes", t => t.ProjectTypeId)
                .ForeignKey("dbo.Teams", t => t.TeamId)
                .ForeignKey("dbo.Technologies", t => t.TechnologyId)
                .Index(t => t.TechnologyId)
                .Index(t => t.TeamId)
                .Index(t => t.ProjectTypeId)
                .Index(t => t.CompanyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Projects", "TechnologyId", "dbo.Technologies");
            DropForeignKey("dbo.Projects", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.Projects", "ProjectTypeId", "dbo.ProjectTypes");
            DropForeignKey("dbo.Projects", "CompanyId", "dbo.companies");
            DropIndex("dbo.Projects", new[] { "CompanyId" });
            DropIndex("dbo.Projects", new[] { "ProjectTypeId" });
            DropIndex("dbo.Projects", new[] { "TeamId" });
            DropIndex("dbo.Projects", new[] { "TechnologyId" });
            DropTable("dbo.Projects",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Project_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
