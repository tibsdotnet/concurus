namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_Pending_Employee_Age : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employee", "Age", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employee", "Age");
        }
    }
}
