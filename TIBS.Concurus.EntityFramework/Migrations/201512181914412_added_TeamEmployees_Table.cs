namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class added_TeamEmployees_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TeamEmplyees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TeamId = c.Int(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TeamEmployee_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employee", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.Teams", t => t.TeamId, cascadeDelete: true)
                .Index(t => t.TeamId)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeamEmplyees", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.TeamEmplyees", "EmployeeId", "dbo.Employee");
            DropIndex("dbo.TeamEmplyees", new[] { "EmployeeId" });
            DropIndex("dbo.TeamEmplyees", new[] { "TeamId" });
            DropTable("dbo.TeamEmplyees",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TeamEmployee_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
