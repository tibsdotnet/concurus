namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_companyId_Pending_Migration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CompanyContact", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("dbo.CompanyContact", "CompanyId");
            AddForeignKey("dbo.CompanyContact", "CompanyId", "dbo.companies", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CompanyContact", "CompanyId", "dbo.companies");
            DropIndex("dbo.CompanyContact", new[] { "CompanyId" });
            DropColumn("dbo.CompanyContact", "CompanyId");
        }
    }
}
