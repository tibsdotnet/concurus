namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_ProjectId_Pending_ForProjectChecklists : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectCheckLists", "ProjectId", c => c.Int(nullable: false));
            CreateIndex("dbo.ProjectCheckLists", "ProjectId");
            AddForeignKey("dbo.ProjectCheckLists", "ProjectId", "dbo.Projects", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectCheckLists", "ProjectId", "dbo.Projects");
            DropIndex("dbo.ProjectCheckLists", new[] { "ProjectId" });
            DropColumn("dbo.ProjectCheckLists", "ProjectId");
        }
    }
}
