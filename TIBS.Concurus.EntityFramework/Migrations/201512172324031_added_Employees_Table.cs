namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class added_Employees_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        BirthDate = c.DateTime(),
                        HireDate = c.DateTime(),
                        Address = c.String(),
                        PhoneNo = c.String(),
                        Email = c.String(),
                        PhotoPath = c.String(),
                        UserId = c.Long(),
                        CountryId = c.Int(),
                        CityId = c.Int(),
                        LocationId = c.Int(),
                        Sex = c.String(),
                        RateForHour = c.Decimal(precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Employee_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .ForeignKey("dbo.City", t => t.CityId)
                .ForeignKey("dbo.Country", t => t.CountryId)
                .ForeignKey("dbo.Location", t => t.LocationId)
                .Index(t => t.UserId)
                .Index(t => t.CountryId)
                .Index(t => t.CityId)
                .Index(t => t.LocationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employee", "LocationId", "dbo.Location");
            DropForeignKey("dbo.Employee", "CountryId", "dbo.Country");
            DropForeignKey("dbo.Employee", "CityId", "dbo.City");
            DropForeignKey("dbo.Employee", "UserId", "dbo.AbpUsers");
            DropIndex("dbo.Employee", new[] { "LocationId" });
            DropIndex("dbo.Employee", new[] { "CityId" });
            DropIndex("dbo.Employee", new[] { "CountryId" });
            DropIndex("dbo.Employee", new[] { "UserId" });
            DropTable("dbo.Employee",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Employee_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
