namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_Pending_ProjectRoleIdForProjectTeam_Table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectTeam", "ProjectRoleId", c => c.Int());
            CreateIndex("dbo.ProjectTeam", "ProjectRoleId");
            AddForeignKey("dbo.ProjectTeam", "ProjectRoleId", "dbo.ProjectRole", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectTeam", "ProjectRoleId", "dbo.ProjectRole");
            DropIndex("dbo.ProjectTeam", new[] { "ProjectRoleId" });
            DropColumn("dbo.ProjectTeam", "ProjectRoleId");
        }
    }
}
