// <auto-generated />
namespace TIBS.Concurus.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class add_Project_BacklogName_Pending_Update : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(add_Project_BacklogName_Pending_Update));
        
        string IMigrationMetadata.Id
        {
            get { return "201512311716096_add_Project_BacklogName_Pending_Update"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
