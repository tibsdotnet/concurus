namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class added_ProjectTeams_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectTeam",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        EmployeeId = c.Long(),
                        ActualStart = c.DateTime(),
                        ActualEnd = c.DateTime(),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        Hours = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProjectTeam_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("dbo.AbpUsers", t => t.EmployeeId)
                .Index(t => t.ProjectId)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectTeam", "EmployeeId", "dbo.AbpUsers");
            DropForeignKey("dbo.ProjectTeam", "ProjectId", "dbo.Projects");
            DropIndex("dbo.ProjectTeam", new[] { "EmployeeId" });
            DropIndex("dbo.ProjectTeam", new[] { "ProjectId" });
            DropTable("dbo.ProjectTeam",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProjectTeam_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
