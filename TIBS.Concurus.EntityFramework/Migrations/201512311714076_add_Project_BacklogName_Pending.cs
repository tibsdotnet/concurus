namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_Project_BacklogName_Pending : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductTask", "ProjectBacklogName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductTask", "ProjectBacklogName");
        }
    }
}
