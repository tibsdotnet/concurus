using System.Data.Entity.Migrations;
using TIBS.Concurus.Migrations.Seed;

namespace TIBS.Concurus.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Concurus.EntityFramework.ConcurusDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Concurus";
        }

        protected override void Seed(Concurus.EntityFramework.ConcurusDbContext context)
        {
            new InitialDbBuilder(context).Create();
        }
    }
}
