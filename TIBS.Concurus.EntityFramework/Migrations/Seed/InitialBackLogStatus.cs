﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.BackLogStatuss;
using TIBS.Concurus.EntityFramework;

namespace TIBS.Concurus.Migrations.Seed
{
    public class InitialBackLogStatus
    {
    private readonly ConcurusDbContext _context;

    public InitialBackLogStatus(ConcurusDbContext context)
        {
            _context = context;
        }
    public void Create()
    {

        var v1 = _context.BackLogStatues.FirstOrDefault(p => p.BackLogStatusName == "Open");
        if (v1 == null)
        {
            _context.BackLogStatues.Add(
                new BackLogStatus
                {
                    BackLogStatusCode = "OPN",
                    BackLogStatusName = "Open"
                });
        }

        var v2 = _context.BackLogStatues.FirstOrDefault(p => p.BackLogStatusName == "Closed");
        if (v2 == null)
        {
            _context.BackLogStatues.Add(
                new BackLogStatus
                {
                    BackLogStatusCode = "CLO",
                    BackLogStatusName = "Closed"
                });
        }

        var v3 = _context.BackLogStatues.FirstOrDefault(p => p.BackLogStatusName == "Suspended");
        if (v3 == null)
        {
            _context.BackLogStatues.Add(
                new BackLogStatus
                {
                    BackLogStatusCode = "SUS",
                    BackLogStatusName = "Suspended"
                });
        }

        var v4 = _context.BackLogStatues.FirstOrDefault(p => p.BackLogStatusName == "Assigned");
        if (v4 == null)
        {
            _context.BackLogStatues.Add(
                new BackLogStatus
                {
                    BackLogStatusCode = "ASG",
                    BackLogStatusName = "Assigned"
                });
        }
        var v5 = _context.BackLogStatues.FirstOrDefault(p => p.BackLogStatusName == "Resolved");
        if (v5 == null)
        {
            _context.BackLogStatues.Add(
                new BackLogStatus
                {
                    BackLogStatusCode = "RES",
                    BackLogStatusName = "Resolved"
                });
        }
        var v6 = _context.BackLogStatues.FirstOrDefault(p => p.BackLogStatusName == "ReOpened");
        if (v6 == null)
        {
            _context.BackLogStatues.Add(
                new BackLogStatus
                {
                    BackLogStatusCode = "REO",
                    BackLogStatusName = "ReOpened"
                });
        }
        var v7 = _context.BackLogStatues.FirstOrDefault(p => p.BackLogStatusName == "InProgress");
        if (v7 == null)
        {
            _context.BackLogStatues.Add(
                new BackLogStatus
                {
                    BackLogStatusCode = "INP",
                    BackLogStatusName = "InProgress"
                });
        }

    }
    }
}
