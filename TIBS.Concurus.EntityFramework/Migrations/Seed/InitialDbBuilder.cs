﻿using EntityFramework.DynamicFilters;
using TIBS.Concurus.EntityFramework;

namespace TIBS.Concurus.Migrations.Seed
{
    public class InitialDbBuilder
    {
        private readonly ConcurusDbContext _context;

        public InitialDbBuilder(ConcurusDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new DefaultTenantRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            new ContactTypeInsert(_context).Create();
            new InitialBackLogType(_context).Create();
            new InitialBackLogPriority(_context).Create();
            new InitialBackLogStatus(_context).Create();

            _context.SaveChanges();
        }
    }
}
