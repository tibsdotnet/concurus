﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.BackLogTypes;
using TIBS.Concurus.EntityFramework;

namespace TIBS.Concurus.Migrations.Seed
{
    public class InitialBackLogType
    {
    private readonly ConcurusDbContext _context;

    public InitialBackLogType(ConcurusDbContext context)
        {
            _context = context;
        }
    public void Create()
    {

        var v1 = _context.BackLogTypes.FirstOrDefault(p => p.BackLogTypeName == "Enhancement");
        if (v1 == null)
        {
            _context.BackLogTypes.Add(
                new BackLogType
                {
                    BackLogTypeCode = "ENC",
                    BackLogTypeName = "Enhancement"
                });
        }

        var v2 = _context.BackLogTypes.FirstOrDefault(p => p.BackLogTypeName == "Cosmetic");
        if (v2 == null)
        {
            _context.BackLogTypes.Add(
                new BackLogType
                {
                    BackLogTypeCode = "COS",
                    BackLogTypeName = "Cosmetic"
                });
        }

        var v3 = _context.BackLogTypes.FirstOrDefault(p => p.BackLogTypeName == "Bug");
        if (v3 == null)
        {
            _context.BackLogTypes.Add(
                new BackLogType
                {
                    BackLogTypeCode = "BUG",
                    BackLogTypeName = "Bug"
                });
        }

        var v4 = _context.BackLogTypes.FirstOrDefault(p => p.BackLogTypeName == "Change Request");
        if (v4 == null)
        {
            _context.BackLogTypes.Add(
                new BackLogType
                {
                    BackLogTypeCode = "CHR",
                    BackLogTypeName = "Change Request"
                });
        }
        var v5 = _context.BackLogTypes.FirstOrDefault(p => p.BackLogTypeName == "Technical");
        if (v5 == null)
        {
            _context.BackLogTypes.Add(
                new BackLogType
                {
                    BackLogTypeCode = "TEC",
                    BackLogTypeName = "Technical"
                });
        }
        var v6 = _context.BackLogTypes.FirstOrDefault(p => p.BackLogTypeName == "Knowledge Acquisition");
        if (v6 == null)
        {
            _context.BackLogTypes.Add(
                new BackLogType
                {
                    BackLogTypeCode = "KNA",
                    BackLogTypeName = "Knowledge Acquisition"
                });
        }

    }
    }
}
