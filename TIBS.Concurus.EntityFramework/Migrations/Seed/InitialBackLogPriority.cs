﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.BackLogPrioritys;
using TIBS.Concurus.EntityFramework;

namespace TIBS.Concurus.Migrations.Seed
{
    public class InitialBackLogPriority
    {
    private readonly ConcurusDbContext _context;

    public InitialBackLogPriority(ConcurusDbContext context)
        {
            _context = context;
        }
    public void Create()
    {

        var v1 = _context.BackLogPriorities.FirstOrDefault(p => p.BackLogPriorityName == "Low");
        if (v1 == null)
        {
            _context.BackLogPriorities.Add(
                new BackLogPriority
                {
                    BackLogPriorityCode = "LOW",
                    BackLogPriorityName = "Low"
                });
        }

        var v2 = _context.BackLogPriorities.FirstOrDefault(p => p.BackLogPriorityName == "Normal");
        if (v2 == null)
        {
            _context.BackLogPriorities.Add(
                new BackLogPriority
                {
                    BackLogPriorityCode = "NOR",
                    BackLogPriorityName = "Normal"
                });
        }

        var v3 = _context.BackLogPriorities.FirstOrDefault(p => p.BackLogPriorityName == "High");
        if (v3 == null)
        {
            _context.BackLogPriorities.Add(
                new BackLogPriority
                {
                    BackLogPriorityCode = "HIG",
                    BackLogPriorityName = "High"
                });
        }

        var v4 = _context.BackLogPriorities.FirstOrDefault(p => p.BackLogPriorityName == "Urgent");
        if (v4 == null)
        {
            _context.BackLogPriorities.Add(
                new BackLogPriority
                {
                    BackLogPriorityCode = "URG",
                    BackLogPriorityName = "Urgent"
                });
        }
        var v5 = _context.BackLogPriorities.FirstOrDefault(p => p.BackLogPriorityName == "Immediate");
        if (v5 == null)
        {
            _context.BackLogPriorities.Add(
                new BackLogPriority
                {
                    BackLogPriorityCode = "IME",
                    BackLogPriorityName = "Immediate"
                });
        }
        var v6 = _context.BackLogPriorities.FirstOrDefault(p => p.BackLogPriorityName == "None");
        if (v6 == null)
        {
            _context.BackLogPriorities.Add(
                new BackLogPriority
                {
                    BackLogPriorityCode = "NON",
                    BackLogPriorityName = "None"
                });
        }

    }
    }
}
