﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.EntityFramework;

namespace TIBS.Concurus.Migrations.Seed
{
   
   public class ContactTypeInsert
    {
        private readonly ConcurusDbContext _context;
       public ContactTypeInsert(ConcurusDbContext context)
        {
            _context = context;
        }
       public void Create()
       {
           var contactType1 = _context.ContactTypes.FirstOrDefault(u => u.TypeCode == "CON" && u.TypeName == "Contractor");
           if(contactType1==null)
           {
               _context.ContactTypes.Add(
                   new ContactTypes.ContactType
                   {
                       TypeName = "Contractor",
                       TypeCode = "CON"

                   });
           }
           var contactType2 = _context.ContactTypes.FirstOrDefault(u => u.TypeCode == "CUS" && u.TypeName == "Customer");
           if (contactType2 == null)
           {
               _context.ContactTypes.Add(
                   new ContactTypes.ContactType
                   {
                       TypeName = "Customer",
                       TypeCode = "CUS"

                   });
              
           }
           var contactType3 = _context.ContactTypes.FirstOrDefault(u => u.TypeCode == "Agent" && u.TypeName == "Agent");
           if (contactType3 == null)
           {
               _context.ContactTypes.Add(
                   new ContactTypes.ContactType
                   {
                       TypeName = "Agent",
                       TypeCode = "Agent"

                   });
           }
       }
    }
}
