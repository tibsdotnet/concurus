namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class added_ProjectImportants_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProjectImportants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectChecklistId = c.Int(),
                        ImportantMakeUserId = c.Long(nullable: false),
                        ProjectId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProjectImportant_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.ImportantMakeUserId, cascadeDelete: true)
                .ForeignKey("dbo.ProjectCheckLists", t => t.ProjectChecklistId)
                .ForeignKey("dbo.Projects", t => t.ProjectId)
                .Index(t => t.ProjectChecklistId)
                .Index(t => t.ImportantMakeUserId)
                .Index(t => t.ProjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectImportants", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.ProjectImportants", "ProjectChecklistId", "dbo.ProjectCheckLists");
            DropForeignKey("dbo.ProjectImportants", "ImportantMakeUserId", "dbo.AbpUsers");
            DropIndex("dbo.ProjectImportants", new[] { "ProjectId" });
            DropIndex("dbo.ProjectImportants", new[] { "ImportantMakeUserId" });
            DropIndex("dbo.ProjectImportants", new[] { "ProjectChecklistId" });
            DropTable("dbo.ProjectImportants",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProjectImportant_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
