namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class added_ProductBacklogs_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductBacklog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        BacklogTypeId = c.Int(),
                        BacklogPriorityId = c.Int(),
                        Sprint = c.String(),
                        BacklogStatusId = c.Int(),
                        Where = c.String(),
                        WhoId = c.Long(),
                        What = c.String(),
                        Why = c.String(),
                        When = c.String(),
                        Notes = c.String(),
                        TotalHousrs = c.String(),
                        RequestFrom = c.Long(),
                        RequestDate = c.DateTime(),
                        ApprovedBy = c.Long(),
                        ApprovedDate = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductBacklog_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.ApprovedBy)
                .ForeignKey("dbo.BackLogPriority", t => t.BacklogPriorityId)
                .ForeignKey("dbo.BackLogStatus", t => t.BacklogStatusId)
                .ForeignKey("dbo.BackLogType", t => t.BacklogTypeId)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .ForeignKey("dbo.AbpUsers", t => t.RequestFrom)
                .ForeignKey("dbo.AbpUsers", t => t.WhoId)
                .Index(t => t.ProjectId)
                .Index(t => t.BacklogTypeId)
                .Index(t => t.BacklogPriorityId)
                .Index(t => t.BacklogStatusId)
                .Index(t => t.WhoId)
                .Index(t => t.RequestFrom)
                .Index(t => t.ApprovedBy);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductTask", "WhoId", "dbo.AbpUsers");
            DropForeignKey("dbo.ProductTask", "RequestFrom", "dbo.AbpUsers");
            DropForeignKey("dbo.ProductTask", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.ProductTask", "BacklogTypeId", "dbo.BackLogType");
            DropForeignKey("dbo.ProductTask", "BacklogStatusId", "dbo.BackLogStatus");
            DropForeignKey("dbo.ProductTask", "BacklogPriorityId", "dbo.BackLogPriority");
            DropForeignKey("dbo.ProductTask", "ApprovedBy", "dbo.AbpUsers");
            DropIndex("dbo.ProductTask", new[] { "ApprovedBy" });
            DropIndex("dbo.ProductTask", new[] { "RequestFrom" });
            DropIndex("dbo.ProductTask", new[] { "WhoId" });
            DropIndex("dbo.ProductTask", new[] { "BacklogStatusId" });
            DropIndex("dbo.ProductTask", new[] { "BacklogPriorityId" });
            DropIndex("dbo.ProductTask", new[] { "BacklogTypeId" });
            DropIndex("dbo.ProductTask", new[] { "ProjectId" });
            DropTable("dbo.ProductTask",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductBacklog_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
