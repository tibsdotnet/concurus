namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_Project_BacklogName_Pending_Update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductBacklog", "ProjectBacklogName", c => c.String());
            DropColumn("dbo.ProductTask", "ProjectBacklogName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductTask", "ProjectBacklogName", c => c.String());
            DropColumn("dbo.ProductBacklog", "ProjectBacklogName");
        }
    }
}
