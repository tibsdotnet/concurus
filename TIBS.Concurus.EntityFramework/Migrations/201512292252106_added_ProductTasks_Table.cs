namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class added_ProductTasks_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductTask",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductBacklogId = c.Int(),
                        TaskName = c.String(nullable: false),
                        AssignedUserId = c.Long(),
                        EstimateHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AllotedHours = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ActualTime = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        RemainingTime = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StatusId = c.Int(),
                        Remarks = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductTask_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.AssignedUserId)
                .ForeignKey("dbo.BackLogStatus", t => t.StatusId)
                .ForeignKey("dbo.ProductBacklog", t => t.ProductBacklogId)
                .Index(t => t.ProductBacklogId)
                .Index(t => t.AssignedUserId)
                .Index(t => t.StatusId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductTask", "ProductBacklogId", "dbo.ProductBacklog");
            DropForeignKey("dbo.ProductTask", "StatusId", "dbo.BackLogStatus");
            DropForeignKey("dbo.ProductTask", "AssignedUserId", "dbo.AbpUsers");
            DropIndex("dbo.ProductTask", new[] { "StatusId" });
            DropIndex("dbo.ProductTask", new[] { "AssignedUserId" });
            DropIndex("dbo.ProductTask", new[] { "ProductBacklogId" });
            DropTable("dbo.ProductTask",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProductTask_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
