namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_Pending_ProjectImportant : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectCheckLists", "ProjectImportant", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectCheckLists", "ProjectImportant");
        }
    }
}
