namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_userId_Pending_ForProjectChecklists : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectCheckLists", "PostedId", c => c.Long(nullable: false));
            CreateIndex("dbo.ProjectCheckLists", "PostedId");
            AddForeignKey("dbo.ProjectCheckLists", "PostedId", "dbo.AbpUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectCheckLists", "PostedId", "dbo.AbpUsers");
            DropIndex("dbo.ProjectCheckLists", new[] { "PostedId" });
            DropColumn("dbo.ProjectCheckLists", "PostedId");
        }
    }
}
