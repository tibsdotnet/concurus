namespace TIBS.Concurus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_Pending_company_Migration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.companies", "ContactTypeId", c => c.Int());
            CreateIndex("dbo.companies", "ContactTypeId");
            AddForeignKey("dbo.companies", "ContactTypeId", "dbo.ContactType", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.companies", "ContactTypeId", "dbo.ContactType");
            DropIndex("dbo.companies", new[] { "ContactTypeId" });
            DropColumn("dbo.companies", "ContactTypeId");
        }
    }
}
