﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using TIBS.Concurus.Authorization.Roles;
using TIBS.Concurus.Authorization.Users;
using TIBS.Concurus.MultiTenancy;
using TIBS.Concurus.Storage;
using TIBS.Concurus.ProjectRoless;
using TIBS.Concurus.Countryy;
using TIBS.Concurus.Cityy;
using TIBS.Concurus.Locationn;
using TIBS.Concurus.Employees;
using TIBS.Concurus.TeamMasterTable;
using TIBS.Concurus.TeamEmploees;
using TIBS.Concurus.ContactTypes;
using TIBS.Concurus.Companies;
using TIBS.Concurus.Contacts;
using TIBS.Concurus.Technologies;
using TIBS.Concurus.ProjectTypes;
using TIBS.Concurus.Projects;
using TIBS.Concurus.ProjectTeams;
using TIBS.Concurus.BackLogPrioritys;
using TIBS.Concurus.BackLogStatuss;
using TIBS.Concurus.BackLogTypes;
using TIBS.Concurus.ProductBacklogs;
using TIBS.Concurus.ProductTasks;
using TIBS.Concurus.ProjectCheckLists;
using TIBS.Concurus.ProjectImportants;

namespace TIBS.Concurus.EntityFramework
{
    public class ConcurusDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        /* Define an IDbSet for each entity of the application */

        public virtual IDbSet<BinaryObject> BinaryObjects { get; set; }
        public virtual IDbSet<ProjectRole> ProjectRoles { get; set; }
        public virtual IDbSet<Country> Coutries { get; set; }
        public virtual IDbSet<City> Cities { get; set; }
        public virtual IDbSet<Location> Locations { get; set; }
        public virtual IDbSet<Employee> Employees { get; set; }
        public virtual IDbSet<Team> Teams { get; set; }
        public virtual IDbSet<TeamEmployee> TeamEmployees { get; set; }
        public virtual IDbSet<ContactType> ContactTypes { get; set; }
        public virtual IDbSet<Company> Companies { get; set; }
        public virtual IDbSet<Contact> Contacts { get; set; }
        public virtual IDbSet<Technology> Technologies { get; set; }
        public virtual IDbSet<ProjectType> ProjectTypes { get; set; }
        public virtual IDbSet<Project> Projects { get; set; }
        public virtual IDbSet<ProjectTeam> ProjectTeams { get; set; }
        public virtual IDbSet<BackLogPriority> BackLogPriorities { get; set; }
        public virtual IDbSet<BackLogStatus> BackLogStatues { get; set; }
        public virtual IDbSet<BackLogType> BackLogTypes { get; set; }
        public virtual IDbSet<ProductBacklog> ProductBacklogs { get; set; }
        public virtual IDbSet<ProductTask> ProductTasks { get; set; }
        public virtual IDbSet<ProjectCheckList> ProjectCheckLists { get; set; }
        public virtual IDbSet<ProjectImportant> ProjectImportants { get; set; }

        /* Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         * But it may cause problems when working Migrate.exe of EF. ABP works either way.         * 
         */
        public ConcurusDbContext()
            : base("Default")
        {

        }

        /* This constructor is used by ABP to pass connection string defined in ConcurusDataModule.PreInitialize.
         * Notice that, actually you will not directly create an instance of ConcurusDbContext since ABP automatically handles it.
         */
        public ConcurusDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        /* This constructor is used in tests to pass a fake/mock connection.
         */
        public ConcurusDbContext(DbConnection dbConnection)
            : base(dbConnection, true)
        {

        }
    }
}
