﻿using Abp.Modules;
using Abp.Zero.Configuration;

namespace TIBS.Concurus.Tests
{
    [DependsOn(
        typeof(ConcurusApplicationModule),
        typeof(ConcurusDataModule))]
    public class ConcurusTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Use database as language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();
        }
    }
}
