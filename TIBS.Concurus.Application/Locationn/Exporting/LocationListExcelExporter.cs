﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Locationn.Dto;

namespace TIBS.Concurus.Locationn.Exporting
{
    public class LocationListExcelExporter : EpPlusExcelExporterBase, ILocationListExcelExporter
    {

        public FileDto ExportToFile(List<LocationListDto> locationListDtos)
        {
            return CreateExcelPackage(
                "Location.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Location"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("LocationName"),
                        L("LocationCode"),
                        L("CityName")
                        //L("IsDeleted"),
                        //L("DeleterUserId"),
                        //L("DeletionTime"),
                        //L("LastModificationTime"),
                        //L("LastModifierUserId"),
                        //L("CreationTime"),
                        //L("CreatorUserId")
                    );

                    AddObjects(
                        sheet, 2, locationListDtos,
                        _ => _.LocationName,
                        _ => _.LocationCode,
                        _ => _.CityName
                        //_ => _.IsDeleted,
                        //_ => _.DeleterUserId,
                        //_ => _.DeletionTime,
                        //_ => _.LastModificationTime,
                        //_ => _.LastModifierUserId,
                        //_ => _.CreationTime,
                        //_ => _.CreatorUserId

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(4);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 4; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
