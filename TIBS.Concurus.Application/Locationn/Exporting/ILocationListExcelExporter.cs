﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Locationn.Dto;

namespace TIBS.Concurus.Locationn.Exporting
{
    public interface ILocationListExcelExporter
    {
        FileDto ExportToFile(List<LocationListDto> locationListDtos);
    }
}
