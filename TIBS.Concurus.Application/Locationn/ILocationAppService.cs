﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TIBS.Concurus.Cityy.Dto;
using TIBS.Concurus.Locationn.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;


namespace TIBS.Concurus.Locationn
{
    public interface ILocationAppService:IApplicationService
    {
        Task<PagedResultOutput<LocationListDto>> GetLocations(GetLocationInput input);

        Task<GetLocation> GetLocationForEdit(NullableIdInput<long> input);

        Task CreateOrUpdateLocation(CreateLocationInput input);

        Task DeleteLocation(IdInput input);

        Task<FileDto> GetLocationToExcel();
    }
}
