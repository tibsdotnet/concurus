﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using TIBS.Concurus.Authorization;
using System;
using TIBS.Concurus.Cityy;
using TIBS.Concurus.Locationn.Dto;
using TIBS.Concurus.Countryy;
using TIBS.Concurus.Locationn.Exporting;
using TIBS.Concurus.Dto;
using System.Linq.Dynamic;
using System.Data.SqlClient;
using Abp.UI;


namespace TIBS.Concurus.Locationn
{
    public class LocationAppService : ConcurusAppServiceBase, ILocationAppService 
    {
        private readonly IRepository<Location> _LocationRepository;

        private readonly IRepository<City> _CityRepository;

        private readonly IRepository<Country> _CountryRepository;

        private readonly ILocationListExcelExporter _locationListExcelExporter;

        public LocationAppService(ILocationListExcelExporter locationListExcelExporter,IRepository<Location> LocationRepository, IRepository<City> CityRepository, IRepository<Country> CountryRepository)
        {
            _LocationRepository = LocationRepository;
            _CityRepository = CityRepository;
            _CountryRepository = CountryRepository;
            _locationListExcelExporter = locationListExcelExporter;
        }

        public async Task<PagedResultOutput<LocationListDto>> GetLocations(GetLocationInput input)
        {
            var query = _LocationRepository.GetAll()
                .Include(u => u.City)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.LocationName.Contains(input.Filter) ||
                        u.LocationCode.Contains(input.Filter) ||
                        u.Id.ToString().Contains(input.Filter) ||
                        u.City.CityName.ToString().Contains(input.Filter)
                );

            var locationCount = await query.CountAsync();
            var locations = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var locationListDtos = locations.MapTo<List<LocationListDto>>();
            await FillCityNames(locationListDtos);

            return new PagedResultOutput<LocationListDto>(
                locationCount,
                locationListDtos
                );
        }

        public async Task<FileDto> GetLocationToExcel()
        {

            var location = _LocationRepository
                .GetAll();
            var locationListDtos = location.MapTo<List<LocationListDto>>();

            await FillCityNames(locationListDtos);
            return _locationListExcelExporter.ExportToFile(locationListDtos);
        }

        private async Task FillCityNames(List<LocationListDto> locationListDtos)
        {
            /* This method is optimized to fill role names to given list. */

            var distinctcityIds = (
                from locationListDto in locationListDtos
                select locationListDto.CityId
                ).Distinct();

            var cityNames = new Dictionary<int, string>();
            foreach (var cityId in distinctcityIds)
            {
                int cid = Convert.ToInt32(cityId);
                var coun = (_CityRepository.GetAll().Where(p => p.Id == cityId)).FirstOrDefault();
                cityNames[cityId] = coun.CityName.ToString();
            }

            foreach (var cityListDto in locationListDtos)
            {
                cityListDto.CityName = cityNames[cityListDto.CityId];
            }
        }

        public async Task<GetLocation> GetLocationForEdit(NullableIdInput<long> input)
        {
            var locationcityDtos = (await _CityRepository.GetAll()
                .OrderBy(r => r.CityName)
                .Select(r => new LocationCityDto
                {
                    CityId = r.Id,
                    CityName = r.CityName,
                    CountryName=""
                })
                .ToArrayAsync());


            var location = _LocationRepository
                .GetAll().Where(p => p.Id == input.Id).FirstOrDefault();


            //var cityListDtos = locationcityDtos.MapTo<List<LocationCityDto>>();
            foreach (var one in locationcityDtos)
            {
                var citd = (_CityRepository.GetAll().Where(c => c.Id == one.CityId)).FirstOrDefault();
                one.CountryId = citd.CountryId;
                var coun = (_CountryRepository.GetAll().Where(p => p.Id == one.CountryId)).FirstOrDefault();
                if (coun != null)
                {
                    one.CountryName = coun.CountryName;
                }
            }

            var output = new GetLocation
            {
                CityName = locationcityDtos
            };

            output.location = location.MapTo<CreateLocationInput>();

            return output;

        }

        public async Task CreateOrUpdateLocation(CreateLocationInput input)
        {
            if (input.Id != 0)
            {
                await UpdateLocationAsync(input);
            }
            else
            {
                await CreateLocationAsync(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Geography_Location_CreateNewLocation)]
        public virtual async Task CreateLocationAsync(CreateLocationInput input)
        {
            var Location = input.MapTo<Location>();
            DateTime myUtcDateTime = DateTime.Now;

            Location.LastModificationTime = DateTime.Now.AddHours(-12);
            var val = _LocationRepository
              .GetAll().Where(p => p.LocationCode == input.LocationCode || p.LocationName == input.LocationName).FirstOrDefault();
            if (val == null)
            {
                await _LocationRepository.InsertAsync(Location);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Location Name '" + input.LocationName + "' or Location Code '" + input.LocationCode + "'..."); 
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Geography_Location_EditLocation)]
        public virtual async Task UpdateLocationAsync(CreateLocationInput input)
        {
            var Location = input.MapTo<Location>();
            Location.LocationName = Location.LocationName;
            Location.LocationCode = Location.LocationCode;
            Location.CityId = Location.CityId;
            var val = _LocationRepository
             .GetAll().Where(p => (p.LocationCode == input.LocationCode || p.LocationName == input.LocationName) && p.Id != input.Id).FirstOrDefault();
            if (val == null)
            {
                await _LocationRepository.UpdateAsync(Location);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Location Name '" + input.LocationName + "' or Location Code '" + input.LocationCode + "'..."); 
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Geography_Location_DeleteLocation)]
        public async Task DeleteLocation(IdInput input)
        {
            
                        await _LocationRepository.DeleteAsync(input.Id);
                   
            
        }
    }
    public class FindDelete
    {
        public int id { get; set; }

        public string name { get; set; }
    }
}
