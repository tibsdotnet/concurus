﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TIBS.Concurus.Cityy;

namespace TIBS.Concurus.Locationn.Dto
{
    public class LocationCityDto:IDto
    {
        public int CityId { get; set; }

        public string CityName { get; set; }

        public string CountryName{ get; set; }

        public int CountryId { get; set; }
    }
}
