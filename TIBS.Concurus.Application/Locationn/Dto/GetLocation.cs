﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Locationn.Dto
{
    public class GetLocation:IInputDto
    {
        public CreateLocationInput location { get; set; }

        public LocationCityDto[] CityName { get; set; }
    }
}
