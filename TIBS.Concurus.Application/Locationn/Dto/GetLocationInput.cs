﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using TIBS.Concurus.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Locationn.Dto
{
    public class GetLocationInput: PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "LocationName,LocationCode,CreationTime";
            }
        }
    }
}
