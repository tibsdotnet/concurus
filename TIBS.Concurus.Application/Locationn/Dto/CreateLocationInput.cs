﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace TIBS.Concurus.Locationn.Dto
{
    [AutoMapTo(typeof(Location))]
    public class CreateLocationInput:IInputDto
    {
        [Required]
        [MaxLength(Location.NameLength)]
        public string LocationName { get; set; }

        [Required]
        [MaxLength(Location.CodeLength)]
        public string LocationCode { get; set; }

        public long Id { get; set; }

        [Required]
        public long CityId { get; set; }
    }
}
