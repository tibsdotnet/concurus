﻿using System.Collections.ObjectModel;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using TIBS.Concurus.Countryy;
using System.Collections.Generic;
using System;

namespace TIBS.Concurus.Locationn.Dto
{
    [AutoMapFrom(typeof(Location))]
    public class LocationListDto: EntityDto<long>, IHasCreationTime
    {
        public string LocationName { get; set; }

        public string LocationCode { get; set; }

        public int CityId { get; set; }

        public string CityName { get; set; }


        public DateTime CreationTime { get; set; }
    }
}
