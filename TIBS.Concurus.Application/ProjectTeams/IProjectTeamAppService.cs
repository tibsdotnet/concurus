﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectTeams.Dto;

namespace TIBS.Concurus.ProjectTeams
{
    public interface IProjectTeamAppService:IApplicationService
    {
        Task<ListResultOutput<ProjectTeamView_Dto>> GetProjectTeam(GetProjectTeamInput input);
        Task<GetProjectTeams> GetProjectTeamForEdit(NullableIdInput<long> input);
        Task<GetProjectTeams> GetCompanyForEdit(NullableIdInput<long> input);
        Task CreateProjectTeam(CreateProjectTeamInput input);
        Task<GetProjectTeams> GetProjectEmployeeForEdit(NullableIdInput<long> input);
        Task UpdateProjectEmployee(CreateProjectTeamInput input);
        Task DeleteTeamEmplyee(IdInput input);
        Task<FileDto> GetProjectTeamToExcel(GetProjectTeamInput input);
    }
}
