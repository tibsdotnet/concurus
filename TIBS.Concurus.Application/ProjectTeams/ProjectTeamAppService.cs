﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.ProjectTeams.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.AutoMapper;
using TIBS.Concurus.Employees;
using System.Data.Entity;
using TIBS.Concurus.Companies;
using TIBS.Concurus.Projects;
using TIBS.Concurus.ProjectRoless;
using Abp.Runtime.Session;
using TIBS.Concurus.Authorization.Roles;
using Abp.Authorization;
using TIBS.Concurus.Authorization;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectTeams.Exporting;

namespace TIBS.Concurus.ProjectTeams
{
    public class ProjectTeamAppService:ConcurusAppServiceBase,IProjectTeamAppService
    {
        private readonly IRepository<ProjectTeam> _ProjectTeamRepository;
        public readonly IRepository<Employee> _EmployeeRepository;
        private readonly IRepository<Project> _CompanyRepository;
        private readonly IRepository<ProjectRole> _ProjectRoleRepository;
        private readonly RoleManager _roleManager;
        private readonly IProjectTeamListExcelExporter _ProjectTeamListExcelExporter;
        public ProjectTeamAppService(IRepository<ProjectTeam> ProjectTeamRepository, IRepository<Employee> EmployeeRepository, IRepository<Project> CompanyRepository, IRepository<ProjectRole> ProjectRoleRepository, RoleManager roleManager,
            IProjectTeamListExcelExporter ProjectTeamListExcelExporter)
        {
            _ProjectTeamRepository = ProjectTeamRepository;
            _EmployeeRepository = EmployeeRepository;
            _CompanyRepository = CompanyRepository;
            _ProjectRoleRepository = ProjectRoleRepository;
            _roleManager = roleManager;
            _ProjectTeamListExcelExporter = ProjectTeamListExcelExporter;
        }
        public async  Task<ListResultOutput<ProjectTeamView_Dto>> GetProjectTeam(GetProjectTeamInput input)
        {
            long userid = (long)AbpSession.GetUserId();

            var user = await UserManager.GetUserByIdAsync(userid);
            var roleid = user.Roles.ToList();

            string[] userroles = new string[roleid.Count];
            int i = 0;
            foreach (var role in roleid)
            {
                int rol = role.RoleId;
                var userrolesquery = _roleManager.Roles.Where(p => p.Id == rol).OrderBy(p => p.DisplayName).FirstOrDefault();
                userroles[i] = userrolesquery.DisplayName;
                i++;
            }
            var uroles = userroles.ToList();
            var query = _ProjectTeamRepository.GetAll().Where(p => p.Id == 0);
            
            if (uroles.Contains("Admin"))
            {
               query = _ProjectTeamRepository.GetAll()
                          .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                          u => u.Projects.ProjectName.Contains(input.Filter) ||
                              u.Projects.Technologies.TechnologyName.Contains(input.Filter)
                          );
            }
            else
            {
                query = _ProjectTeamRepository.GetAll().Where(p=>p.EmployeeId==userid)
                         .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                         u => u.Projects.ProjectName.Contains(input.Filter) ||
                             u.Projects.Technologies.TechnologyName.Contains(input.Filter)
                         );
            }
            var projectTeam = from c in query  select new ProjectTeamView_Dto { ProjectName = c.ProjectId != null ? c.Projects.ProjectName : "", TechnologyName = c.ProjectId != null ? c.Projects.Technologies.TechnologyName : "",ProjectId=c.ProjectId };
            var projectTeams = from c in projectTeam group c by c.ProjectName into g  select new ProjectTeamView_Dto { ProjectName = g.Key, TechnologyName = g.Select(p=>p.TechnologyName).FirstOrDefault(),ProjectId=g.Select(p=>p.ProjectId).FirstOrDefault() };
            try
            {
                var projectTeamdto1 = projectTeams.MapTo<List<ProjectTeamView_Dto>>();
            }catch(Exception ex)
            {

            }
            var projectTeamdto = projectTeams.MapTo<List<ProjectTeamView_Dto>>();
            return new ListResultOutput<ProjectTeamView_Dto>(projectTeamdto);


        }
        public async Task<FileDto> GetProjectTeamToExcel(GetProjectTeamInput input)
        {
            long userid = (long)AbpSession.GetUserId();

            var user = await UserManager.GetUserByIdAsync(userid);
            var roleid = user.Roles.ToList();

            string[] userroles = new string[roleid.Count];
            int i = 0;
            foreach (var role in roleid)
            {
                int rol = role.RoleId;
                var userrolesquery = _roleManager.Roles.Where(p => p.Id == rol).OrderBy(p => p.DisplayName).FirstOrDefault();
                userroles[i] = userrolesquery.DisplayName;
                i++;
            }
            var uroles = userroles.ToList();
            var query = _ProjectTeamRepository.GetAll().Where(p => p.Id == 0);

            if (uroles.Contains("Admin"))
            {
                query = _ProjectTeamRepository.GetAll()
                           .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                           u => u.Projects.ProjectName.Contains(input.Filter) ||
                               u.Projects.Technologies.TechnologyName.Contains(input.Filter)
                           );
            }
            else
            {
                query = _ProjectTeamRepository.GetAll().Where(p => p.EmployeeId == userid)
                         .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                         u => u.Projects.ProjectName.Contains(input.Filter) ||
                             u.Projects.Technologies.TechnologyName.Contains(input.Filter)
                         );
            }
            var projectTeam = from c in query select new ProjectTeamView_Dto { ProjectName = c.ProjectId != null ? c.Projects.ProjectName : "", TechnologyName = c.ProjectId != null ? c.Projects.Technologies.TechnologyName : "", ProjectId = c.ProjectId };
            var projectTeams = from c in projectTeam group c by c.ProjectName into g select new ProjectTeamView_Dto { ProjectName = g.Key, TechnologyName = g.Select(p => p.TechnologyName).FirstOrDefault(), ProjectId = g.Select(p => p.ProjectId).FirstOrDefault() };
            try
            {
                var projectTeamdto1 = projectTeams.MapTo<List<ProjectTeamView_Dto>>();
            }
            catch (Exception ex)
            {

            }
            var projectTeamdto = projectTeams.MapTo<List<ProjectTeamView_Dto>>();
            return _ProjectTeamListExcelExporter.ExportToFile(projectTeamdto);
        }
        public async Task<GetProjectTeams> GetProjectTeamForEdit(NullableIdInput<long> input)
        {
            var employeedto = UserManager.Users.Select(p => new Employeedto { EmoloyeeName = p.UserName, EmployeeId = p.Id }).ToArray();
            foreach(var emp in employeedto)
            {
                var projectemp = _ProjectTeamRepository.GetAll().Where(p => p.EmployeeId == emp.EmployeeId&&p.ProjectId==input.Id).FirstOrDefault();
                if(projectemp!=null)
                {
                    emp.IsAssigned = true;
                    emp.Id = projectemp.Id;
                    emp.RoleName=(from c in _ProjectRoleRepository.GetAll().Where(p => p.Id == projectemp.ProjectRoleId) select c.PRoleName).FirstOrDefault();

                }
                else
                {
                    emp.IsAssigned = false;
                }
                var empuser = _EmployeeRepository.GetAll().Where(p => p.UserId == emp.EmployeeId).FirstOrDefault();
                if(empuser!=null)
                {
                    emp.PhotoPath = empuser.PhotoPath!=null?empuser.PhotoPath:"/Common/Images/default-profile-picture.png";
                }
                else
                {
                    emp.PhotoPath = "/Common/Images/default-profile-picture.png";
                }
            }
            var projectTeam = await _ProjectTeamRepository.GetAll().Where(p => p.ProjectId == input.Id).FirstOrDefaultAsync();
          
            string ProjectName = projectTeam.ProjectId!=null?projectTeam.Projects.ProjectName:"";
            var output = new GetProjectTeams {Employee=employeedto ,ProjectName=ProjectName};
           
            output.ProjectTeam = projectTeam.MapTo<CreateProjectTeamInput>();
            
            return output;
        }
        public async Task<GetProjectTeams> GetCompanyForEdit(NullableIdInput<long> input)
        {
            var company = await _CompanyRepository.GetAll().Select(p => new Companydto { CompanyName = p.ProjectName, CompanyId = p.Id }).ToArrayAsync();
            var output = new GetProjectTeams {  Company = company };
            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectTeam_CreateProjectTeam)]
        public async Task CreateProjectTeam(CreateProjectTeamInput input)
        {
            var projectTeam = input.MapTo<ProjectTeam>();
            await _ProjectTeamRepository.InsertAsync(projectTeam);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectTeam_EditProjectTeam)]
        public async Task<GetProjectTeams> GetProjectEmployeeForEdit(NullableIdInput<long> input)
        {
            var employee = UserManager.Users.Select(p => new Employeedto { EmoloyeeName = p.UserName, EmployeeId = p.Id }).ToArray();
            var projectRole = await _ProjectRoleRepository.GetAll().Select(p => new ProjectRoledto { ProjectRoleName = p.PRoleName, ProjectRoleId = p.Id }).ToArrayAsync();
            var output = new GetProjectTeams { ProjectRole = projectRole, Employee = employee };
            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectTeam_EditProjectTeam)]
        public async Task UpdateProjectEmployee(CreateProjectTeamInput input)
        {
           
            var projectTeam = input.MapTo<ProjectTeam>();
            await _ProjectTeamRepository.InsertAsync(projectTeam);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectTeam_DeleteProjectTeam)]
        public async Task DeleteTeamEmplyee(IdInput input)
        {
            await _ProjectTeamRepository.DeleteAsync(input.Id);
        }
    }
}
