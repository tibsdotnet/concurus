﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.ProjectTeams.Dto
{
    public class GetProjectTeams:IOutputDto
    {
        public CreateProjectTeamInput ProjectTeam { get; set; }
        public Employeedto[] Employee { get; set; }
        public Companydto[] Company { get; set; }
        public ProjectRoledto[] ProjectRole { get; set; }
        public string ProjectName { get; set; }
    }
}
