﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.ProjectTeams.Dto
{
    public class ProjectRoledto
    {
        public int ProjectRoleId { get; set; }
        public string ProjectRoleName { get; set; }
    }
}
