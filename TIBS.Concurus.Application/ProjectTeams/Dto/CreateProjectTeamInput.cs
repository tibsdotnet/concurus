﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace TIBS.Concurus.ProjectTeams.Dto
{
    [AutoMapTo(typeof(ProjectTeam))]
    public class CreateProjectTeamInput:IInputDto
    {
        public  int ProjectId { get; set; }
       
        public  long? EmployeeId { get; set; }
        public  DateTime? ActualStart { get; set; }
        public  DateTime? ActualEnd { get; set; }
        public  DateTime? StartDate { get; set; }
        public  DateTime? EndDate { get; set; }
        public  int? Hours { get; set; }
        public  int? ProjectRoleId { get; set; }
    }
}
