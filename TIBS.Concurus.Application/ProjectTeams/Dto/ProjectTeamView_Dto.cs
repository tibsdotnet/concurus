﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.ProjectTeams.Dto
{
    [AutoMapFrom(typeof(ProjectTeam))]
    public class ProjectTeamView_Dto:FullAuditedEntityDto
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string TechnologyName { get; set; }
        public string EmployeeName { get; set; }
    }
}
