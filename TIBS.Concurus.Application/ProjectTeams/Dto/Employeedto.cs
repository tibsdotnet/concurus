﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.ProjectTeams.Dto
{
   public class Employeedto
    {
       public string EmoloyeeName { get; set; }
       public long EmployeeId { get; set; }
       public string PhotoPath { get; set; }
       public string RoleName { get; set; }
       public bool IsAssigned { get; set; }
       public int Id { get; set; }
    }
}
