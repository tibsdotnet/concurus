﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.ProjectTeams.Dto
{
    public class Companydto
    {
        public string CompanyName { get; set; }
        public int CompanyId { get; set; }
    }
}
