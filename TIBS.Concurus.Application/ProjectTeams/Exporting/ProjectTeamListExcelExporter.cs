﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectTeams.Dto;

namespace TIBS.Concurus.ProjectTeams.Exporting
{
    public class ProjectTeamListExcelExporter : EpPlusExcelExporterBase, IProjectTeamListExcelExporter
    {

        public FileDto ExportToFile(List<ProjectTeamView_Dto> ProjectTeamListDtos)
        {
            return CreateExcelPackage(
                "ProjectTeam.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ProjectTeam"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ProjectName"),
                        L("TechnologyName")


                    );

                    AddObjects(
                        sheet, 2, ProjectTeamListDtos,
                        _ => _.ProjectName,
                        _ => _.TechnologyName


                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
