﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectTeams.Dto;

namespace TIBS.Concurus.ProjectTeams.Exporting
{
    public interface IProjectTeamListExcelExporter
    {
        FileDto ExportToFile(List<ProjectTeamView_Dto> ProjectTeamListDtos);
    }
}
