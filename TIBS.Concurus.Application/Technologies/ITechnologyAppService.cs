﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Technologies.Dto;

namespace TIBS.Concurus.Technologies
{
    public interface ITechnologyAppService :IApplicationService
    {
        ListResultOutput<TechnologyView_Dto> GetTechnogies(GetTechnologyInput input);
        Task<GetTechnologies> GetTechnologyForEdit(NullableIdInput<long> input);
        Task CreateOrUpdateTechnology(CreateTechnologyInput input);
        Task DeleteTechnology(IdInput input);
        Task<FileDto> GetTechnogyToExcel(GetTechnologyInput input);
    }
}
