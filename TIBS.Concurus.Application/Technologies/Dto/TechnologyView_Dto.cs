﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace TIBS.Concurus.Technologies.Dto
{
    [AutoMapFrom(typeof(Technology))]
    public class TechnologyView_Dto:FullAuditedEntityDto
    {
        public string TechnologyCode { get; set; }
        public string TechnologyName { get; set; }
    }
}
