﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Technologies.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.AutoMapper;
using Abp.Authorization;
using TIBS.Concurus.Authorization;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Technologies.Exporting;

namespace TIBS.Concurus.Technologies
{
    public class TechnologyAppService : ConcurusAppServiceBase, ITechnologyAppService
    {
        private readonly IRepository<Technology> _TechnologyRepository;
        private readonly ITechnologyListExcelExporter _TechnologyListExcelExporter;
        public TechnologyAppService(IRepository<Technology> TechnologyRepository, ITechnologyListExcelExporter TechnologyListExcelExporter)
        {
            _TechnologyRepository = TechnologyRepository;
            _TechnologyListExcelExporter = TechnologyListExcelExporter;
        }
        public ListResultOutput<TechnologyView_Dto> GetTechnogies(GetTechnologyInput input)
        {
            var query = _TechnologyRepository.GetAll()
                      .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                      u => u.TechnologyCode.Contains(input.Filter) ||
                      u.TechnologyName.Contains(input.Filter)).OrderBy(p => p.TechnologyName)
                .ThenBy(p => p.TechnologyCode)
                .ToList();
            
            return new ListResultOutput<TechnologyView_Dto>(query.MapTo<List<TechnologyView_Dto>>());
        }
        public async Task<FileDto> GetTechnogyToExcel(GetTechnologyInput input)
        {
            var query =await _TechnologyRepository.GetAll()
                     .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                     u => u.TechnologyCode.Contains(input.Filter) ||
                     u.TechnologyName.Contains(input.Filter)).OrderBy(p => p.TechnologyName)
               .ThenBy(p => p.TechnologyCode)
               .ToListAsync();
            return _TechnologyListExcelExporter.ExportToFile(query.MapTo<List<TechnologyView_Dto>>());
        }
        public async Task<GetTechnologies> GetTechnologyForEdit(NullableIdInput<long> input)
        {
            var output = new GetTechnologies { };
            var tech =await _TechnologyRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            output.Technology = tech.MapTo<CreateTechnologyInput>();
            return output;
        }
        public async Task CreateOrUpdateTechnology(CreateTechnologyInput input)
        {
            if(input.Id!=0)
            {
                await UpdateTechnogy(input);

            }
            else
            {
                await CreateTechnology(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Technology_CreateTechnology)]
        public async Task CreateTechnology(CreateTechnologyInput input)
        {
            var tech = input.MapTo<Technology>();
            await _TechnologyRepository.InsertAsync(tech);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Technology_EditTechnogy)]
        public async Task UpdateTechnogy(CreateTechnologyInput input)
        {
            var tech = input.MapTo<Technology>();
            await _TechnologyRepository.UpdateAsync(tech);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Technology_DeleteTechnology)]
        public async Task DeleteTechnology(IdInput input)
        {
            await _TechnologyRepository.DeleteAsync(input.Id);
        }

    }
}
