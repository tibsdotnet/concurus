﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Technologies.Dto;

namespace TIBS.Concurus.Technologies.Exporting
{
    public class TechnologyListExcelExporter : EpPlusExcelExporterBase, ITechnologyListExcelExporter
    {

        public FileDto ExportToFile(List<TechnologyView_Dto> technologyListDtos)
        {
            return CreateExcelPackage(
                "Technology.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Country"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("TechnologyName"),
                        L("TechnologyCode")


                    );

                    AddObjects(
                        sheet, 2, technologyListDtos,
                        _ => _.TechnologyName,
                        _ => _.TechnologyCode


                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
