﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Technologies.Dto;

namespace TIBS.Concurus.Technologies.Exporting
{
    public interface ITechnologyListExcelExporter
    {
        FileDto ExportToFile(List<TechnologyView_Dto> technologyListDtos);
    }
}
