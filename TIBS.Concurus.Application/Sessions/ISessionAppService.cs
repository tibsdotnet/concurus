﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.Concurus.Sessions.Dto;

namespace TIBS.Concurus.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
