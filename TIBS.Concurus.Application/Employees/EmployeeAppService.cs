﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Employees.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using System.Linq.Dynamic;
using Abp.AutoMapper;
using TIBS.Concurus.Locationn;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Cityy;
using TIBS.Concurus.Countryy;
using TIBS.Concurus.Employees.Exporting;

namespace TIBS.Concurus.Employees
{
    public class EmployeeAppService:ConcurusAppServiceBase,IEmployeeAppService
    {
        private readonly IRepository<Employee> _EmployeeRepository;
        private readonly IRepository<Location> _LocationRepository;
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<Country> _CountryRepository;
        private readonly IEmployeeListExcelExporter _EmployeeListExcelExporter;
        public EmployeeAppService(IRepository<Employee> EmployeeRepository, IRepository<Location> LocationRepository, IRepository<City> cityRepository, IRepository<Country> CountryRepository, IEmployeeListExcelExporter EmployeeListExcelExporter)
        {
            _EmployeeRepository = EmployeeRepository;
            _LocationRepository = LocationRepository;
            _cityRepository = cityRepository;
            _CountryRepository = CountryRepository;
            _EmployeeListExcelExporter = EmployeeListExcelExporter;
        }
        public async Task<PagedResultOutput<EmployeeViewDto>> GetEmployees(GetEmployeeInput input)
        {
            var employeequery=_EmployeeRepository.GetAll()
               .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.FirstName.Contains(input.Filter) ||
                        u.LastName.Contains(input.Filter) ||
                        u.Countries.CountryName.Contains(input.Filter) ||
                        u.Id.ToString().Contains(input.Filter)||
                        u.CountryId.ToString().Contains(input.Filter)||
                        u.cities.CityName.ToString().Contains(input.Filter)||
                        u.Locations.LocationName.Contains(input.Filter)||
                        u.PhoneNo.Contains(input.Filter)||
                        u.RateForHour.ToString().Contains(input.Filter)||
                        u.BirthDate.ToString().Contains(input.Filter)||
                        u.HireDate.ToString().Contains(input.Filter)
                );
             var employeecount = await employeequery.CountAsync();
            var employees = await employeequery
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
            var Employeedto = (from c in employees select new EmployeeViewDto { FirstName = c.FirstName, LastName = c.LastName, CityName = c.CityId != null ? c.cities.CityName : "", CountryName = c.CountryId != null ? c.Countries.CountryName : "", HireDate = c.HireDate.ToString(), BirthDate = c.BirthDate.ToString(), LocationName = c.LocationId != null ? c.Locations.LocationName : "", PhoneNo = c.PhoneNo, RateForHour = c.RateForHour, Sex = c.Sex, Address = c.Address, UserStatus = c.UserId != null ? "User Created" : "Not Yet Created", Id = c.Id, PhotoPath=c.PhotoPath});
            var employeedtos = Employeedto.MapTo<List<EmployeeViewDto>>();

            return new PagedResultOutput<EmployeeViewDto>(employeecount, employeedtos);

        }
        public async Task<FileDto> GetEmployeeToExcel()
        {

            var employees =await _EmployeeRepository
                .GetAll().ToListAsync();
            var Employeedto = (from c in employees select new EmployeeViewDto { FirstName = c.FirstName, LastName = c.LastName, CityName = c.CityId != null ? c.cities.CityName : "", CountryName = c.CountryId != null ? c.Countries.CountryName : "", HireDate = c.HireDate.ToString(), BirthDate = c.BirthDate.ToString(), LocationName = c.LocationId != null ? c.Locations.LocationName : "", PhoneNo = c.PhoneNo, RateForHour = c.RateForHour, Sex = c.Sex, Address = c.Address, UserStatus = c.UserId != null ? "User Created" : "Not Yet Created", Id = c.Id, PhotoPath = c.PhotoPath });


            var employeedtos = Employeedto.MapTo<List<EmployeeViewDto>>();
            return _EmployeeListExcelExporter.ExportToFile(employeedtos);
        }
        public async Task<GetEmployee> GetEmployeeForEdit(NullableIdInput<long> input)
        {
            
            var Employee = _EmployeeRepository.GetAll().Where(p => p.Id==input.Id).FirstOrDefault();
            var location =await _LocationRepository.GetAll().Select(p => new LocationDto { LocationId = p.Id, LocationName = p.LocationName }).ToArrayAsync();
            var output = new GetEmployee
                             { 
                                 Loaction=location
                             };
            output.Employee = Employee.MapTo<CreateEmployeeInput>();
            return output;

        }
        public async Task CreateOrUpdateEmployee(CreateEmployeeInput input)
        {
            if(input.Id!=0)
            {
                await UpdateEmployee(input);
            }
            else
            {
                await CreateEmployee(input);
            }
        }
        public async Task CreateEmployee(CreateEmployeeInput input)
        {
            if(input.LocationId!=0&&input.LocationId!=null)
            {
                input.CityId = (from c in _LocationRepository.GetAll().Where(p => p.Id == input.LocationId) select c.CityId).FirstOrDefault();
                input.CountryId = (from c in _cityRepository.GetAll().Where(p => p.Id == input.CityId) select c.CountryId).FirstOrDefault();
            }
            
            var employee = input.MapTo<Employee>();
            await _EmployeeRepository.InsertAsync(employee);
        }
        public async Task UpdateEmployee(CreateEmployeeInput input)
        {
            if (input.LocationId != 0 && input.LocationId != null)
            {
                input.CityId = (from c in _LocationRepository.GetAll().Where(p => p.Id == input.LocationId) select c.CityId).FirstOrDefault();
                input.CountryId = (from c in _cityRepository.GetAll().Where(p => p.Id == input.CityId) select c.CountryId).FirstOrDefault();
            }
            var employee = input.MapTo<Employee>();
            await _EmployeeRepository.UpdateAsync(employee);
        }
        public async Task DeleteEmployee(IdInput input)
        {
            await _EmployeeRepository.DeleteAsync(input.Id);
        }
        public virtual async Task UpdateAccount(AccountInput input)
        {
            if (input.AccountId == 0)
            {
                var query = _EmployeeRepository.GetAll();
                var emp = (from c in query where c.Id == input.Id select c).FirstOrDefault();





                emp.UserId = (UserManager.Users.Select(x => (int?)x.Id).Max() ?? 0);

                var employee = emp.MapTo<Employee>();
                try
                {
                    await _EmployeeRepository.UpdateAsync(employee);
                }
                catch (Exception ex)
                {
                }

            }

        }
        public string GetProfile(NullableIdInput<int> input)
        {
            string Path = "";

            var query = (from r in _EmployeeRepository.GetAll()
                         where r.Id == input.Id
                         orderby r.Id descending
                         select r).FirstOrDefault();
            if (query != null)
            {
                Path = query.PhotoPath;

            }

            return Path;
        }
    }
}
