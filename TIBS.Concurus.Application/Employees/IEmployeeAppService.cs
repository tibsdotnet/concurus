﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Employees.Dto;

namespace TIBS.Concurus.Employees
{
    public interface IEmployeeAppService:IApplicationService
    {
        Task<PagedResultOutput<EmployeeViewDto>> GetEmployees(GetEmployeeInput input);
        Task<GetEmployee> GetEmployeeForEdit(NullableIdInput<long> input);
        Task CreateOrUpdateEmployee(CreateEmployeeInput input);
        Task DeleteEmployee(IdInput input);
        Task UpdateAccount(AccountInput input);
        string GetProfile(NullableIdInput<int> input);
        Task<FileDto> GetEmployeeToExcel();
    }
}
