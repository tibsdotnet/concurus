﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Employees.Dto;

namespace TIBS.Concurus.Employees.Exporting
{
    class EmployeeListExcelExporter : EpPlusExcelExporterBase,IEmployeeListExcelExporter
    {
        public FileDto ExportToFile(List<EmployeeViewDto> employeeListDtos)
        {
            return CreateExcelPackage(
                "Employee.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Country"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("FirstName"),
                        L("LastName"),
                        L("Email"),
                        L("CountryName"),
                        L("HireDate"),
                        L("PhoneNo"),
                        L("RateForHour"),
                        L("UserStatus")


                    );

                    AddObjects(
                        sheet, 2, employeeListDtos,
                        
                        _ => _.FirstName,
                        _=>_.LastName,
                        _=>_.Email,
                        _ => _.CountryName,
                        _ => _.HireDate,
                        _ => _.PhoneNo,
                        _ => _.RateForHour,
                        _ => _.UserStatus
                        //_ => _.LastModificationTime,
                        //_ => _.LastModifierUserId,
                        //_ => _.CreationTime,
                        //_ => _.CreatorUserId

                        );
                

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
