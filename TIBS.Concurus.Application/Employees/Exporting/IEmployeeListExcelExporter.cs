﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Employees.Dto;

namespace TIBS.Concurus.Employees.Exporting
{
    public interface IEmployeeListExcelExporter
    {
        FileDto ExportToFile(List<EmployeeViewDto> employeeListDtos);
    }
}
