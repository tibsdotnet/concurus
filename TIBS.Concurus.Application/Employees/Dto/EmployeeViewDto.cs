﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace TIBS.Concurus.Employees.Dto
{
    [AutoMapFrom(typeof(Employee))]
   public class EmployeeViewDto:FullAuditedEntityDto
    {
        public  string FirstName { get; set; }
        public  string LastName { get; set; }
        public  string BirthDate { get; set; }
        public  string HireDate { get; set; }
        public  string Address { get; set; }
        public  string PhoneNo { get; set; }
        public  string Email { get; set; }
        public string PhotoPath { get; set; }
        
        
        public  string CountryName { get; set; }
        
        public  string CityName { get; set; }
       
        public  string  LocationName { get; set; }
        public  string Sex { get; set; }
        public  decimal? RateForHour { get; set; }
        public  int Age { get; set; }
        public  string UserStatus { get; set; }
        public int UserId { get; set; }
    }
}
