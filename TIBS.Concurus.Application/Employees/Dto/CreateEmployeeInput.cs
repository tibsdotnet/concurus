﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;
namespace TIBS.Concurus.Employees.Dto
{
    [AutoMapTo(typeof(Employee))]
    public class CreateEmployeeInput:IInputDto
    {
        public int Id { get; set; }
        public  string FirstName { get; set; }
        public  string LastName { get; set; }
        public  DateTime? BirthDate { get; set; }
        public  DateTime? HireDate { get; set; }
        public  string Address { get; set; }
        public  string PhoneNo { get; set; }
        public  string Email { get; set; }
        public string PhotoPath { get; set; }
        
        public  long? UserId { get; set; }
        
        public  int? CountryId { get; set; }
        
        public  int? CityId { get; set; }
        
        public  int? LocationId { get; set; }
        public  string Sex { get; set; }
        public  decimal? RateForHour { get; set; }
        public  int? Age { get; set; }
    }
}
