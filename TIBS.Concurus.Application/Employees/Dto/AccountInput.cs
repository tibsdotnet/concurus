﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Employees.Dto
{
    [AutoMapTo(typeof(Employee))]
    public class AccountInput : IInputDto
    {
       public int Id { get; set; }

       public int AccountId { get; set; }
       public string Path { get; set; }

    }
}
