﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
namespace TIBS.Concurus.Employees.Dto
{
    public class GetEmployee:IOutputDto
    {
        public CreateEmployeeInput Employee { get; set; }
        public LocationDto[] Loaction { get; set; }
    }
}
