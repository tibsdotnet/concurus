﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;
using TIBS.Concurus.Authorization;

namespace TIBS.Concurus
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(typeof(ConcurusCoreModule), typeof(AbpAutoMapperModule))]
    public class ConcurusApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Adding authorization providers
            Configuration.Authorization.Providers.Add<AppAuthorizationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            //Custom DTO auto-mappings
            CustomDtoMapper.CreateMappings();
        }
    }
}
