﻿using System.Collections.Generic;
using TIBS.Concurus.Auditing.Dto;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
