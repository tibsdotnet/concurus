﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.Concurus.Configuration.Tenants.Dto;

namespace TIBS.Concurus.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);
    }
}
