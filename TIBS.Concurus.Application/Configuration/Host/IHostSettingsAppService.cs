﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.Concurus.Configuration.Host.Dto;

namespace TIBS.Concurus.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);
    }
}
