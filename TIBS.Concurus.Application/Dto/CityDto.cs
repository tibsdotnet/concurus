﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Cityy.Dto;
using TIBS.Concurus.Cityy.Dto;

namespace TIBS.Concurus.Dto
{
    public class CityDto : IDto
    {
        public int CityId { get; set; }

        public string CityName { get; set; }
        public CreateCityInput[] cities { get; set; }
        public int CountryId { get; set; }
    }
    
}
