﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Dto
{
    public class LocationDto : IDto
    {

        public int LocationId { get; set; }

        public string LocationName { get; set; }
    }
}
