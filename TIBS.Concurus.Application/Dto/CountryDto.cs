﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Dto
{
    public class CountryDto : IDto
    {
        public int CountryId { get; set; }

        public string CountryName { get; set; }
    }
}
