﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.ProjectRoles.Dto
{
    public class ProjectRoleInput : IInputDto
    {
        public string Filter { get; set; }
    }
}
