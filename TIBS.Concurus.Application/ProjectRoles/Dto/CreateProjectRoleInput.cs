﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.ProjectRoless;

namespace TIBS.Concurus.ProjectRoles.Dto
{
    [AutoMapTo(typeof(ProjectRole))]
    public class CreateProjectRoleInput : IInputDto
    {
        public string PRoleCode { get; set; }
        public string PRoleName { get; set; }
        public int Id { get; set; }
    }
}
