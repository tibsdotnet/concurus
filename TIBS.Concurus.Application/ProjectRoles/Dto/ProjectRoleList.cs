﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.ProjectRoless;

namespace TIBS.Concurus.ProjectRoles.Dto
{
    [AutoMapFrom(typeof(ProjectRole))]
    public class ProjectRoleList : FullAuditedEntityDto
    {
        public string PRoleCode { get; set; }
        public string PRoleName { get; set; }
    }
}
