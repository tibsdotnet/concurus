﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.ProjectRoles.Dto
{
    public class GetProjectRole : IOutputDto
    {
        public CreateProjectRoleInput projectRole { get; set; }
    }
}
