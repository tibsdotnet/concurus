﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectRoles.Dto;

namespace TIBS.Concurus.ProjectRoles.Export
{
    public interface IProjectRoleExport
    {
        FileDto ExportToFile(List<ProjectRoleList> projectRoleList);
    }
}
