﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectRoles.Dto;

namespace TIBS.Concurus.ProjectRoles.Export
{
    public class ProjectRoleExport : EpPlusExcelExporterBase, IProjectRoleExport
    {
        public FileDto ExportToFile(List<ProjectRoleList> projectRoleList)
        {
            return CreateExcelPackage(
                "ProjectRole.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ProjectRole"));
                    sheet.OutLineApplyStyle = true;
                    AddHeader(
                        sheet,
                        L("ProjectRoleCode"),
                        L("ProjectRoleName")
                    );

                    AddObjects(
                        sheet, 2, projectRoleList,
                        _ => _.PRoleName,
                        _ => _.PRoleCode
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(2);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 2; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }


    }
}

