﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.ProjectRoles.Dto;
using TIBS.Concurus.ProjectRoless;
using Abp.AutoMapper;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.Domain.Repositories;
using Abp.Authorization;
using TIBS.Concurus.Authorization;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectRoles.Export;

namespace TIBS.Concurus.ProjectRoles
{
    public class ProjectRoleAppService : ConcurusAppServiceBase, IProjectRoleAppService
    {
        private readonly IRepository<ProjectRole> _ProjectRoleRepository;
        private readonly IProjectRoleExport _ProjectRoleExport;

        public ProjectRoleAppService(IProjectRoleExport ProjectRoleExport, IRepository<ProjectRole> ProjectRoleRespository)
        {
            _ProjectRoleRepository = ProjectRoleRespository;
            _ProjectRoleExport = ProjectRoleExport;
        }
        public ListResultOutput<ProjectRoleList> GetProjectRole(ProjectRoleInput input)
        {
            var projectrole =_ProjectRoleRepository
                .GetAll()
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    p =>p.Id.ToString().Contains(input.Filter)||
                        p.PRoleName.Contains(input.Filter) ||
                         p.PRoleCode.Contains(input.Filter) 
                )
                .OrderBy(p => p.PRoleName)
                .ThenBy(p => p.PRoleCode)
                .ToList();
            return new ListResultOutput<ProjectRoleList>(projectrole.MapTo<List<ProjectRoleList>>());
        }

        public async Task<FileDto> GetProjectRoleToExcel()
        {

            var projectrole = _ProjectRoleRepository
                .GetAll();
            var projectrolelist = projectrole.MapTo<List<ProjectRoleList>>();


            return _ProjectRoleExport.ExportToFile(projectrolelist);
        }

        public async Task CreateOrUpdateProjectRole(CreateProjectRoleInput input)
        {
            if (input.Id != 0)
            {
                await UpdateProjectRoleAsync(input);
            }
            else
            {
                await CreateProjectRoleAsync(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_ProjectRole_CreateProjectRole)]
        public virtual async Task CreateProjectRoleAsync(CreateProjectRoleInput input)
        {
            var projectrole = input.MapTo<ProjectRole>();
            var val = _ProjectRoleRepository
             .GetAll().Where(p => p.PRoleCode == input.PRoleCode || p.PRoleName == input.PRoleName).FirstOrDefault();
            if (val == null)
            {
                await _ProjectRoleRepository.InsertAsync(projectrole);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Project Role Name '" + input.PRoleName + "' or Project Role Code '" + input.PRoleCode + "'..."); 
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_ProjectRole_EditProjectRole)]
        public virtual async Task UpdateProjectRoleAsync(CreateProjectRoleInput input)
        {
            var projectrole = input.MapTo<ProjectRole>();
            projectrole.PRoleName = input.PRoleName;
            projectrole.PRoleCode = input.PRoleCode;
            var val = _ProjectRoleRepository
            .GetAll().Where(p => (p.PRoleCode == input.PRoleCode || p.PRoleName == input.PRoleName )&& p.Id != input.Id).FirstOrDefault();
            if (val == null)
            {
                await _ProjectRoleRepository.UpdateAsync(projectrole);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Project Role Name '" + input.PRoleName + "' or Project Role Code '" + input.PRoleCode + "'...");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_ProjectRole_DeleteProjectRole)]
        public async Task DeleteProjectRole(IdInput input)
        {
            await _ProjectRoleRepository.DeleteAsync(input.Id);
        }
        public async Task<GetProjectRole> GetProjectRoleForEdit(NullableIdInput<long> input)
        {
            var output = new GetProjectRole
            {
            };

            var projectrole = _ProjectRoleRepository
                .GetAll().Where(p => p.Id == input.Id).FirstOrDefault();

            output.projectRole = projectrole.MapTo<CreateProjectRoleInput>();

            return output;

        }
    }
}

