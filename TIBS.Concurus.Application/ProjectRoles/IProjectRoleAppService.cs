﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectRoles.Dto;

namespace TIBS.Concurus.ProjectRoles
{
    public interface IProjectRoleAppService : IApplicationService
    {
        ListResultOutput<ProjectRoleList> GetProjectRole(ProjectRoleInput input);
        Task CreateOrUpdateProjectRole(CreateProjectRoleInput input);
        Task DeleteProjectRole(IdInput input);
        Task<GetProjectRole> GetProjectRoleForEdit(NullableIdInput<long> input);
        Task<FileDto> GetProjectRoleToExcel();
    }
}
