﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TIBS.Concurus.Cityy.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Cityy
{
    public interface ICityAppService : IApplicationService
    {
        Task<PagedResultOutput<CityListDto>> GetCities(GetCityInput input);

        Task<GetCity> GetCityForEdit(NullableIdInput<long> input);

        Task CreateOrUpdateCity(CreateCityInput input);

        Task DeleteCity(IdInput input);
        Task<FileDto> GetCityToExcel();

    }
}
