﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Cityy.Dto;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Cityy.Exporting
{
    public class CityListExcelExporter : EpPlusExcelExporterBase, ICityListExcelExporter
    {

        public FileDto ExportToFile(List<CityListDto> cityListDtos)
        {
            return CreateExcelPackage(
                "City.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("City"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CityName"),
                        L("CityCode"),
                        L("CountryName")
                        //L("IsDeleted"),
                        //L("DeleterUserId"),
                        //L("DeletionTime"),
                        //L("LastModificationTime"),
                        //L("LastModifierUserId"),
                        //L("CreationTime"),
                        //L("CreatorUserId")
                    );

                    AddObjects(
                        sheet, 2, cityListDtos,
                        _ => _.CityName,
                        _ => _.CityCode,
                        _ => _.CountryName
                        //_ => _.IsDeleted,
                        //_ => _.DeleterUserId,
                        //_ => _.DeletionTime,
                        //_ => _.LastModificationTime,
                        //_ => _.LastModifierUserId,
                        //_ => _.CreationTime,
                        //_ => _.CreatorUserId

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(4);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 4; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
