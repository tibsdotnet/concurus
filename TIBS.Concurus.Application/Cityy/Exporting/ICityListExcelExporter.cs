﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Cityy.Dto;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Cityy.Exporting
{
    public interface ICityListExcelExporter
    {
        FileDto ExportToFile(List<CityListDto> cityListDtos);
    }
}
