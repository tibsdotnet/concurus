﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using TIBS.Concurus.Authorization;
using TIBS.Concurus.Cityy.Dto;
using TIBS.Concurus.Countryy;
using System;
using TIBS.Concurus.Cityy.Exporting;
using TIBS.Concurus.Dto;
using System.Data.SqlClient;

using Abp.UI;

namespace TIBS.Concurus.Cityy
{
    public class CityAppService : ConcurusAppServiceBase, ICityAppService 
    {
        private readonly IRepository<City> _CityRepository;

        private readonly IRepository<Country> _CountryRepository;
        private readonly ICityListExcelExporter _cityListExcelExporter;
        public CityAppService(ICityListExcelExporter cityListExcelExporter, IRepository<City> CityRepository, IRepository<Country> CountryRepository)
        {
            _CityRepository = CityRepository;
            _CountryRepository = CountryRepository;
            _cityListExcelExporter = cityListExcelExporter;
        }

        public async Task<PagedResultOutput<CityListDto>> GetCities(GetCityInput input)
        {
            var query = _CityRepository.GetAll()
                .Include(u => u.Country)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.CityName.Contains(input.Filter) ||
                        u.CityCode.Contains(input.Filter) ||
                        u.ISDCode.Contains(input.Filter) ||
                        u.Id.ToString().Contains(input.Filter)||
                        u.CountryId.ToString().Contains(input.Filter)||
                        u.Country.CountryName.ToString().Contains(input.Filter)
                );

            var cityCount = await query.CountAsync();
            var cities = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            //var cities = _CityRepository.GetAll()
            //    .Include(u => u.Country)
            //    .WhereIf(
            //        !input.Filter.IsNullOrWhiteSpace(),
            //        u =>
            //            u.CityName.Contains(input.Sorting) ||
            //            u.CityCode.Contains(input.Sorting) ||
            //            u.ISDCode.Contains(input.Sorting) ||
            //            u.Id.ToString().Contains(input.Sorting) ||
            //            u.CountryId.ToString().Contains(input.Sorting) ||
            //            u.Country.CountryName.ToString().Contains(input.Sorting)
            //    );


            var cityListDtos = cities.MapTo<List<CityListDto>>();
            await FillCountryNames(cityListDtos);

            return new PagedResultOutput<CityListDto>(
                cityCount,
                cityListDtos
                );
        }

        public async Task<FileDto> GetCityToExcel()
        {

            var city = _CityRepository
                .GetAll();
            var cityListDtos = city.MapTo<List<CityListDto>>();

            await FillCountryNames(cityListDtos);
            return _cityListExcelExporter.ExportToFile(cityListDtos);
        }

        private async Task FillCountryNames(List<CityListDto> cityListDtos)
        {
            /* This method is optimized to fill role names to given list. */

            var distinctcountryIds = (
                from cityListDto in cityListDtos
                select cityListDto.CountryId
                ).Distinct();

            var countryNames = new Dictionary<int, string>();
            foreach (var countryId in distinctcountryIds)
            {
                int cid = Convert.ToInt32(countryId);
                var coun = (_CountryRepository.GetAll().Where(p => p.Id == countryId)).FirstOrDefault();
                countryNames[countryId] = coun.CountryName.ToString();
            }

            foreach (var countryListDto in cityListDtos)
            {
                countryListDto.CountryName = countryNames[countryListDto.CountryId];
            }
        }

        public async Task<GetCity> GetCityForEdit(NullableIdInput<long> input)
        {
            var city = _CityRepository
               .GetAll().Where(p => p.Id == input.Id).FirstOrDefault();

            var output = new GetCity { };

           
                var citycountryDtos = (await _CountryRepository.GetAll()
                    .OrderBy(r => r.CountryName)
                    .Select(r => new CityCountryDto
                    {
                        CountryId = r.Id,
                        CountryName = r.CountryName

                    })
                    .ToArrayAsync());

                output.CountryName = citycountryDtos;
            

            output.city = city.MapTo<CreateCityInput>();

            return output;

        }

        public async Task CreateOrUpdateCity(CreateCityInput input)
        {
            if (input.Id!=0)
            {
                await UpdateCityAsync(input);
            }
            else
            {
                await CreateCityAsync(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Geography_City_CreateNewCity)]
        public virtual async Task CreateCityAsync(CreateCityInput input)
        {
            var City = input.MapTo<City>();
            DateTime myUtcDateTime = DateTime.Now;
            City.LastModificationTime = DateTime.Now.AddHours(-12);
            var val = _CityRepository
              .GetAll().Where(p => p.CityName == input.CityName || p.CityCode == input.CityCode).FirstOrDefault();

            if (val == null)
            {
                await _CityRepository.InsertAsync(City);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in City Name '" + input.CityName + "' or City Code '"+input.CityCode+"'...");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Geography_City_EditCity)]
        public virtual async Task UpdateCityAsync(CreateCityInput input)
        {
            var City = input.MapTo<City>();
            City.CityName = input.CityName;
            City.CityCode = input.CityCode;
            City.ISDCode = input.ISDCode;
            City.CountryId = (int)input.CountryId;

            var val = _CityRepository
             .GetAll().Where(p =>( p.CityName == input.CityName || p.CityCode == input.CityCode) && p.Id != input.Id).FirstOrDefault();
            if (val == null)
            {
                await _CityRepository.UpdateAsync(City);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in City Name '" + input.CityName + "' or City Code '" + input.CityCode + "'...");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Geography_City_DeleteCity)]
        public async Task DeleteCity(IdInput input)
        {
           
           
                        await _CityRepository.DeleteAsync(input.Id);
                   
           
        }
    }
    public class FindDelete
    {
        public int id { get; set; }

        public string name { get; set; }
    }
}
