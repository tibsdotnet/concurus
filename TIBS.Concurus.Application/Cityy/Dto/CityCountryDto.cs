﻿using Abp.Application.Services.Dto;


namespace TIBS.Concurus.Cityy.Dto
{
    public class CityCountryDto:IDto
    {
        public int CountryId { get; set; }

        public string CountryName { get; set; }
    }
}
