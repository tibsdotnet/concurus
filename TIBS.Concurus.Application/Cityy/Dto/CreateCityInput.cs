﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;


namespace TIBS.Concurus.Cityy.Dto
{
    [AutoMapTo(typeof(City))]
    public class CreateCityInput:IInputDto
    {
        [Required]
        [MaxLength(City.NameLength)]
        public string CityName { get; set; }

        [Required]
        [MaxLength(City.CodeLength)]
        public string CityCode { get; set; }

        [MaxLength(City.CodeLength)]
        public string ISDCode { get; set; }

        public long Id { get; set; }

        [Required]
        public long CountryId { get; set; }
    }
}
