﻿using System.Collections.ObjectModel;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using TIBS.Concurus.Countryy;
using System.Collections.Generic;
using System;

namespace TIBS.Concurus.Cityy.Dto
{
   [AutoMapFrom(typeof(City))]
    public class CityListDto : EntityDto<long>, IHasCreationTime
    {
        public string CityName { get; set; }

        public string CityCode { get; set; }

        public string ISDCode { get; set; }

        public int CountryId { get; set; }

        public string CountryName { get; set; }

        public DateTime CreationTime { get; set; }

    }
}
