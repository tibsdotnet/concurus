﻿using Abp.Application.Services.Dto;
using TIBS.Concurus.Countryy.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace TIBS.Concurus.Cityy.Dto
{
    public class GetCity:IInputDto
    {
        public CreateCityInput city { get; set; }

        public CityCountryDto[] CountryName { get; set; }
    }
}
