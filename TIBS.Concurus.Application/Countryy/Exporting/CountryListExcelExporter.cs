﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Extensions;
using TIBS.Concurus.Countryy.Dto;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Countryy.Exporting
{
    public class CountryListExcelExporter : EpPlusExcelExporterBase, ICountryListExcelExporter
    {

        public FileDto ExportToFile(List<CountryListDto> countryListDtos)
        {
            return CreateExcelPackage(
                "Country.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Country"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CountryName"),
                        L("CountryCode"),
                        L("ISDCode")
                       
                    );

                    AddObjects(
                        sheet, 2, countryListDtos,
                        _ => _.CountryName,
                        _ => _.CountryCode,
                        _ => _.ISDCode
                        //_ => _.IsDeleted,
                        //_ => _.DeleterUserId,
                        //_ => _.DeletionTime,
                        //_ => _.LastModificationTime,
                        //_ => _.LastModifierUserId,
                        //_ => _.CreationTime,
                        //_ => _.CreatorUserId
                        
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
