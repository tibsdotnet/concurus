﻿using System.Collections.ObjectModel;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace TIBS.Concurus.Countryy.Dto
{
    [AutoMapFrom(typeof(Country))]
    public class CountryListDto:FullAuditedEntityDto
    {
        public string CountryName { get; set; }

        public string CountryCode { get; set; }

        public string ISDCode { get; set; }
    }
}
