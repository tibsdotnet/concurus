﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace TIBS.Concurus.Countryy.Dto
{
    [AutoMapTo(typeof(Country))]
    public class CreateCountryInput:IInputDto
    {
        [Required]
        [MaxLength(Country.NameLength)]
        public string CountryName { get; set; }

        [Required]
        [MaxLength(Country.CodeLength)]
        public string CountryCode { get; set; }

        [MaxLength(Country.ISDLength)]
        public string ISDCode { get; set; }

        public long Id { get; set; }
    }
}
