﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using TIBS.Concurus.Authorization;
using TIBS.Concurus.Countryy;
using TIBS.Concurus.Countryy.Dto;
using TIBS.Concurus.Countryy.Exporting;
using TIBS.Concurus.Dto;
using System;
using System.Data.SqlClient;
using Abp.UI;

namespace TIBS.Concurus.Countryy
{
    public class CountryAppService : ConcurusAppServiceBase, ICountryAppService
    {
        private readonly IRepository<Country> _CountryRepository;
        private readonly ICountryListExcelExporter _countryListExcelExporter;

        public CountryAppService(ICountryListExcelExporter countryListExcelExporter, IRepository<Country> CountryRepository)
        {
            _CountryRepository = CountryRepository;
            _countryListExcelExporter = countryListExcelExporter;
        }

        public ListResultOutput<CountryListDto> GetCountry(GetCountryInput input)
        {
            var Country = _CountryRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Id.ToString().Contains(input.Filter) ||
                        p.CountryName.Contains(input.Filter) ||
                         p.CountryCode.Contains(input.Filter) ||
                         p.ISDCode.Contains(input.Filter)
                )
                .OrderBy(p => p.CountryName)
                .ThenBy(p => p.CountryCode)
                .ToList();

            return new ListResultOutput<CountryListDto>(Country.MapTo<List<CountryListDto>>());
        }

        public async Task<FileDto> GetCountryToExcel()
        {

            var country = _CountryRepository
                .GetAll();
            var countryListDtos = country.MapTo<List<CountryListDto>>();


            return _countryListExcelExporter.ExportToFile(countryListDtos);
        }


        public async Task<GetCountry> GetCountryForEdit(NullableIdInput<long> input)
        {
            var output = new GetCountry
            {
            };

            var country =  _CountryRepository
                .GetAll().Where(p => p.Id == input.Id).FirstOrDefault();

            output.country =  country.MapTo<CreateCountryInput>();

            return output;

        }

        public async Task CreateOrUpdateUser(CreateCountryInput input)
        {
            if (input.Id!=0)
            {
                await UpdateCountryAsync(input);
            }
            else
            {
                await CreateCountryAsync(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Geography_Country_CreateNewCountry)]

        public virtual async Task CreateCountryAsync(CreateCountryInput input)
        {
            var Country = input.MapTo<Country>();
            DateTime myUtcDateTime = DateTime.Now;

            Country.LastModificationTime = DateTime.Now.AddHours(-12);
            var val = _CountryRepository
              .GetAll().Where(p => p.CountryCode == input.CountryCode || p.CountryName == input.CountryName).FirstOrDefault();
            if (val == null)
            {
                await _CountryRepository.InsertAsync(Country);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Country Name '" + input.CountryName + "' or Country Code '" + input.CountryCode + "'...");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Geography_Country_EditCountry)]

        public virtual async Task UpdateCountryAsync(CreateCountryInput input)
        {
            var Country = input.MapTo<Country>();
            Country.CountryName = input.CountryName;
            Country.CountryCode = input.CountryCode;
            Country.ISDCode = input.ISDCode;
            var val = _CountryRepository
              .GetAll().Where(p => (p.CountryCode == input.CountryCode || p.CountryName == input.CountryName) && p.Id != input.Id).FirstOrDefault();
            if (val == null)
            {
                await _CountryRepository.UpdateAsync(Country);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Country Name '" + input.CountryName + "' or Country Code '" + input.CountryCode + "'...");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Geography_Country_DeleteCountry)]

        public async Task DeleteCountry(IdInput input)
        {
            
                        await _CountryRepository.DeleteAsync(input.Id);
                  
            
        }

    }
    public class FindDelete
    {
        public int id { get; set; }

        public string name { get; set; }
    }
}
