﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;

using TIBS.Concurus.Countryy.Dto;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Countryy
{
    public interface  ICountryAppService : IApplicationService
    {
        ListResultOutput<CountryListDto> GetCountry(GetCountryInput input);

        Task CreateOrUpdateUser(CreateCountryInput input);


        Task<GetCountry> GetCountryForEdit(NullableIdInput<long> input);

        Task DeleteCountry(IdInput input);

        Task<FileDto> GetCountryToExcel();
    }
}
