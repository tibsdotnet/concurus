﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.ProjectTypes.Dto
{
    [AutoMapFrom(typeof(ProjectType))]
    public class ProjectTypeView_dto:FullAuditedEntityDto
    {
        public string ProjectTypeCode { get; set; }
        public string ProjectTypeName { get; set; }
    }
}
