﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.ProjectTypes.Dto
{
    [AutoMapTo(typeof(ProjectType))]
    public class CreateProjectTypeInput:IInputDto
    {
        public int Id { get; set; }
        public string ProjectTypeCode { get; set; }
        public string ProjectTypeName { get; set; }
    }
}
