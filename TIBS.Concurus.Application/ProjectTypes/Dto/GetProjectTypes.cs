﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.ProjectTypes.Dto
{
   public  class GetProjectTypes:IOutputDto
    {
       public CreateProjectTypeInput ProjectType { get; set; }
    }
}
