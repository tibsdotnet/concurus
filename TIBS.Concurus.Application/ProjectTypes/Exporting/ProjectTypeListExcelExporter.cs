﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectTypes.Dto;

namespace TIBS.Concurus.ProjectTypes.Exporting
{
    public class ProjectTypeListExcelExporter : EpPlusExcelExporterBase, IProjectTypeListExcelExporter
    {

        public FileDto ExportToFile(List<ProjectTypeView_dto> ProjectTypeListDtos)
        {
            return CreateExcelPackage(
                "ProjectType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ProjectType"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ProjectTypeName"),
                        L("ProjectTypeCode")


                    );

                    AddObjects(
                        sheet, 2, ProjectTypeListDtos,
                        _ => _.ProjectTypeName,
                        _ => _.ProjectTypeCode


                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
