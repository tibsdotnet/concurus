﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectTypes.Dto;

namespace TIBS.Concurus.ProjectTypes.Exporting
{
    public interface IProjectTypeListExcelExporter
    {
        FileDto ExportToFile(List<ProjectTypeView_dto> ProjectTypeListDtos);
    }
}
