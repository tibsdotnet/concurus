﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.ProjectTypes.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.AutoMapper;
using System.Data.Entity;
using Abp.Authorization;
using TIBS.Concurus.Authorization;
using TIBS.Concurus.ProjectTypes.Exporting;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.ProjectTypes
{
    public class ProjectTypeAppService:ConcurusAppServiceBase,IProjectTypeAppService
    {
        private readonly IRepository<ProjectType> _projectTypeRepository;
        public readonly IProjectTypeListExcelExporter _ProjectTypeListExcelExporter;
        public ProjectTypeAppService(IRepository<ProjectType> ProjectTypeRepository, IProjectTypeListExcelExporter ProjectTypeListExcelExporter)
        {
            _projectTypeRepository = ProjectTypeRepository;
            _ProjectTypeListExcelExporter = ProjectTypeListExcelExporter;
        }
        public ListResultOutput<ProjectTypeView_dto> GetProjectTypes(GetProjectTypeInput input)
        {
            var query = _projectTypeRepository.GetAll()
                     .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                     u => u.ProjectTypeCode.Contains(input.Filter) ||
                     u.ProjectTypeName.Contains(input.Filter)).OrderBy(p => p.ProjectTypeName)
               .ThenBy(p => p.ProjectTypeCode)
               .ToList();
            return new ListResultOutput<ProjectTypeView_dto>(query.MapTo<List<ProjectTypeView_dto>>());

        }
        public async Task<FileDto> GetrojectTypeToExcel(GetProjectTypeInput input)
        {
            var query =await _projectTypeRepository.GetAll()
                    .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                    u => u.ProjectTypeCode.Contains(input.Filter) ||
                    u.ProjectTypeName.Contains(input.Filter)).OrderBy(p => p.ProjectTypeName)
              .ThenBy(p => p.ProjectTypeCode)
              .ToListAsync();
            return _ProjectTypeListExcelExporter.ExportToFile(query.MapTo<List<ProjectTypeView_dto>>());
        }
        public async Task<GetProjectTypes> GetProjectForEdit(NullableIdInput<long> input)
        {
            var output = new GetProjectTypes { };
            var projectType = await _projectTypeRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            output.ProjectType = projectType.MapTo<CreateProjectTypeInput>();
            return output;
        }
        public async Task CreateOrUpdateProjectType(CreateProjectTypeInput input)
        {
            if(input.Id!=0)
            {
                await UpdateProjectType(input);

            }
            else
            {
                await CreateProjectType(input);

            }

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectType_CreateProjectType)]
        public async Task CreateProjectType(CreateProjectTypeInput input)
        {
            var projectType = input.MapTo<ProjectType>();
            await _projectTypeRepository.InsertAsync(projectType);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectType_EditProjectType)]
        public async Task UpdateProjectType(CreateProjectTypeInput input)
        {
            var projectType = input.MapTo<ProjectType>();
            await _projectTypeRepository.UpdateAsync(projectType);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectType_DeleteProjectType)]
        public async Task DeleteProjectType(IdInput input)
        {
            await _projectTypeRepository.DeleteAsync(input.Id);
        }
    }
}
