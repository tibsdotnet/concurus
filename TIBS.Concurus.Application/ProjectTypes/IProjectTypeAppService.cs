﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectTypes.Dto;

namespace TIBS.Concurus.ProjectTypes
{
    public interface IProjectTypeAppService:IApplicationService
    {
        ListResultOutput<ProjectTypeView_dto> GetProjectTypes(GetProjectTypeInput input);
        Task<GetProjectTypes> GetProjectForEdit(NullableIdInput<long> input);
        Task CreateOrUpdateProjectType(CreateProjectTypeInput input);
        Task DeleteProjectType(IdInput input);
        Task<FileDto> GetrojectTypeToExcel(GetProjectTypeInput input);
    }
}
