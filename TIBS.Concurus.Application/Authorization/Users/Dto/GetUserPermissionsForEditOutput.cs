﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TIBS.Concurus.Authorization.Dto;

namespace TIBS.Concurus.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput : IOutputDto
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}