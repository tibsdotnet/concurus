using System.Collections.Generic;
using TIBS.Concurus.Authorization.Users.Dto;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}