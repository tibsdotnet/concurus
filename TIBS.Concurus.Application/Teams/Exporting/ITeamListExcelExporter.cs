﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Teams.Dto;

namespace TIBS.Concurus.Teams.Exporting
{
    public interface ITeamListExcelExporter
    {
        FileDto ExportToFile(List<TeamListDto> TeamListDtos);
    }
}
