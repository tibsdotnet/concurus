﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Teams.Dto;

namespace TIBS.Concurus.Teams.Exporting
{
    public class TeamListExcelExporter : EpPlusExcelExporterBase, ITeamListExcelExporter
    {

        public FileDto ExportToFile(List<TeamListDto> TeamListDtos)
        {
            return CreateExcelPackage(
                "Team.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Country"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("TeamName"),
                        L("TeamCode")


                    );

                    AddObjects(
                        sheet, 2, TeamListDtos,
                        _ => _.TeamName,
                        _ => _.TeamCode


                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
