﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Teams.Dto
{
    public class EmployeeDto:IOutputDto
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public bool IsAssigned { get; set; }
    }
}
