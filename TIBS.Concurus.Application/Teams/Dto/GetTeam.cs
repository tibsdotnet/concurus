﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Teams.Dto
{
    public class GetTeam:IOutputDto
    {
        public EmployeeDto[] Employee { get; set; }
        public CreateTeamInput Team { get; set; }
    }
}
