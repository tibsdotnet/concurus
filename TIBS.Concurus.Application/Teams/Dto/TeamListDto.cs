﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.TeamMasterTable;

namespace TIBS.Concurus.Teams.Dto
{
    [AutoMapFrom(typeof(Team))]
   public class TeamListDto:FullAuditedEntityDto
    {
       public string TeamCode { get; set; }
       public string TeamName { get; set; }
    }
}
