﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.TeamMasterTable;

namespace TIBS.Concurus.Teams.Dto
{
    [AutoMapTo(typeof(Team))]
    public class CreateTeamInput:IInputDto
    {
        public string TeamCode { get; set; }
        public string TeamName { get; set; }
        public int Id { get; set; }
        public int[] EmployeeId { get; set; }
    }
}
