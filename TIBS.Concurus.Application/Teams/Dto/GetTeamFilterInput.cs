﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Teams.Dto
{
    public class GetTeamFilterInput:IInputDto
    {
        public string Filter { get; set; }
        public int Id { get; set; }
    }
}
