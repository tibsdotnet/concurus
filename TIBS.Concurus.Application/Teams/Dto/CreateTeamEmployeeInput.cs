﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.TeamEmploees;

namespace TIBS.Concurus.Teams.Dto
{
    [AutoMapTo(typeof(TeamEmployee))]
    public class CreateTeamEmployeeInput:IInputDto
    {
        public virtual int TeamId { get; set; }
        
        public virtual int EmployeeId { get; set; }
    }
}
