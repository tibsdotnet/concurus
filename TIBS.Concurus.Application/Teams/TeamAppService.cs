﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.TeamMasterTable;
using TIBS.Concurus.Teams.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.AutoMapper;
using TIBS.Concurus.TeamEmploees;
using TIBS.Concurus.Employees;
using System.Data.Entity;
using TIBS.Concurus.Employees.Dto;
using Abp.Authorization;
using TIBS.Concurus.Authorization;
using TIBS.Concurus.Teams.Exporting;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Teams
{
    public class TeamAppService:ConcurusAppServiceBase,ITeamAppService
    {
        private readonly IRepository<Team> _TeamRepository;
        private readonly IRepository<TeamEmployee> _TeamEmployeeRepository;
        public readonly IRepository<Employee> _EmployeeRepository;
        public readonly ITeamListExcelExporter _TeamListExcelExporter;
        public TeamAppService(IRepository<Team> TeamRepository, IRepository<TeamEmployee> TeamEmployeeRepository, IRepository<Employee> EmployeeRepository, ITeamListExcelExporter TeamListExcelExporter)
        {
            _TeamRepository = TeamRepository;
            _TeamEmployeeRepository = TeamEmployeeRepository;
            _EmployeeRepository = EmployeeRepository;
            _TeamListExcelExporter = TeamListExcelExporter;
        }
        public  ListResultOutput<TeamListDto> GetTeam(GetTeamFilterInput input)
        {
            var teamquery=_TeamRepository.GetAll()
                      .WhereIf(!input.Filter.IsNullOrEmpty(),
                    p => p.Id.ToString().Contains(input.Filter) ||
                        p.TeamCode.Contains(input.Filter) ||
                         p.TeamName.Contains(input.Filter)
                )
                .OrderBy(p => p.TeamName)
                .ThenBy(p => p.TeamCode)
                .ToList();

            return new ListResultOutput<TeamListDto>(teamquery.MapTo<List<TeamListDto>>());

        }
        public async Task<FileDto> GetTeamToExcel(GetTeamFilterInput input)
        {
            var query = await _TeamRepository.GetAll()
                      .WhereIf(!input.Filter.IsNullOrEmpty(),
                    p => p.Id.ToString().Contains(input.Filter) ||
                        p.TeamCode.Contains(input.Filter) ||
                         p.TeamName.Contains(input.Filter)
                )
                .OrderBy(p => p.TeamName)
                .ThenBy(p => p.TeamCode)
               .ToListAsync();
            return _TeamListExcelExporter.ExportToFile(query.MapTo<List<TeamListDto>>());
        }
        public async Task<GetTeam> GetTeamForEdit(NullableIdInput<long> input)
        {
            var employees =await _EmployeeRepository.GetAll().Select(p => new EmployeeDto { EmployeeId = p.Id, EmployeeName = p.FirstName }).ToArrayAsync();
            var team = await _TeamRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            foreach(var emp in employees)
            {
                var teamemployee =  _TeamEmployeeRepository.GetAll().Where(p => p.TeamId == input.Id&&p.EmployeeId==emp.EmployeeId).FirstOrDefault();
                if(teamemployee!=null)
                {
                    emp.IsAssigned = true;
                }

            }
            var output = new GetTeam { Employee = employees };
            output.Team = team.MapTo<CreateTeamInput>();
            return output;
           
        }
        public async Task CreateOrUpdateTeam(CreateTeamInput input)
        {
            if(input.Id!=0)
            {
                await UpdateTeam(input);
            }
            else
            {
                await CreateTeam(input);
            }

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Team_CreateTeam)]
        public async Task CreateTeam(CreateTeamInput input)
        {
            var team = input.MapTo<Team>();
            try
            {
                await _TeamRepository.InsertAsync(team);
            }catch(Exception ex)
            {

            }
            if(input.EmployeeId!=null)
            {
                foreach (var data in input.EmployeeId)
                {
                    CreateTeamEmployeeInput teamempInput = new CreateTeamEmployeeInput();
                    teamempInput.EmployeeId = data;
                    teamempInput.TeamId = (_TeamRepository.GetAll().Select(x => (int?)x.Id).Max() ?? 0);
                    var teamEmployee = teamempInput.MapTo<TeamEmployee>();
                    await _TeamEmployeeRepository.InsertAsync(teamEmployee);
                }

            }
           
           

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Team_EditTeam)]
        public async Task UpdateTeam(CreateTeamInput input)
        {
            var team = input.MapTo<Team>();
            await _TeamRepository.UpdateAsync(team);
            foreach(var data in input.EmployeeId)
            {
                var query = _TeamEmployeeRepository.GetAll().Where(p => p.EmployeeId == data && p.TeamId==input.Id).FirstOrDefault();
                if(query==null)
                {
                    CreateTeamEmployeeInput teamempInput = new CreateTeamEmployeeInput();
                    teamempInput.EmployeeId = data;
                    teamempInput.TeamId=input.Id;
                    var teamEmployee=teamempInput.MapTo<TeamEmployee>();
                    await _TeamEmployeeRepository.InsertAsync(teamEmployee);


                }
            }

        }
        public ListResultOutput<EmployeeViewDto> GetTeamMembers(GetTeamFilterInput input)
        {
            var teamempquery = _TeamEmployeeRepository.GetAll().Where(p => p.TeamId == input.Id).ToList();
            var query = from c in teamempquery join emp in _EmployeeRepository.GetAll() on c.EmployeeId equals emp.Id select new { emp, c };
            var Employeedto = (from c in query select new EmployeeViewDto { FirstName = c.emp.FirstName, LocationName = c.emp.LocationId != null ? c.emp.Locations.LocationName : "", PhoneNo = c.emp.PhoneNo, Id = c.c.Id, PhotoPath = c.emp.PhotoPath != null ? c.emp.PhotoPath : "/Common/Images/default-profile-picture.png",UserId=c.emp.Id });
            var employeedtos = Employeedto.MapTo<List<EmployeeViewDto>>();
            return new ListResultOutput<EmployeeViewDto>(employeedtos);

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Team_DeleteTeam)]
        public async Task DeleteTeamEmployee(IdInput input)
        {
            await _TeamEmployeeRepository.DeleteAsync(input.Id);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Team_DeleteTeam)]
        public async Task DeleteTeam(IdInput input)
        {
            await _TeamRepository.DeleteAsync(input.Id);
        }
    }
    
}
