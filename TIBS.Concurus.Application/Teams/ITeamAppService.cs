﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Employees.Dto;
using TIBS.Concurus.Teams.Dto;

namespace TIBS.Concurus.Teams
{
    public interface ITeamAppService:IApplicationService
    {
        ListResultOutput<TeamListDto> GetTeam(GetTeamFilterInput input);
        Task<GetTeam> GetTeamForEdit(NullableIdInput<long> input);
        Task CreateOrUpdateTeam(CreateTeamInput input);
        Task DeleteTeam(IdInput input);
        ListResultOutput<EmployeeViewDto> GetTeamMembers(GetTeamFilterInput input);
        Task DeleteTeamEmployee(IdInput input);
        Task<FileDto> GetTeamToExcel(GetTeamFilterInput input);
    }
}
