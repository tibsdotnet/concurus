﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.Localization.Dto
{
    public class CreateOrUpdateLanguageInput : IInputDto
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}