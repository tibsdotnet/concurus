﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Companies.Dto;
using Abp.Application.Services.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.Linq;
using System.Data.Entity;
using Abp.AutoMapper;
using System.Linq.Dynamic;
using TIBS.Concurus.Contacts;
using TIBS.Concurus.ContactTypes;
using TIBS.Concurus.Companies.Exporting;
using TIBS.Concurus.Dto;
namespace TIBS.Concurus.Companies
{
    public class CompanyAppService:ConcurusAppServiceBase,ICompanyAppService
    {
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<Contact> _ContactRepository;
        private readonly IRepository<ContactType> _ContTypeRepository;
        private readonly ICompanyListExcelExporter _CompanyListExcelExporter;
        private readonly IContactListExcelExporter _ContactListExcelExporter;
        public CompanyAppService(IRepository<ContactType> ContTypeRepository, IRepository<Company> companyRepository, IRepository<Contact> ContactRepository, ICompanyListExcelExporter CompanyListExcelExporter,
             IContactListExcelExporter ContactListExcelExporter)
        {
            _companyRepository = companyRepository;
            _ContactRepository = ContactRepository;
            _ContTypeRepository = ContTypeRepository;
            _CompanyListExcelExporter = CompanyListExcelExporter;
            _ContactListExcelExporter = ContactListExcelExporter;
        }
        public async Task<PagedResultOutput<CompanyView_Dto>> GetCompanies(GetCompanyInput input)
        {
            var query = _companyRepository.GetAll()
                       .WhereIf(
                       !input.Filter.IsNullOrWhiteSpace(),
                       u => u.Name.Contains(input.Filter) ||
                       u.Address.Contains(input.Filter) ||
                       u.MailId.Contains(input.Filter) ||
                       u.PhoneNumber.Contains(input.Filter) ||
                       u.ContactTypes.TypeName.Contains(input.Filter) ||
                       u.Website.Contains(input.Filter));
            var company = await query
             .OrderBy(input.Sorting)
             .PageBy(input)
             .ToListAsync();
            var companycount = await query.CountAsync();
            var comapnydto = from c in company select new CompanyView_Dto { Id = c.Id, Name = c.Name, Address = c.Address, MailId = c.MailId, Website = c.Website, PhoneNumber = c.PhoneNumber, TypeName =c.ContactTypeId!=null?c.ContactTypes.TypeName:"" };
            var companydtos = comapnydto.MapTo<List<CompanyView_Dto>>();
          


            return new PagedResultOutput<CompanyView_Dto>(companycount,companydtos);

        }
        public async Task<FileDto> GetCompanyToExcel(GetCompanyInput input)
        {

            var query = _companyRepository.GetAll()
                       .WhereIf(
                       !input.Filter.IsNullOrWhiteSpace(),
                       u => u.Name.Contains(input.Filter) ||
                       u.Address.Contains(input.Filter) ||
                       u.MailId.Contains(input.Filter) ||
                       u.PhoneNumber.Contains(input.Filter) ||
                       u.ContactTypes.TypeName.Contains(input.Filter) ||
                       u.Website.Contains(input.Filter));
            var company = await query
             .OrderBy(input.Sorting)
             .PageBy(input)
             .ToListAsync();
            var companycount = await query.CountAsync();
            var comapnydto = from c in company select new CompanyView_Dto { Id = c.Id, Name = c.Name, Address = c.Address, MailId = c.MailId, Website = c.Website, PhoneNumber = c.PhoneNumber, TypeName = c.ContactTypeId != null ? c.ContactTypes.TypeName : "" };
            var companydtos = comapnydto.MapTo<List<CompanyView_Dto>>();


            return _CompanyListExcelExporter.ExportToFile(companydtos);
        }
        public async Task<GetCompanies> GetCompanyForEdit(NullableIdInput<long> input)
        {
            var typeDtos = (await _ContTypeRepository.GetAll()
                .OrderBy(r => r.TypeName)
                .Select(r => new ContTypeList
                {
                    ContactTypeId = r.Id,
                    TypeName = r.TypeName

                }).ToArrayAsync());
            
            var comapanies = await _companyRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetCompanies { TypeName = typeDtos };
            output.Company = comapanies.MapTo<CreateCompanyInput>();
            return output;
        }
        public async Task CreateOrUpdateCompany(CreateCompanyInput input)
        {
            if(input.Id!=0)
            {
                await UpdateCompany(input);

            }
            else
            {
                await CreateCompany(input);
            }

        }
        public async Task CreateCompany(CreateCompanyInput input)
        {
            var company = input.MapTo<Company>();
            await _companyRepository.InsertAsync(company);

        }
        public async Task UpdateCompany(CreateCompanyInput input)
        {
            var company = input.MapTo<Company>();
            await _companyRepository.UpdateAsync(company);
        }
        public async Task DeleteCompany(IdInput input)
        {
            await _companyRepository.DeleteAsync(input.Id);
        }
        public async Task<PagedResultOutput<ContactView_Dto>> GetCompanyContacts(GetContactInput input)
        {
            var query = _ContactRepository.GetAll().Where(p => p.CompanyId == input.CompanyId)
                        .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                        u => u.ContactName.Contains(input.Filter) ||
                        u.PhoneNumber.Contains(input.Filter) ||
                        u.MailId.Contains(input.Filter) ||
                        u.companies.Name.Contains(input.Filter));
            try
            {
                var contactss = await query
             .OrderBy(input.Sorting)
             .PageBy(input)
             .ToListAsync();

            }catch(Exception ex)
            {

            }
            var contacts = await query
              .OrderBy(input.Sorting)
              .PageBy(input)
              .ToListAsync();
            var contactcount = await query.CountAsync();
            var contact = from c in contacts select new ContactView_Dto { Id=c.Id,Name = c.ContactName, MailId = c.MailId, PhoneNumber = c.PhoneNumber, Address = c.Address };
            var contactdto = contact.MapTo<List<ContactView_Dto>>();

            return new PagedResultOutput<ContactView_Dto>(contactcount,contactdto);
        }
        public async Task<FileDto> GetContactToExcel(GetContactInput input)
        {
            var query = _ContactRepository.GetAll().Where(p => p.CompanyId == input.CompanyId)
                        .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                        u => u.ContactName.Contains(input.Filter) ||
                        u.PhoneNumber.Contains(input.Filter) ||
                        u.MailId.Contains(input.Filter) ||
                        u.companies.Name.Contains(input.Filter));
            try
            {
                var contactss = await query
             .OrderBy(input.Sorting)
             .PageBy(input)
             .ToListAsync();

            }
            catch (Exception ex)
            {

            }
            var contacts = await query
              .OrderBy(input.Sorting)
              .PageBy(input)
              .ToListAsync();
            var contactcount = await query.CountAsync();
            var contact = from c in contacts select new ContactView_Dto { Id = c.Id, Name = c.ContactName, MailId = c.MailId, PhoneNumber = c.PhoneNumber, Address = c.Address };
            var contactdto = contact.MapTo<List<ContactView_Dto>>();
            return _ContactListExcelExporter.ExportToFile(contactdto);
        }
        
        public async Task<GetContacts> GetContactForEdit(NullableIdInput<long> input)
        {
            var contact =await _ContactRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetContacts { };
            output.Contact = contact.MapTo<CreateContactInput>();
            return output;

        }
        public async Task CreateOrUpdateContact(CreateContactInput input)
        {
            if(input.Id!=0)
            {
                await UpdateContact(input);
            }
            else
            {
                await CreateContact(input);
            }
        }
        public async Task CreateContact(CreateContactInput input)
        {
            var contacts = input.MapTo<Contact>();
            await _ContactRepository.InsertAsync(contacts);
        }
        public async Task UpdateContact(CreateContactInput input)
        {
            var contacts = input.MapTo<Contact>();
            await _ContactRepository.UpdateAsync(contacts);
        }
        public async Task DeleteConact(IdInput input)
        {
            await _ContactRepository.DeleteAsync(input.Id);
        }
    }
}
