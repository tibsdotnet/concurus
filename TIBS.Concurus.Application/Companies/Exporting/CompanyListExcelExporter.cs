﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Companies.Dto;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Companies.Exporting
{
    public class CompanyListExcelExporter : EpPlusExcelExporterBase, ICompanyListExcelExporter
    {

        public FileDto ExportToFile(List<CompanyView_Dto> companyListDtos)
        {
            return CreateExcelPackage(
                "Company.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Country"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CompanyName"),
                        L("ContactType"),
                        L("PhoneNo"),
                        L("Email"),
                        L("WebSite"),
                        L("Address")
                        

                    );

                    AddObjects(
                        sheet, 2, companyListDtos,
                        _ => _.Name,
                        _ => _.TypeName,
                        _ => _.PhoneNumber,
                        _ => _.MailId,
                        _ => _.Website,
                        _ => _.Address
                        //_ => _.LastModificationTime,
                        //_ => _.LastModifierUserId,
                        //_ => _.CreationTime,
                        //_ => _.CreatorUserId

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
