﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Companies.Dto;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Companies.Exporting
{
    public class ContactListExcelExporter : EpPlusExcelExporterBase, IContactListExcelExporter
    {

        public FileDto ExportToFile(List<ContactView_Dto> companyListDtos)
        {
            return CreateExcelPackage(
                "contact.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Country"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("PhoneNo"),
                        L("Email"),
                        L("Address")


                    );

                    AddObjects(
                        sheet, 2, companyListDtos,
                        _ => _.Name,
                        _ => _.PhoneNumber,
                        _ => _.MailId,
                        _ => _.Address
                       

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
