﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Companies.Dto;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Companies.Exporting
{
    public interface IContactListExcelExporter
    {
        FileDto ExportToFile(List<ContactView_Dto> companyListDtos);
    }
}
