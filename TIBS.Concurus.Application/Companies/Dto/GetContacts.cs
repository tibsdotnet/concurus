﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Companies.Dto
{
    public class GetContacts:IOutputDto
    {
        public CreateContactInput Contact { get; set; }
    }
}
