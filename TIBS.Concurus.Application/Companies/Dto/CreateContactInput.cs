﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.Contacts;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.Companies.Dto
{
    [AutoMapTo(typeof(Contact))]
    public class CreateContactInput:IInputDto
    {
        public int Id { get; set; }
        public  string ContactName { get; set; }
        public  string PhoneNumber { get; set; }
        public  string MailId { get; set; }
        public  string Address { get; set; }
        
        public  int CompanyId { get; set; }

    }
}
