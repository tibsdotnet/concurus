﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Companies.Dto
{
    public class GetCompanies:IOutputDto
    {
        public CreateCompanyInput Company { get; set; }
        public ContTypeList[] TypeName { get; set; }
    }
}
