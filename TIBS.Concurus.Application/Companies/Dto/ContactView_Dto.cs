﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.Contacts;

namespace TIBS.Concurus.Companies.Dto
{
    [AutoMapFrom(typeof(Contact))]
   public class ContactView_Dto:FullAuditedEntityDto
    {
        public virtual string Name { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string MailId { get; set; }
        public virtual string Address { get; set; }
    }
}
