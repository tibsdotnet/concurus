﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.Companies.Dto
{
    [AutoMapFrom(typeof(Company))]
    public class CompanyView_Dto:FullAuditedEntityDto
    {
        public  string Name { get; set; }
        public  string Address { get; set; }
        public  string PhoneNumber { get; set; }
        public  string MailId { get; set; }
        public  string Website { get; set; }
        public int ContactTypeId { get; set; }
        public string TypeName { get; set; }
    }
}
