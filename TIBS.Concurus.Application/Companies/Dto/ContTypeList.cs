﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Companies.Dto
{
    public class ContTypeList : IDto
    {
        public int ContactTypeId { get; set; }
        public string TypeName { get; set; }
    }
}
