﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TIBS.Concurus.Companies.Dto
{
    [AutoMapTo(typeof(Company))]
    public class CreateCompanyInput:IInputDto
    {
        public int Id { get; set; }
        [Required]
        public  string Name { get; set; }
        public  string Address { get; set; }
        public  string PhoneNumber { get; set; }
        public  string MailId { get; set; }
        public  string Website { get; set; }
        public int? ContactTypeId { get; set; }
    }
}
