﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Companies.Dto;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Companies
{
    public interface ICompanyAppService:IApplicationService
    {
        Task<PagedResultOutput<CompanyView_Dto>> GetCompanies(GetCompanyInput input);
        Task<GetCompanies> GetCompanyForEdit(NullableIdInput<long> input);
        Task CreateOrUpdateCompany(CreateCompanyInput input);
        Task DeleteCompany(IdInput input);
        Task<PagedResultOutput<ContactView_Dto>> GetCompanyContacts(GetContactInput input);
        Task<GetContacts> GetContactForEdit(NullableIdInput<long> input);
        Task CreateOrUpdateContact(CreateContactInput input);
        Task DeleteConact(IdInput input);
        Task<FileDto> GetCompanyToExcel(GetCompanyInput input);
        Task<FileDto> GetContactToExcel(GetContactInput input);
    }
}
