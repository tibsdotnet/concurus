﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.ProductBacklogs;
using Abp.Application.Services.Dto;
using Abp.Linq.Extensions;

namespace TIBS.Concurus.Projects.Dto
{
    [AutoMapFrom(typeof(ProductBacklog))]
    public class ProjectBaclogView_Dto:FullAuditedEntityDto
    {
        public  string ProjectName { get; set; }
       
        public  string BacklogType { get; set; }
        
        public  string BacklogPriority { get; set; }
        public  string Sprint { get; set; }
        
        public  string BacklogStatus { get; set; }
        public  string Where { get; set; }

        
        public  string Who { get; set; }
        public  string What { get; set; }
        public  string Why { get; set; }
        public  string When { get; set; }
        public  string Notes { get; set; }
        public  string TotalHousrs { get; set; }
       
        public  string RequestFrom { get; set; }
        public  string RequestDate { get; set; }
        
        public  string ApprovedBy { get; set; }
        public  string ApprovedDate { get; set; }
        public string ProjectBacklogName { get; set; }
        public bool EnableEdit { get; set; }
    }
}
