﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
    public class ProjectTypeOutputDto
    {
        public string ProjectTypeName { get; set; }
        public int ProjectTypeId { get; set; }
    }
}
