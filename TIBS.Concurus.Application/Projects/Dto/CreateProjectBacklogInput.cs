﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.ProductBacklogs;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.Projects.Dto
{
    [AutoMapTo(typeof(ProductBacklog))]
    public class CreateProjectBacklogInput:IInputDto
    {
        public int Id { get; set; }
        
        public  int ProjectId { get; set; }
        
        public  int? BacklogTypeId { get; set; }
        
        public  int? BacklogPriorityId { get; set; }
        public  string Sprint { get; set; }
       
        public  int? BacklogStatusId { get; set; }
        public  string Where { get; set; }

        
        public  long? WhoId { get; set; }
        public  string What { get; set; }
        public  string Why { get; set; }
        public  string When { get; set; }
        public  string Notes { get; set; }
        public  string TotalHousrs { get; set; }
        
        public  long? RequestFrom { get; set; }
        public  DateTime? RequestDate { get; set; }
        
        public  long? ApprovedBy { get; set; }
        public  DateTime? ApprovedDate { get; set; }
        public  string ProjectBacklogName { get; set; }

    }
}
