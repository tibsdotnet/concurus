﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.ProductTasks;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TIBS.Concurus.Projects.Dto
{
    [AutoMapTo(typeof(ProductTask))]
    public class CreateProjectTaskInput:IInputDto
    {
        public int Id { get; set; }
        public  int? ProductBacklogId { get; set; }
        [Required]
        public  string TaskName { get; set; }
      
        public  long? AssignedUserId { get; set; }
        public  decimal EstimateHours { get; set; }
        public  decimal AllotedHours { get; set; }
        public  decimal ActualTime { get; set; }
        public  DateTime? StartDate { get; set; }
        public  DateTime? EndDate { get; set; }
        public  decimal RemainingTime { get; set; }
       
        public  int? StatusId { get; set; }
        public  string Remarks { get; set; }
    }
}
