﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
    public class GetProjectTasks:IOutputDto
    {
        public CreateProjectTaskInput ProjectTask { get; set; }
        public Userdto[] User { get; set; }
        public ProjectBacklogDto[] ProjectBacklog { get; set; }
        public BacklogStatusDto[] BacklogStatus { get; set; }
    }
}
