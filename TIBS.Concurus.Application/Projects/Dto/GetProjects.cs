﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
    public class GetProjects:IOutputDto
    {
        public CreateProjectInput Project { get; set; }
        public TechnologyOutputdto[] Technology { get; set; }
        public TeamOutputdto[] Team { get; set; }
        public CompanyOutputDto[] Company { get; set; }
        public ProjectTypeOutputDto[] ProjectType { get; set; }
    }
}
