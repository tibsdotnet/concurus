﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.ProductTasks;
using Abp.Domain.Entities.Auditing;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.Projects.Dto
{
    [AutoMapFrom(typeof(ProductTask))]
    public class ProjectTaskView_dto:FullAuditedEntityDto
    {
        public virtual string ProductBacklog { get; set; }
        
        public virtual string TaskName { get; set; }
       
        public virtual string AssignedUser { get; set; }
        public virtual decimal EstimateHours { get; set; }
        public virtual decimal AllotedHours { get; set; }
        public virtual decimal ActualTime { get; set; }
        public virtual string  StartDate { get; set; }
        public virtual string EndDate { get; set; }
        public virtual decimal RemainingTime { get; set; }
        
        public virtual string Status { get; set; }
        public virtual string Remarks { get; set; }
        public bool EnableEdit { get; set; }
    
    }
}
