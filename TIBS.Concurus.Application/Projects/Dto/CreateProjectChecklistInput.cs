﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.ProjectCheckLists;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.Projects.Dto
{
    [AutoMapTo(typeof(ProjectCheckList))]
    public class CreateProjectChecklistInput:IInputDto
    {
        public int Id { get; set; }
        public  string Factor { get; set; }
        public  string Description { get; set; }
        
        public  int ProjectId { get; set; }
        public  long PostedId { get; set; }
        public bool ProjectImportant { get; set; }
    }
}
