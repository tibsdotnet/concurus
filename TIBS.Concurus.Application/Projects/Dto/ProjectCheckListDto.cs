﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.ProjectCheckLists;

namespace TIBS.Concurus.Projects.Dto
{
    [AutoMapFrom(typeof(ProjectCheckList))]
    public class ProjectCheckListDto:FullAuditedEntityDto
    {
        public string Factor { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
        public Guid? PhotoPath { get; set; }
        public string Date { get; set; }
        public long? UserId { get; set; }
        public string PostedTime { get; set; }
        public bool? ProjectImportant { get; set; }
    }
}
