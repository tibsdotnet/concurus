﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
    public class TeamOutputdto
    {
        public string TeamName { get; set; }
        public int TeamId { get; set; }
    }
}
