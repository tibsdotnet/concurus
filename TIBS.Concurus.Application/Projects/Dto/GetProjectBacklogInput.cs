﻿using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Projects.Dto
{
    public class GetProjectBacklogInput : PagedAndSortedInputDto, IShouldNormalize
    {
        
        public string Filter { get; set; }
        public int ProjectId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "ProjectBacklogName,ProjectName,RequestFrom,RequestDate,Sprint,TotalHousrs,What,When,Where,Who,Why,BacklogType,BacklogStatus,BacklogPriority,ApprovedBy";
            }
        }
    }
}
