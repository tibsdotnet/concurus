﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
    public class GetProjectCheckLists:IOutputDto
    {
        public CreateProjectChecklistInput ProjectCheckList { get; set; }
    }
}
