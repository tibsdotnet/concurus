﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.Concurus.ProjectImportants;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.Projects.Dto
{
    [AutoMapTo(typeof(ProjectImportant))]
    public class CreateProjectImportantInput:IInputDto
    {
        public  int? ProjectChecklistId { get; set; }
        
        public  long ImportantMakeUserId { get; set; }
        
        public  int? ProjectId { get; set; }
    }
}
