﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
    public class GetProjectBacklogs:IOutputDto
    {
        public CreateProjectBacklogInput ProjectBacklog { get; set; }
        public ProjectDto[] Project { get; set; }
        public BacklogStatusDto[] BacklogStatus { get; set; }
        public BacklogPriorityDto[] BacklogProirity { get; set; }
        public BacklogTypeDto[] BacklogType { get; set; }
        public Userdto[] Users { get; set; }
    }
}
