﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;
namespace TIBS.Concurus.Projects.Dto
{
    [AutoMapTo(typeof(Project))]
    public class CreateProjectInput:IInputDto
    {
        public int Id { get; set; }
        public  string ProjectName { get; set; }
        public  string Description { get; set; }
        
        public  int? TechnologyId { get; set; }
        
        public  int? TeamId { get; set; }
       
        public  int? ProjectTypeId { get; set; }
        
        public  int? CompanyId { get; set; }
    }
}
