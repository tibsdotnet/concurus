﻿using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;

namespace TIBS.Concurus.Projects.Dto
{
    public class GetProjectInput : PagedAndSortedInputDto, IShouldNormalize
    {
       
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "ProjectName";
            }
        }
    }
}
