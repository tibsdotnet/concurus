﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
    public class BacklogStatusDto
    {
        public string BacklogStatusName { get; set; }
        public int BacklogStatusId { get; set; }
    }
    public class BacklogTypeDto
    {
        public string BacklogTypeName { get; set; }
        public int BacklogTypeId { get; set; }
        
    }
    public class BacklogPriorityDto
    {
        public string BacklogPriorityName { get; set; }
        public int BacklogPriorityId { get; set; }
    }
}
