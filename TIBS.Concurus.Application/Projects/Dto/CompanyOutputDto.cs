﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
   public class CompanyOutputDto
    {
       public string CompanyName { get; set; }
       public int CompanyId { get; set; }
    }
}
