﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
    public class ProjectBacklogDto
    {
        public string ProjectBacklogName { get; set; }
        public int ProductBacklogId { get; set; }
    }
}
