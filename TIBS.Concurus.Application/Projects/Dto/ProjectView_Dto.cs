﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;

namespace TIBS.Concurus.Projects.Dto
{
    [AutoMapFrom(typeof(Project))]
    public class ProjectView_Dto:FullAuditedEntityDto
    {
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string Team { get; set; }
        public string Technology { get; set; }
        public string Company { get; set; }
        public string ProjectType { get; set; }
        public ProjectData[] ProjectEmployeePhotoPath { get; set; }
    }
}
