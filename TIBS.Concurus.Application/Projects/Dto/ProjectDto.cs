﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
    public class ProjectDto
    {
        public string ProjectName { get; set; }
        public int ProjectId { get; set; }
    }
}
