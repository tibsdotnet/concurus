﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.Concurus.Projects.Dto
{
   public class TechnologyOutputdto
    {
       public int TechnologyId { get; set; }
       public string TechnologyName { get; set; }
    }
}
