﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Projects.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using System.Linq.Dynamic;
using Abp.AutoMapper;
using TIBS.Concurus.Companies;
using TIBS.Concurus.ProjectTypes;
using TIBS.Concurus.Technologies;
using TIBS.Concurus.TeamMasterTable;
using TIBS.Concurus.ProjectTeams;
using TIBS.Concurus.Employees;
using TIBS.Concurus.ProductBacklogs;
using TIBS.Concurus.BackLogTypes;
using TIBS.Concurus.BackLogPrioritys;
using TIBS.Concurus.BackLogStatuss;
using TIBS.Concurus.ProductTasks;
using Abp.Runtime.Session;
using TIBS.Concurus.Authorization.Roles;
using Abp.Authorization;
using TIBS.Concurus.Authorization;
using TIBS.Concurus.Projects.Exporting;
using TIBS.Concurus.Dto;
using TIBS.Concurus.ProjectCheckLists;
using TIBS.Concurus.ProjectImportants;

namespace TIBS.Concurus.Projects
{
    public class ProjectAppService : ConcurusAppServiceBase, IProjectAppService
    {
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<ProjectType> _projectTypeRepository;
        private readonly IRepository<Technology> _TechnologyRepository;
        private readonly IRepository<Team> _TeamRepository;
        private readonly IRepository<ProjectTeam> _ProjectTeamRepository;
        private readonly IRepository<Employee> _EmployeeRepository;
        private readonly IRepository<ProductBacklog> _ProductBacklogRepository;
        private readonly IRepository<BackLogType> _BackLogTypeRepository;
        private readonly IRepository<BackLogPriority> _BackLogPriorityRepository;
        private readonly IRepository<BackLogStatus> _BackLogStatusRepository;
        private readonly IRepository<ProductTask> _projectTaskRepository;
        private readonly RoleManager _roleManager;
        private readonly IProjectListExcelExporter _ProjectListExcelExporter;
        private readonly IProjectBacklogListExcelExporter _ProjectBacklogListExcelExporter;
        private readonly IProjectTaskListExcelExporter _ProjectTaskListExcelExporter;
        private readonly IRepository<ProjectCheckList> _ProjectCheckListRepository;
        private readonly IRepository<ProjectImportant> _ProjectImportantRepository;
        public ProjectAppService(IRepository<Project> ProjectRepository, IRepository<Company> companyRepository, IRepository<ProjectType> projectTypeRepository, IRepository<Technology> TechnologyRepository, IRepository<Team> TeamRepository, IRepository<ProjectTeam> ProjectTeamRepository, IRepository<Employee> EmployeeRepository, IRepository<ProductBacklog> ProductBacklogRepository,
            IRepository<BackLogType> BackLogTypeRepository, IRepository<BackLogPriority> BackLogPriorityRepository, IRepository<BackLogStatus> BackLogStatusRepository, IRepository<ProductTask> projectTaskRepository, RoleManager roleManager, IProjectListExcelExporter ProjectListExcelExporter, IProjectBacklogListExcelExporter ProjectBacklogListExcelExporter,
            IProjectTaskListExcelExporter ProjectTaskListExcelExporter, IRepository<ProjectCheckList> ProjectCheckListRepository, IRepository<ProjectImportant> ProjectImportantRepository)
        {
            _projectRepository = ProjectRepository;
            _companyRepository = companyRepository;
            _projectTypeRepository = projectTypeRepository;
            _TeamRepository = TeamRepository;
            _TechnologyRepository = TechnologyRepository;
            _ProjectTeamRepository = ProjectTeamRepository;
            _EmployeeRepository = EmployeeRepository;
            _ProductBacklogRepository = ProductBacklogRepository;
            _BackLogStatusRepository = BackLogStatusRepository;
            _BackLogTypeRepository = BackLogTypeRepository;
            _BackLogPriorityRepository = BackLogPriorityRepository;
            _projectTaskRepository = projectTaskRepository;
            _roleManager = roleManager;
            _ProjectListExcelExporter = ProjectListExcelExporter;
            _ProjectBacklogListExcelExporter = ProjectBacklogListExcelExporter;
            _ProjectTaskListExcelExporter = ProjectTaskListExcelExporter;
            _ProjectCheckListRepository = ProjectCheckListRepository;
            _ProjectImportantRepository = ProjectImportantRepository;
            
        }
        public async Task<ListResultOutput<ProjectView_Dto>> GetProjects(GetProjectInput input)
        {
            long userid = (long)AbpSession.GetUserId();

            var user = await UserManager.GetUserByIdAsync(userid);
            var roleid = user.Roles.ToList();

            string[] userroles = new string[roleid.Count];
            int i = 0;
            foreach (var role in roleid)
            {
                int rol = role.RoleId;
                var userrolesquery = _roleManager.Roles.Where(p => p.Id == rol).OrderBy(p => p.DisplayName).FirstOrDefault();
                userroles[i] = userrolesquery.DisplayName;
                i++;
            }
            var uroles = userroles.ToList();
            var query = _projectRepository.GetAll().Where(p => p.Id == 0);
             if (uroles.Contains("Admin"))
             {
                  query = _projectRepository.GetAll()
                      .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                      u => u.ProjectName.Contains(input.Filter) ||
                      u.Description.Contains(input.Filter) ||
                      u.Teams.TeamName.Contains(input.Filter) ||
                      u.Technologies.TechnologyName.Contains(input.Filter) ||
                      u.ProjectTypes.ProjectTypeName.Contains(input.Filter) ||
                      u.Companies.Name.Contains(input.Filter));

             }
             else
             {
                 query=(from p in _projectRepository.GetAll() join t  in _ProjectTeamRepository.GetAll() on p.Id equals t.ProjectId where t.EmployeeId==userid select p) 
                      .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                      u => u.ProjectName.Contains(input.Filter) ||
                      u.Description.Contains(input.Filter) ||
                      u.Teams.TeamName.Contains(input.Filter) ||
                      u.Technologies.TechnologyName.Contains(input.Filter) ||
                      u.ProjectTypes.ProjectTypeName.Contains(input.Filter) ||
                      u.Companies.Name.Contains(input.Filter));

             }
           
            var project = await query
             .OrderBy(input.Sorting)
             .PageBy(input)
             .ToListAsync();
            var projectcount = await query.CountAsync();
            var projectdto = (from c in project select new ProjectView_Dto { ProjectName = c.ProjectName, Description = c.Description, Team = c.TeamId != null ? c.Teams.TeamName : "", Technology = c.TechnologyId != null ? c.Technologies.TechnologyName : "", Company = c.CompanyId != null ? c.Companies.Name : "", ProjectType = c.ProjectTypeId != null ? c.ProjectTypes.ProjectTypeName : "", Id = c.Id }).ToList();
            var projectdtos = projectdto.MapTo<List<ProjectView_Dto>>().ToList();
            List<ProjectData> projectData = new List<ProjectData>();
            foreach (var pro in projectdtos)
            {
                //string projectdata = "<h4 class=list-group-item-heading>" + pro.ProjectName.ToUpper() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                //projectdata += " <span class=person-buttons><button style='float:right;margin-top: 12px'  data-id='" + pro.Id + "' title='Edit' class='btn btn-circle btn-icon-only white projectbtn'><i class=icon-pencil></i></button> </span></h4>";
                //string projectdata="";
                var pject = (from c in _ProjectTeamRepository.GetAll().Where(p => p.ProjectId == pro.Id) select c).ToList();
                foreach (var p in pject)
                {
                    try
                    {

                        var emp = _EmployeeRepository.GetAll().Where(e => e.UserId == p.EmployeeId).FirstOrDefault();
                        string path = emp.PhotoPath != null ? emp.PhotoPath : "/Common/Images/default-profile-picture.png";
                        //projectdata +=  path;
                        projectData.Add(new ProjectData { Data = path, UserName = emp.FirstName });


                    }
                    catch (Exception ex)
                    {

                    }


                }
                //projectData.ToArray();
                pro.ProjectEmployeePhotoPath = projectData.ToArray();
                projectData = new List<ProjectData>();

                //if(pject.Count()>0)
                //{
                //    projectdata += "</p>";
                //}

                //  projectData.Add(new ProjectData { ProjectId = pro.Id, Data = projectdata });
            }
            return new ListResultOutput<ProjectView_Dto>(projectdtos);

        }
        public async Task<FileDto> GetProjectToExcel(GetProjectInput input)
        {
            long userid = (long)AbpSession.GetUserId();

            var user = await UserManager.GetUserByIdAsync(userid);
            var roleid = user.Roles.ToList();

            string[] userroles = new string[roleid.Count];
            int i = 0;
            foreach (var role in roleid)
            {
                int rol = role.RoleId;
                var userrolesquery = _roleManager.Roles.Where(p => p.Id == rol).OrderBy(p => p.DisplayName).FirstOrDefault();
                userroles[i] = userrolesquery.DisplayName;
                i++;
            }
            var uroles = userroles.ToList();
            var query = _projectRepository.GetAll().Where(p => p.Id == 0);
            if (uroles.Contains("Admin"))
            {
                query = _projectRepository.GetAll()
                    .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                    u => u.ProjectName.Contains(input.Filter) ||
                    u.Description.Contains(input.Filter) ||
                    u.Teams.TeamName.Contains(input.Filter) ||
                    u.Technologies.TechnologyName.Contains(input.Filter) ||
                    u.ProjectTypes.ProjectTypeName.Contains(input.Filter) ||
                    u.Companies.Name.Contains(input.Filter));

            }
            else
            {
                query = (from p in _projectRepository.GetAll() join t in _ProjectTeamRepository.GetAll() on p.Id equals t.ProjectId where t.EmployeeId == userid select p)
                     .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                     u => u.ProjectName.Contains(input.Filter) ||
                     u.Description.Contains(input.Filter) ||
                     u.Teams.TeamName.Contains(input.Filter) ||
                     u.Technologies.TechnologyName.Contains(input.Filter) ||
                     u.ProjectTypes.ProjectTypeName.Contains(input.Filter) ||
                     u.Companies.Name.Contains(input.Filter));

            }

            var project = await query
             .OrderBy(input.Sorting)
             .PageBy(input)
             .ToListAsync();
            var projectcount = await query.CountAsync();
            var projectdto = (from c in project select new ProjectView_Dto { ProjectName = c.ProjectName, Description = c.Description, Team = c.TeamId != null ? c.Teams.TeamName : "", Technology = c.TechnologyId != null ? c.Technologies.TechnologyName : "", Company = c.CompanyId != null ? c.Companies.Name : "", ProjectType = c.ProjectTypeId != null ? c.ProjectTypes.ProjectTypeName : "", Id = c.Id }).ToList();
            var projectdtos = projectdto.MapTo<List<ProjectView_Dto>>().ToList();


            return _ProjectListExcelExporter.ExportToFile(projectdtos);
        }
        public async Task<GetProjects> GetProjectForEdit(NullableIdInput<long> input)
        {
            var team = await _TeamRepository.GetAll().Select(p => new TeamOutputdto { TeamId = p.Id, TeamName = p.TeamName }).ToArrayAsync();
            var technology = await _TechnologyRepository.GetAll().Select(p => new TechnologyOutputdto { TechnologyId = p.Id, TechnologyName = p.TechnologyName }).ToArrayAsync();
            var company = await _companyRepository.GetAll().Select(p => new CompanyOutputDto { CompanyId = p.Id, CompanyName = p.Name }).ToArrayAsync();
            var projectType = await _projectTypeRepository.GetAll().Select(p => new ProjectTypeOutputDto { ProjectTypeId = p.Id, ProjectTypeName = p.ProjectTypeName }).ToArrayAsync();
            var output = new GetProjects
            {
                Company = company,
                Team = team,
                Technology = technology,
                ProjectType = projectType
            };
            var projectquery = await _projectRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            var project = projectquery.MapTo<CreateProjectInput>();
            output.Project = project;
            return output;
        }
        public async Task CreateOrUpdateProject(CreateProjectInput input)
        {
            if (input.Id == 0)
            {
                await CreateProject(input);

            }
            else
            {
                await UpdateProject(input);

            }
        }
         [AbpAuthorize(AppPermissions.Pages_Tenant_Project_CreateProject)]
        public async Task CreateProject(CreateProjectInput input)
        {
            var project = input.MapTo<Project>();
            await _projectRepository.InsertAsync(project);

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_EditProject)]
        public async Task UpdateProject(CreateProjectInput input)
        {
            var project = input.MapTo<Project>();
            await _projectRepository.UpdateAsync(project);

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_DeleteProject)]
        public async Task DeleteProject(IdInput input)
        {
            await _projectRepository.DeleteAsync(input.Id);
        }
        public async Task<PagedResultOutput<ProjectBaclogView_Dto>> GetProjectBacklog(GetProjectBacklogInput input)
        {
            var query = _ProductBacklogRepository.GetAll().Where(p => p.ProjectId == input.ProjectId)
                      .WhereIf(
                      !input.Filter.IsNullOrWhiteSpace(),
                      u => u.Projects.ProjectName.Contains(input.Filter) ||
                      u.RequestUser.UserName.Contains(input.Filter) ||
                      u.What.Contains(input.Filter) ||
                      u.When.Contains(input.Filter) ||
                      u.Where.Contains(input.Filter) ||
                      u.WhoUser.UserName.Contains(input.Filter) ||
                      u.Why.Contains(input.Filter) ||
                      u.TotalHousrs.ToString().Contains(input.Filter) ||
                      u.Sprint.Contains(input.Filter) ||
                      u.Notes.Contains(input.Filter) ||
                      u.BacklogTypes.BackLogTypeName.Contains(input.Filter) ||
                      u.BackLogStatuss.BackLogStatusName.Contains(input.Filter) ||
                      u.BackLogPrioritys.BackLogPriorityName.Contains(input.Filter) ||
                      u.ApproverUser.UserName.Contains(input.Filter) ||
                      u.ApprovedDate.ToString().Contains(input.Filter));
            try
            {
                var projectbacklog1 = await query
                    .OrderBy(p => p.ProjectBacklogName)
             .PageBy(input)
            .ToListAsync();
            }catch(Exception ex)
            {

            }
            var projectbacklog = await query
                .OrderBy(p=>p.ProjectBacklogName)
             .PageBy(input)
            .ToListAsync();
            var projectbacklogcount = await query.CountAsync();
            var projectbacklogdto = (from c in projectbacklog
                                    select new ProjectBaclogView_Dto { Id=c.Id,ProjectName = c.Projects.ProjectName, RequestFrom = c.RequestFrom != null ? c.RequestUser.UserName : "", RequestDate = c.RequestDate != null ? c.RequestDate.ToString() : "", 
                                    Sprint=c.Sprint,TotalHousrs=c.TotalHousrs,What=c.What,When=c.When,Where=c.Where,Who=c.WhoId!=null?c.WhoUser.UserName:"",Why=c.Why,Notes=c.Notes,BacklogType=c.BacklogTypeId!=null?c.BacklogTypes.BackLogTypeName:"",
                                    BacklogStatus=c.BacklogStatusId!=null?c.BackLogStatuss.BackLogStatusName:"",BacklogPriority=c.BacklogPriorityId!=null?c.BackLogPrioritys.BackLogPriorityName:"",ApprovedDate=c.ApprovedDate.ToString(),ApprovedBy=c.ApprovedBy!=null?c.ApproverUser.UserName:"",ProjectBacklogName=c.ProjectBacklogName});
            var querydto = projectbacklogdto.OrderBy(input.Sorting);
            var projectbacklogdtos = querydto.MapTo<List<ProjectBaclogView_Dto>>();

            return new PagedResultOutput<ProjectBaclogView_Dto>(projectbacklogcount, projectbacklogdtos);

        }
        public async Task<FileDto> GetProjectBacklogToExcel(GetProjectBacklogInput input)
        {
            var query = _ProductBacklogRepository.GetAll().Where(p => p.ProjectId == input.ProjectId)
                     .WhereIf(
                     !input.Filter.IsNullOrWhiteSpace(),
                     u => u.Projects.ProjectName.Contains(input.Filter) ||
                     u.RequestUser.UserName.Contains(input.Filter) ||
                     u.What.Contains(input.Filter) ||
                     u.When.Contains(input.Filter) ||
                     u.Where.Contains(input.Filter) ||
                     u.WhoUser.UserName.Contains(input.Filter) ||
                     u.Why.Contains(input.Filter) ||
                     u.TotalHousrs.ToString().Contains(input.Filter) ||
                     u.Sprint.Contains(input.Filter) ||
                     u.Notes.Contains(input.Filter) ||
                     u.BacklogTypes.BackLogTypeName.Contains(input.Filter) ||
                     u.BackLogStatuss.BackLogStatusName.Contains(input.Filter) ||
                     u.BackLogPrioritys.BackLogPriorityName.Contains(input.Filter) ||
                     u.ApproverUser.UserName.Contains(input.Filter) ||
                     u.ApprovedDate.ToString().Contains(input.Filter));
            try
            {
                var projectbacklog1 = await query
                    .OrderBy(p => p.ProjectBacklogName)
             .PageBy(input)
            .ToListAsync();
            }
            catch (Exception ex)
            {

            }
            var projectbacklog = await query
                .OrderBy(p => p.ProjectBacklogName)
             .PageBy(input)
            .ToListAsync();
            var projectbacklogcount = await query.CountAsync();
            var projectbacklogdto = (from c in projectbacklog
                                     select new ProjectBaclogView_Dto
                                     {
                                         Id = c.Id,
                                         ProjectName = c.Projects.ProjectName,
                                         RequestFrom = c.RequestFrom != null ? c.RequestUser.UserName : "",
                                         RequestDate = c.RequestDate != null ? c.RequestDate.ToString() : "",
                                         Sprint = c.Sprint,
                                         TotalHousrs = c.TotalHousrs,
                                         What = c.What,
                                         When = c.When,
                                         Where = c.Where,
                                         Who = c.WhoId != null ? c.WhoUser.UserName : "",
                                         Why = c.Why,
                                         Notes = c.Notes,
                                         BacklogType = c.BacklogTypeId != null ? c.BacklogTypes.BackLogTypeName : "",
                                         BacklogStatus = c.BacklogStatusId != null ? c.BackLogStatuss.BackLogStatusName : "",
                                         BacklogPriority = c.BacklogPriorityId != null ? c.BackLogPrioritys.BackLogPriorityName : "",
                                         ApprovedDate = c.ApprovedDate.ToString(),
                                         ApprovedBy = c.ApprovedBy != null ? c.ApproverUser.UserName : "",
                                         ProjectBacklogName = c.ProjectBacklogName
                                     });
            var querydto = projectbacklogdto.OrderBy(input.Sorting);
            var projectbacklogdtos = querydto.MapTo<List<ProjectBaclogView_Dto>>();
            return _ProjectBacklogListExcelExporter.ExportToFile(projectbacklogdtos);
        }
        public async Task<GetProjectBacklogs> GetProjectBacklogForEdit(NullableIdInput<long> input)
        {
            var projectBacklog = _ProductBacklogRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefault();
            var projects = await _projectRepository.GetAll().Select(p => new ProjectDto { ProjectId = p.Id, ProjectName = p.ProjectName }).ToArrayAsync();
            var backlogstatus = await _BackLogStatusRepository.GetAll().Select(p => new BacklogStatusDto { BacklogStatusId = p.Id, BacklogStatusName = p.BackLogStatusName }).ToArrayAsync();
            var backlogType = await _BackLogTypeRepository.GetAll().Select(p => new BacklogTypeDto { BacklogTypeId = p.Id, BacklogTypeName = p.BackLogTypeName }).ToArrayAsync();
            var users = UserManager.Users.Select(p => new Userdto { UserId = p.Id, UserName = p.UserName }).ToArray();
            var backlogpriority = await _BackLogPriorityRepository.GetAll().Select(p => new BacklogPriorityDto { BacklogPriorityId = p.Id, BacklogPriorityName = p.BackLogPriorityName }).ToArrayAsync();


            var output = new GetProjectBacklogs { Project=projects,BacklogProirity=backlogpriority,BacklogStatus=backlogstatus,BacklogType=backlogType,Users=users};
            output.ProjectBacklog = projectBacklog.MapTo<CreateProjectBacklogInput>();
            return output;
        }
        public async Task CreateOrUpdateProjectBacklog(CreateProjectBacklogInput input)
        {
            if(input.Id==0)
            {
                await CreateProjectBacklog(input);

            }
            else
            {
                await UpdateProjectBacklog(input);

            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectBacklog_CreateProjectBacklog)]
        public async Task CreateProjectBacklog(CreateProjectBacklogInput input)
        {
            input.RequestDate = DateTime.Now;
            input.RequestFrom = (long)AbpSession.UserId;
            var projectBacklog = input.MapTo<ProductBacklog>();
            await _ProductBacklogRepository.InsertAsync(projectBacklog);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectBacklog_EditProjectBacklog)]
        public async Task UpdateProjectBacklog(CreateProjectBacklogInput input)
        {
           
            input.ProjectId = (from c in _ProductBacklogRepository.GetAll().Where(p => p.Id == input.Id) select c.ProjectId).FirstOrDefault();
            if(input.BacklogPriorityId==null)
            {
                input.BacklogPriorityId = (from c in _ProductBacklogRepository.GetAll().Where(p => p.Id == input.Id) select c.BacklogPriorityId).FirstOrDefault();
            }
            if(input.BacklogStatusId==null)
            {
                input.BacklogStatusId = (from c in _ProductBacklogRepository.GetAll() where c.Id == input.Id select c.BacklogStatusId).FirstOrDefault();
            }
            if(input.BacklogTypeId==null)
            {
                input.BacklogTypeId = (from c in _ProductBacklogRepository.GetAll() where c.Id == input.Id select c.BacklogTypeId).FirstOrDefault();
            }
            if(input.ApprovedBy==null)
            {
                input.ApprovedBy = (from c in _ProductBacklogRepository.GetAll() where c.Id == input.Id select c.ApprovedBy).FirstOrDefault();
            }
            if(input.WhoId==null)
            {
                input.WhoId = (from c in _ProductBacklogRepository.GetAll() where c.Id == input.Id select c.WhoId).FirstOrDefault();
            }
            var projectBacklog = input.MapTo<ProductBacklog>();
            await _ProductBacklogRepository.UpdateAsync(projectBacklog);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectBacklog_DeleteProjectBacklog)]
        public async Task DeleteProjectBacklog(IdInput input)
        {
            await _ProductBacklogRepository.DeleteAsync(input.Id);
        }
        public async Task<PagedResultOutput<ProjectTaskView_dto>> GetProjectTask(GetProjectTaskInput input)
        {
            var query=_projectTaskRepository.GetAll().Where(p=>p.ProductBacklogs.ProjectId==input.ProjectId)
                       .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                       u=>u.Remarks.Contains(input.Filter)||
                           u.AssignedUser.UserName.ToString().Contains(input.Filter)||
                           u.BackLogStatuss.BackLogStatusName.Contains(input.Filter)||
                           u.StartDate.ToString().Contains(input.Filter)||
                           u.TaskName.Contains(input.Filter)||
                           u.ProductBacklogs.ProjectBacklogName.Contains(input.Filter)||
                           u.ActualTime.ToString().Contains(input.Filter)||
                           u.AllotedHours.ToString().Contains(input.Filter)||
                           u.EndDate.ToString().Contains(input.Filter)||
                           u.EstimateHours.ToString().Contains(input.Filter));
            var projectTaskCount = await query.CountAsync();
            var projectquery = await query
                .OrderBy(p=>p.Id)
                .PageBy(input)
                .ToListAsync();
            var projectDto = from c in projectquery
                             select new ProjectTaskView_dto
                             {Id=c.Id,
                               TaskName=c.TaskName,ProductBacklog=c.ProductBacklogId!=null?c.ProductBacklogs.ProjectBacklogName:"",Status=c.StatusId!=null?c.BackLogStatuss.BackLogStatusName:"",Remarks=c.Remarks,AssignedUser=c.AssignedUserId!=null?c.AssignedUser.UserName:"",
                               EndDate=c.EndDate.ToString(),StartDate=c.StartDate.ToString(),ActualTime=c.ActualTime,AllotedHours=c.AllotedHours,EstimateHours=c.EstimateHours
                             };
            try
            {
                var projectquerydto1 = projectDto.OrderBy(input.Sorting);
            }catch(Exception ex)
            {

            }
            var projectquerydto = projectDto.OrderBy(input.Sorting);
            var projectDtos = projectquerydto.MapTo<List<ProjectTaskView_dto>>();

            return new PagedResultOutput<ProjectTaskView_dto>(projectTaskCount,projectDtos);
        }
        public async Task<FileDto> GetProjectTaskForExcel(GetProjectTaskInput input)
        {
            var query = _projectTaskRepository.GetAll().Where(p => p.ProductBacklogs.ProjectId == input.ProjectId)
                       .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                       u => u.Remarks.Contains(input.Filter) ||
                           u.AssignedUser.UserName.ToString().Contains(input.Filter) ||
                           u.BackLogStatuss.BackLogStatusName.Contains(input.Filter) ||
                           u.StartDate.ToString().Contains(input.Filter) ||
                           u.TaskName.Contains(input.Filter) ||
                           u.ProductBacklogs.ProjectBacklogName.Contains(input.Filter) ||
                           u.ActualTime.ToString().Contains(input.Filter) ||
                           u.AllotedHours.ToString().Contains(input.Filter) ||
                           u.EndDate.ToString().Contains(input.Filter) ||
                           u.EstimateHours.ToString().Contains(input.Filter));
            var projectTaskCount = await query.CountAsync();
            var projectquery = await query
                .OrderBy(p => p.Id)
                .PageBy(input)
                .ToListAsync();
            var projectDto = from c in projectquery
                             select new ProjectTaskView_dto
                             {
                                 Id = c.Id,
                                 TaskName = c.TaskName,
                                 ProductBacklog = c.ProductBacklogId != null ? c.ProductBacklogs.ProjectBacklogName : "",
                                 Status = c.StatusId != null ? c.BackLogStatuss.BackLogStatusName : "",
                                 Remarks = c.Remarks,
                                 AssignedUser = c.AssignedUserId != null ? c.AssignedUser.UserName : "",
                                 EndDate = c.EndDate.ToString(),
                                 StartDate = c.StartDate.ToString(),
                                 ActualTime = c.ActualTime,
                                 AllotedHours = c.AllotedHours,
                                 EstimateHours = c.EstimateHours
                             };
            try
            {
                var projectquerydto1 = projectDto.OrderBy(input.Sorting);
            }
            catch (Exception ex)
            {

            }
            var projectquerydto = projectDto.OrderBy(input.Sorting);
            var projectDtos = projectquerydto.MapTo<List<ProjectTaskView_dto>>();
            return _ProjectTaskListExcelExporter.ExportToFile(projectDtos);
        }
        public async Task<GetProjectTasks> GetProjectTaskForEdit(NullableIdInput<long> input)
        {
            var users = UserManager.Users.Select(p => new Userdto { UserId = p.Id, UserName = p.UserName }).ToArray();
            var projectbacklogStatus = await _BackLogStatusRepository.GetAll().Select(p => new BacklogStatusDto {BacklogStatusId=p.Id,BacklogStatusName=p.BackLogStatusName }).ToArrayAsync();
            var projectBacklog = await _ProductBacklogRepository.GetAll().Select(p => new ProjectBacklogDto { ProductBacklogId = p.Id, ProjectBacklogName = p.ProjectBacklogName }).ToArrayAsync();
            var projectTask = await _projectTaskRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetProjectTasks { User=users,ProjectBacklog=projectBacklog,BacklogStatus=projectbacklogStatus};
            output.ProjectTask = projectTask.MapTo<CreateProjectTaskInput>();

            return output;
        }
        public async Task CreateOrUpdateProjectTask(CreateProjectTaskInput input)
        {
            if(input.Id==0)
            {
                await CreateProjectTask(input);
            }
            else
            {
                await UpdateProjectTask(input);
            }

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectTask_CreateProjectTask)]
        public async Task CreateProjectTask(CreateProjectTaskInput input)
        {
            var projectTask = input.MapTo<ProductTask>();
            await _projectTaskRepository.InsertAsync(projectTask);

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectTask_EditProjectTask)]
        public async Task UpdateProjectTask(CreateProjectTaskInput input)
        {
            if(input.AssignedUserId==null)
            {
                input.AssignedUserId = (from c in _projectTaskRepository.GetAll() where c.Id == input.Id select c.AssignedUserId).FirstOrDefault();
            }
            if(input.StatusId==null)
            {
                input.StatusId = (from c in _projectTaskRepository.GetAll() where c.Id == input.Id select c.StatusId).FirstOrDefault();
            }
            if(input.ProductBacklogId==null)
            {
                input.ProductBacklogId = (from c in _projectTaskRepository.GetAll() where c.Id == input.Id select c.ProductBacklogId).FirstOrDefault();
            }
            var projectTask = input.MapTo<ProductTask>();
            await _projectTaskRepository.UpdateAsync(projectTask);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Project_ProjectTask_DeleteProjectTask)]
        public async Task DeleteProjectTask(IdInput input)
        {
           await _projectTaskRepository.DeleteAsync(input.Id);
        }
        public async Task<ListResultOutput<ProjectCheckListDto>> GetProjectChecklists(NullableIdInput<long> input)
        {
            var projectchecklist = await _ProjectCheckListRepository.GetAll().Where(p => p.ProjectId == input.Id).ToListAsync();
            var projectchectListDto = (from c in projectchecklist select new ProjectCheckListDto { Id = c.Id, Factor = c.Factor, Description = c.Description, UserId = c.CreatorUserId != null ? c.CreatorUserId : 0, UserName = c.PostedUser.Name + " " + c.PostedUser.Surname, PhotoPath = c.PostedUser.ProfilePictureId, CreationTime = c.CreationTime, PostedTime = c.CreationTime.ToString("dd MMM yyyy"), ProjectImportant = c.ProjectImportant != null ? c.ProjectImportant : false });
            //foreach (var pclist in projectchectListDto)
            //{
            //    pclist.PostedTime = pclist.CreationTime.ToString("dd-MMM-yyyy");

            //}
            var projectChecklistDtos = projectchectListDto.MapTo<List<ProjectCheckListDto>>();
            return new ListResultOutput<ProjectCheckListDto>(projectChecklistDtos);
        }
        public async Task<GetProjectCheckLists> GetProjectCheckListForEdit(NullableIdInput<long> input)
        {
            var projectCheckList = await _ProjectCheckListRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetProjectCheckLists { };
            output.ProjectCheckList = projectCheckList.MapTo<CreateProjectChecklistInput>();
            return output;
        }
        public async Task CreateOrUpdateProjectCheckList(CreateProjectChecklistInput input)
        {
            if(input.Id==0)
            {
                await CreateProjectCheckList(input);

            }
            else
            {
                await UpdateProjectCheckList(input);

            }
        }
        public async Task CreateProjectCheckList(CreateProjectChecklistInput input)
        {
            input.PostedId = (long)AbpSession.GetUserId();
            var projectCheckList = input.MapTo<ProjectCheckList>();
            
            await _ProjectCheckListRepository.InsertAsync(projectCheckList);
        }
        public async Task UpdateProjectCheckList(CreateProjectChecklistInput input)
        {
            input.PostedId = (long)AbpSession.GetUserId();

            var projectCheckList = input.MapTo<ProjectCheckList>();
            await _ProjectCheckListRepository.UpdateAsync(projectCheckList);
        }
        public async Task DeleteProjectCheckList(IdInput input)
        {
           await _ProjectCheckListRepository.DeleteAsync(input.Id);
        }
        public async Task UpdateProjectImportant(CreateProjectChecklistInput input)
        {
            await UpdateProjectCheckList(input);
            CreateProjectImportantInput projectImportant = new CreateProjectImportantInput();
            projectImportant.ImportantMakeUserId = (long)AbpSession.GetUserId();
            projectImportant.ProjectChecklistId = input.Id;
            projectImportant.ProjectId = input.ProjectId;
            var projectImportantInput = projectImportant.MapTo<ProjectImportant>();
            await _ProjectImportantRepository.InsertAsync(projectImportantInput);
        }
        public async Task<ListResultOutput<ProjectCheckListDto>> GetImportantProjectCheckList(NullableIdInput<long> input)
        {
            long userid=(long)AbpSession.GetUserId();
            var projectchecklist = (from c in _ProjectCheckListRepository.GetAll() join i in _ProjectImportantRepository.GetAll() on c.Id equals i.ProjectChecklistId where c.ProjectId == input.Id && i.MakeUser.Id == userid select c).ToList();
            var projectchectListDto = (from c in projectchecklist select new ProjectCheckListDto { Id = c.Id, Factor = c.Factor, Description = c.Description, UserId = c.CreatorUserId != null ? c.CreatorUserId : 0, UserName = c.PostedUser.Name + " " + c.PostedUser.Surname, PhotoPath = c.PostedUser.ProfilePictureId, CreationTime = c.CreationTime, PostedTime = c.CreationTime.ToString("dd MMM yyyy"), ProjectImportant = c.ProjectImportant != null ? c.ProjectImportant : false });
            //foreach (var pclist in projectchectListDto)
            //{
            //    pclist.PostedTime = pclist.CreationTime.ToString("dd-MMM-yyyy");

            //}
            var projectChecklistDtos = projectchectListDto.MapTo<List<ProjectCheckListDto>>();
            return new ListResultOutput<ProjectCheckListDto>(projectChecklistDtos);
        }
    }
    public class ProjectData
    {
        public string Data { get; set; }
        public string UserName { get; set; }
    }
}
