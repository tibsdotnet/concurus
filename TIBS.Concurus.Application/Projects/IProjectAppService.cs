﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Projects.Dto;

namespace TIBS.Concurus.Projects
{
   public interface IProjectAppService:IApplicationService
    {
       Task<ListResultOutput<ProjectView_Dto>> GetProjects(GetProjectInput input);
       Task<GetProjects> GetProjectForEdit(NullableIdInput<long> input);
       Task CreateOrUpdateProject(CreateProjectInput input);
       Task DeleteProject(IdInput input);
       Task<PagedResultOutput<ProjectBaclogView_Dto>> GetProjectBacklog(GetProjectBacklogInput input);
       Task<GetProjectBacklogs> GetProjectBacklogForEdit(NullableIdInput<long> input);
       Task CreateOrUpdateProjectBacklog(CreateProjectBacklogInput input);
       Task DeleteProjectBacklog(IdInput input);
       Task<PagedResultOutput<ProjectTaskView_dto>> GetProjectTask(GetProjectTaskInput input);
       Task<GetProjectTasks> GetProjectTaskForEdit(NullableIdInput<long> input);
       Task CreateOrUpdateProjectTask(CreateProjectTaskInput input);
       Task DeleteProjectTask(IdInput input);
       Task<FileDto> GetProjectToExcel(GetProjectInput input);
       Task<FileDto> GetProjectBacklogToExcel(GetProjectBacklogInput input);
       Task<FileDto> GetProjectTaskForExcel(GetProjectTaskInput input);
       Task<ListResultOutput<ProjectCheckListDto>> GetProjectChecklists(NullableIdInput<long> input);
       Task<GetProjectCheckLists> GetProjectCheckListForEdit(NullableIdInput<long> input);
       Task CreateOrUpdateProjectCheckList(CreateProjectChecklistInput input);
       Task DeleteProjectCheckList(IdInput input);
       Task UpdateProjectImportant(CreateProjectChecklistInput input);
       Task<ListResultOutput<ProjectCheckListDto>> GetImportantProjectCheckList(NullableIdInput<long> input);

    }
}
