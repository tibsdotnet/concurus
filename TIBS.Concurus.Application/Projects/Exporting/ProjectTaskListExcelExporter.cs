﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Projects.Dto;

namespace TIBS.Concurus.Projects.Exporting
{
    public class ProjectTaskListExcelExporter : EpPlusExcelExporterBase, IProjectTaskListExcelExporter
    {

        public FileDto ExportToFile(List<ProjectTaskView_dto> ProjectTaskListDtos)
        {
            return CreateExcelPackage(
                "ProjectTask.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ProjectTask"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ProductBacklog"),
                        L("TaskName"),
                        L("StartDate"),
                        L("Status"),
                        L("ActualTime"),
                        L("AllotedHours"),
                        L("ProductBacklog"),
                        L("AssignedUser"),
                        L("EndDate"),
                        L("EstimateHours"),
                        L("RemainingTime"),
                        L("Remarks")


                    );

                    AddObjects(
                        sheet, 2, ProjectTaskListDtos,
                        _ => _.ProductBacklog,
                        _ => _.TaskName,
                        _ => _.StartDate,
                        _ => _.Status,
                        _ => _.ActualTime,
                        _ => _.AllotedHours,
                        _ => _.ProductBacklog,
                        _ => _.AssignedUser,
                        _ => _.EndDate,
                        _ => _.EstimateHours,
                        _ => _.RemainingTime,
                        _ => _.Remarks


                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
