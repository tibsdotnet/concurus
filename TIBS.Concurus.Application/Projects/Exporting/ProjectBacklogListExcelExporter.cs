﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Projects.Dto;

namespace TIBS.Concurus.Projects.Exporting
{
    public class ProjectBacklogListExcelExporter : EpPlusExcelExporterBase, IProjectBacklogListExcelExporter
    {

        public FileDto ExportToFile(List<ProjectBaclogView_Dto> ProjectBacklogListDtos)
        {
            return CreateExcelPackage(
                "ProjectBacklog.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ProjectBacklog"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ProjectBacklogName"),
                        L("ProjectName"),
                         L("ApprovedBy"),
                        L("ApprovedDate"),
                        L("BacklogPriority"),
                        L("BacklogStatus"),
                        L("BacklogType"),
                         L("Notes"),
                        L("RequestDate"),
                        L("RequestFrom"),
                        L("Sprint"),
                         L("What"),
                        L("When"),
                        L("Where"),
                         L("Who"),
                        L("Why"),
                        L("TotalHours")



                    );

                    AddObjects(
                        sheet, 2, ProjectBacklogListDtos,
                        _ => _.ProjectBacklogName,
                        _ => _.ProjectName,
                        _ => _.ApprovedBy,
                        _ => _.ApprovedDate,
                        _ => _.BacklogPriority,
                        _ => _.BacklogStatus,
                        _ => _.BacklogType,
                        _ => _.Notes,
                        _ => _.RequestDate,
                        _ => _.RequestFrom,
                        _ => _.Sprint,
                        _ => _.What,
                        _ => _.When,
                        _ => _.Where,
                        _ => _.Who,
                        _ => _.Why,

                        _ => _.TotalHousrs


                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
