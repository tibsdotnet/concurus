﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.DataExporting.Excel.EpPlus;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Projects.Dto;

namespace TIBS.Concurus.Projects.Exporting
{
    public class ProjectListExcelExporter : EpPlusExcelExporterBase, IProjectListExcelExporter
    {

        public FileDto ExportToFile(List<ProjectView_Dto> ProjectTeamListDtos)
        {
            return CreateExcelPackage(
                "Project.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Project"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ProjectName"),
                        L("Description"),
                        L("Team"),
                        L("TechnologyName"),
                        L("Company")


                    );

                    AddObjects(
                        sheet, 2, ProjectTeamListDtos,
                        _ => _.ProjectName,
                        _ => _.Description,
                        _=>_.Team,
                        _ => _.Technology,
                        _=>_.Company



                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
