﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.Concurus.Dto;
using TIBS.Concurus.Projects.Dto;

namespace TIBS.Concurus.Projects.Exporting
{
    public interface IProjectBacklogListExcelExporter
    {
        FileDto ExportToFile(List<ProjectBaclogView_Dto> ProjectBacklogListDtos);
    }
}
