﻿using Abp.Application.Services;
using TIBS.Concurus.Tenants.Dashboard.Dto;

namespace TIBS.Concurus.Tenants.Dashboard
{
    public interface ITenantDashboardAppService : IApplicationService
    {
        GetMemberActivityOutput GetMemberActivity();
    }
}
